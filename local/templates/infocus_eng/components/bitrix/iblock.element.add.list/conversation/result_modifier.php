<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

foreach ($arResult["ELEMENTS"] as $key => $element){
	$arSelect = Array();
	$arFilter = Array("IBLOCK_ID"=>$element['IBLOCK_ID'], "ID"=>$element['ID']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arProps = $ob->GetProperties();

		$elementProps = array(
			"TEXT" => $arFields["PREVIEW_TEXT"],
			"DATE" => $arProps["CONVERSATION_DATE"]["VALUE"],
			"TYPE" => $arProps["CONVERSATION_TYPE"]["VALUE"]
		);

		$arResult["ELEMENTS"][$key]["PROPERTIES"] = $elementProps;
	}
}

?>