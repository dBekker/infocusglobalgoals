<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult = $arResult['ITEMS'][0];

$rsSpec = CUserFieldEnum::GetList(array(), array("ID" => $arResult["UF_PERSONAL_SPEC"]));
if($UserFieldAr = $rsSpec->GetNext()){
	$arResult["UF_PERSONAL_SPEC"] = $UserFieldAr['VALUE'];
}

foreach ($arResult["UF_ASSIGNED_PROJ"] as $project_id){
	$res = CIBlockElement::GetByID($project_id);
	while($ar_res = $res->GetNext()){
		$arResult['PROJECTS'][] = array(
			"NAME" => $ar_res['NAME'],
			"URL" => $ar_res['DETAIL_PAGE_URL'].$ar_res['ID']."/"
		);
	}
}

?>