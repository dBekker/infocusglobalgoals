/**
 * Popup windows
 */
$(function () {
	var $open = $('.popup-open'),
		$layout = $('.page-content .layout'),
		$modal = $('.popup'),
		$close = $('.popup__close'),
		$scrollPos = 0,
		$document = $(document);

	$open.stop().on('click', function (event) {
		var $object = $('#' + $(this).attr('rel')),
			$top = ($(window).height()-($object.height()+68))/2;

		$(window).width() < 991 ? $top = '' : void 0;

		if ($top < 10) {
			$top = 10;
			$object.css({bottom: $top});
		}

		$scrollPos = $(window).scrollTop();
		$object.css({top: $top});

		openObject($object, $layout, $scrollPos);

        event.preventDefault();
	});

    $layout.stop().on('click', function () {
        closeObject($modal, $layout, $scrollPos);
	});

	$close.stop().on('click', function () {
        closeObject($modal, $layout, $scrollPos);
	});

	$document.on('keydown', function (event) {
		if (event.keyCode === 27) {
            closeObject($modal, $layout, $scrollPos);
		}
	});
});