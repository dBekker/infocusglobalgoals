<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<!--    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118818371-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-118818371-1');
    </script>-->

    <?$APPLICATION->ShowHead();?>

	<title><?$APPLICATION->ShowTitle();?></title>

	<meta name="viewport" content="width=device-width,initial-scale=1">

	<meta name="description" content="<?$APPLICATION->ShowProperty("description")?>">
	<meta property="og:site_name" content="<?=$GLOBAL["SITE_NAME"]?>" />
	<meta property="og:locale" content="ru_RU" />

	<meta property="og:title" content="<?$APPLICATION->ShowTitle();?>" />
	<meta property="og:url" content="<?= SITE_URL; ?>/" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="<?= SITE_URL; ?>/pics/favicon/facebook.png" />
	<meta property="og:description" content="<?$APPLICATION->ShowProperty("description")?>" />

	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:title" content="<?$APPLICATION->ShowTitle();?>" />
	<meta name="twitter:description" content="<?$APPLICATION->ShowProperty("description")?>" />
	<meta name="twitter:image" content="<?= SITE_URL; ?>/pics/favicon/facebook.png" />

	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" href="/build/css/main.min.css?v=24">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/pics/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/pics/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/pics/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/pics/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/pics/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/pics/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/pics/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/pics/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/pics/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/pics/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/pics/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="<?$APPLICATION->ShowTitle();?>"/>
    <meta name="msapplication-square70x70logo" content="/pics/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/pics/favicon/mstile-150x150.png" />
    <meta name="msapplication-square310x310logo" content="/pics/favicon/mstile-310x310.png" />

	<meta name="theme-color" content="#ffffff">

	<link rel="manifest" href="/manifest.webmanifest" />

	<script src="/build/js/vendor/blink-fix.min.js"></script>
</head>
<body>
<!--[if lt IE 10]>
<div class="browsehappy">
	<div class="center">
		<p class="chromeframe">Ваш браузер <strong>давно устарел</strong>. Пожалуйста <a href="http://browsehappy.com/">обновите браузер</a> или <a href="http://www.google.com/chrome/">установите Google Chrome</a> для комфортной работы с этим сайтом.</p>
	</div>
</div>
<![endif]-->

<div class="bx-panel">
	<?$APPLICATION->ShowPanel();?>
</div>

<!-- Yandex.Metrika counter -->
<!--<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter48771068 = new Ya.Metrika({
                    id:48771068,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/48771068" style="position:absolute; left:-9999px;" alt="" /></div></noscript>-->
<!-- /Yandex.Metrika counter -->