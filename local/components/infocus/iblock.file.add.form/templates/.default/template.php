<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false); ?>

<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">
	<?= bitrix_sessid_post() ?>
	<? if (!empty($arResult["ERRORS"])): ?>
        <div class="form__response form__response_error">
			<? ShowError(implode("<br />", $arResult["ERRORS"])) ?>
        </div>
	<?endif;
	if (strlen($arResult["MESSAGE"]) > 0):?>
        <div class="form__response">
			<? ShowNote($arResult["MESSAGE"]) ?>
        </div>
	<? endif ?>

    <div class="form__content">
		<? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
			<? foreach ($arParams["PROPERTY_CODES"] as $property_key => $propertyID): ?>
                <div class="input-field row">
                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                        <label for="form-project-report-<?= $property_key; ?>">
							<? if (intval($propertyID) > 0): ?>
								<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?>
							<? else: ?>
								<?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : GetMessage("IBLOCK_FIELD_" . $propertyID) ?>
							<? endif ?>
                        </label>
                    </div>

                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
						<?
						if (intval($propertyID) > 0) {
							if (
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
							)
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                            elseif (
								(
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
									||
									$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
								)
								&&
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
							)
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
						} elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

						if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
							$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
							$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
						} else {
							$inputNum = 1;
						}

						if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
							$INPUT_TYPE = "USER_TYPE";
						else
							$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

						switch ($INPUT_TYPE):
							case "USER_TYPE":
								for ($i = 0; $i < $inputNum; $i++) {
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
										$description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
									} elseif ($i == 0) {
										$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
										$description = "";
									} else {
										$value = "";
										$description = "";
									} ?>
                                    <input id="form-project-report-<?= $property_key ?>" class="input input_date"
                                           name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]['VALUE']" type="text"
                                           value="<?= $value ?>" required>
									<?
								}
								break;
							case "T":
								for ($i = 0; $i < $inputNum; $i++) {

									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									} elseif ($i == 0) {
										$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
									} else {
										$value = "";
									}
									?>
                                    <textarea id="form-project-report-<?= $property_key ?>" class="input input_area"
                                              name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                                              required><?= $value ?></textarea>
									<?
								}
								break;

							case "S":
							case "N":
								for ($i = 0; $i < $inputNum; $i++) {
									if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
										$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									} elseif ($i == 0) {
										$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

									} else {
										$value = "";
									}
									?>
                                    <input id="form-project-report-<?= $property_key ?>" type="text" class="input"
                                           name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                                           size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]; ?>"
                                           value="<?= $value ?>" required>
									<?
								}
								break;

							case "F":
								for ($i = 0; $i<$inputNum; $i++)
								{
									$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
									?>
									<?if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value]))
								{
									?>
									<?
								}?>
                                    <input type="hidden"
                                           name="PROPERTY[<?=$propertyID?>][<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>]"
                                           value="<?=$value?>" />
                                    <input type="file"
                                           id="form-project-<?=$property_key?>"
                                           class="input input_file"
                                           size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>"
                                           name="PROPERTY_FILE_<?=$propertyID?>_<?=$arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i?>"
                                           placeholder="Загрузить"/>
                                    <label for="form-project-<?=$property_key?>">Загрузить</label>
									<?
								}
								break;
						endswitch; ?>
                    </div>
                </div>
			<? endforeach; ?>
		<? endif ?>

        <div class="input-field row">
            <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                <input type="hidden" name="iblock_submit" value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
                <button type="submit" class="btn"><?= GetMessage("IBLOCK_FORM_SUBMIT") ?></button>
                <p>
                    <a href="<?=$arParams["LIST_URL"]?>">Вернуться к проекту</a>
                </p>
            </div>
        </div>
    </div>
</form>