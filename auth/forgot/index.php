<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Восстановление пароля — InFocus");
$APPLICATION->SetTitle("Напоминание пароля");
?>
<div class="row">
    <div class="col col-12">
        <?$APPLICATION->IncludeComponent(
            "bitrix:system.auth.forgotpasswd",
            "",
            array(
                "FORGOT_PASSWORD_URL" => "",
                "PROFILE_URL" => "",
                "REGISTER_URL" => "",
                "SHOW_ERRORS" => "N",
                "COMPONENT_TEMPLATE" => "flat"
            ),
            false
        );?>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>