/**
 * Команда / добавление / удаление
 */
$(function () {
	var $action = $('.team-add'),
		$count = ($('.form-team__list .input-field').length - 1),
		$arrCreate = [],
		$arrDelete = [];

	$action.on('click', function () {
		var $class = 'form-team',
			$wrap = $('.form-team__list'),
			$master = $wrap.find('.form-team__master');


		if ($wrap.find('.input-field').length < 9) {
			$count++;
			$master.clone().removeClass($class + '__master').addClass($class + '__copy-' + $count).appendTo($wrap);

			$arrCreate.push($count);
			$('input[name="team-create"]').val($arrCreate);


			if ($wrap.find('.input-field').length >= 9) {
				$(this).hide();
			}
		}

		$wrap.find('.form-team__copy-' + $count).each(function () {
			var $el = $(this).find('input'),
				$elPhone = $(this).find('input.input_phone'),
				$elDate = $(this).find('input.input_date');

			$(this).attr('rel', $count);

			$(this).find('.form-team__del').removeClass('form-team__del_hidden');

			$elPhone.mask("+0 (000) 000-0000", {clearIfNotMatch: true});
			$elDate.mask("00.00.0000", {clearIfNotMatch: true});
			$elDate.datepicker({
				autoClose: true
			});

			$el.each(function () {
				var $elName = $(this).attr('name'),
					$elID = $(this).attr('id');
				$elName = $elName.substring(6, $elName.length);
				$elName = "team[" + $count + $elName;

				$(this).attr("name", $elName);

				$elID = $elID.substring(11, $elID.length);
				$elID = 'form-team-' + $count + $elID;

				$(this).attr("id", $elID);
			});

			$el.val("");

			var $elLabel = $(this).find('label');

			$elLabel.each(function () {
				if ($(this).attr('for')) {
					var $elLabelFor = $(this).attr('for');

					$elLabelFor = $elLabelFor.substring(11, $elLabelFor.length);

					$elLabelFor = 'form-team-' + $count + $elLabelFor;

					$(this).attr("for", $elLabelFor);
				}
			});
		});
	});

	$('.form-team__list').on('click', '.team-del', function () {
		var $element = $(this).parents('.input-field').not('.form-team__master'),
			$id = $element.attr('rel');

		$arrCreate = $.grep($arrCreate, function(value) {
			return value != $id;
		});

		$('input[name="team-create"]').val($arrCreate);

		if ($element.data('id') > 0) {
			$arrDelete.push($element.data('id'));
			$('input[name="team-delete"]').val($arrDelete);
		}

		$element.remove();
		$action.show();
	});
});