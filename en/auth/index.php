<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');?>
<?$APPLICATION->SetPageProperty("title", "Авторизация — InFocus");
$APPLICATION->SetTitle("Авторизация");?>

<div class="row">
    <div class="col col-12">
        <?$APPLICATION->IncludeComponent(
            "bitrix:system.auth.form",
            "",
            array(
                "FORGOT_PASSWORD_URL" => "/auth/forgot/",
                "PROFILE_URL" => "/profile/",
                "PROFILE_EXPERT_URL" => "/profile-expert/",
                "REGISTER_URL" => "/auth/register/",
                "SHOW_ERRORS" => "Y",
                "COMPONENT_TEMPLATE" => ""
            ),
            false
        );?>
    </div>
</div>

<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php'); ?>


