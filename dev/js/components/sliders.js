/**
 *  Slider main
 */
$(function () {
	var $sliders = $('.slider-main');

	$sliders.each(function () {
		var swiper = new Swiper(this, {
			speed: 400,
			effect: 'fade',
			fadeEffect: {
				crossFade: true
			},
			autoplay: {
				delay: 5000,
				disableOnInteraction: true
			},
			observer: true,
			observeParents: true,
			wrapperClass: 'slider-main__wrap',
			slideClass: 'slider-main__slide',
			pagination: {
				el: '.slider-main__pagination',
				bulletClass: 'slider-main__bullet',
				bulletActiveClass: 'slider-main__bullet_active',
				clickable: true
			},
			on: {
				init: function () {
					$sliders.addClass('slider-main_visible');

					if($(".slider-main__slide").length == 1) {
						$('.slider-main__pagination').addClass("slider-main__pagination_disabled");
					}
				}
			}
		});
	});
});

/**
 *  Slider banner
 */
$(function () {
	var $sliders = $('.slider-banner');

	$sliders.each(function () {
		var swiper = new Swiper(this, {
			speed: 400,
			effect: 'fade',
			fadeEffect: {
				crossFade: true
			},
			autoplay: {
				delay: 5000,
				disableOnInteraction: true
			},
			observer: true,
			observeParents: true,
			wrapperClass: 'slider-banner__wrap',
			slideClass: 'slider-banner__slide',
			pagination: {
				el: '.slider-banner__pagination',
				bulletClass: 'slider-banner__bullet',
				bulletActiveClass: 'slider-banner__bullet_active',
				clickable: true
			},
			on: {
				init: function () {
					$sliders.addClass('slider-banner_visible');

					if($(".slider-banner__slide").length == 1) {
						$('.slider-banner__pagination').addClass("slider-banner__pagination_disabled");
					}
				}
			}
		});
	});
});