<footer class="page-footer">
    <div class="footer-social line-align">
        <div class="container">
            <ul class="footer-social__wrap line-align__wrap">
                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_vk" href="https://vk.com/infocusnetwork" target="_blank" rel="noopener">vk.com</a></li>
<!--                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_ok" href="#!" target="_blank" rel="noopener">ok.ru</a></li>-->
                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_fb" href="https://www.facebook.com/infocusnetwork.org" target="_blank" rel="noopener">facebook.com</a></li>
<!--                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_in" href="#!" target="_blank" rel="noopener">instagram.com</a></li>-->
            </ul>
        </div>
    </div>

    <div class="footer-info vcard">
        <div class="container">
            <div class="row">
                <div class="col col-sm-4 col-12 footer-info__col">
                    Youth Network of Social Innovators<br>Email us: <a href="mailto:info@infocusglobalgoals.net">info@infocusglobalgoals.net</a>

                </div>

                <div class="col col-sm-4 col-12 footer-info__col">

                </div>

                <div class="col col-sm-4 col-12 footer-info__col">
                    <div class="footer-info__links">
						<a href="/en/personal">Privacy Policy</a><br>
						<a href="/en/policy">Terms of Use</a><br>
                        <a href="http://rktv.ru/" target="_blank" rel="noopener">Designed by Reactive</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>