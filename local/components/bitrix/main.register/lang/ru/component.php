<?
$MESS ['REGISTER_WRONG_CAPTCHA'] = "Неверно введено слово с картинки";
$MESS ['REGISTER_FIELD_REQUIRED'] = "Поле #FIELD_NAME# обязательно для заполнения";
$MESS ['REGISTER_DEFAULT_TITLE'] = "Регистрация нового пользователя";
$MESS ['REGISTER_USER_WITH_EMAIL_EXIST'] = "Пользователь с таким e-mail уже существует.";
$MESS["main_register_sess_expired"]="Ваша сессия истекла, повторите попытку регистрации.";
$MESS["main_register_decode_err"]="Ошибка при дешифровании пароля (#ERRCODE#).";
$MESS["BIRTHDAY_CHECK"] = "Участниками проекта могут стать инноваторы в возрасте от 14 до 30 лет";
?>