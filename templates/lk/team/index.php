<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <h1>Личный кабинет</h1>

                        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/nav-sub.php"; ?>

                        <div class="content">
                            <div class="row">
                                <div class="col col-md-9 col-12">
                                    <p>
                                        Заявка оформляется на&nbsp;русском&nbsp;и (или) на&nbsp;английском языке. Название и&nbsp;краткое описание проекта должны быть
                                        оформлены на&nbsp;двух языках.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <h2>Команда</h2>

                        <form id="form-team" class="form form-team" action="/templates/lk/team" method="post" data-ajax="false">
                            <div class="form__response">
                                Логин или пароль указаны неверно. Попробуйте ещё раз.
                            </div>

                            <div class="form__content">
                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-team-1">Название команды</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-team-1" class="input" name="name" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-team-2">Название на английском</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-team-2" class="input" name="name-eng" type="text" required>
                                    </div>

                                    <div class="col-md-6 align-self-center d-none d-md-block">
                                        <div class="input-field__hint">
                                            Пример подсказки
                                        </div>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-team-3">Страна</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-team-3" class="input" name="country" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-team-4">Город</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-team-4" class="input" name="city" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-team-5">Учебное заведение</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-team-5" class="input" name="school" type="text">
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label>Язык общения</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <div class="line-align">
                                            <ul class="line-align__wrap">
                                                <li class="line-align__item">
                                                    <input id="form-team-7" class="input input_radio" name="lang" type="radio" checked>
                                                    <label for="form-team-7">Русский</label>
                                                </li>

                                                <li class="line-align__item">
                                                    <input id="form-team-7" class="input input_radio" name="lang" type="radio">
                                                    <label for="form-team-7">Английский</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12">
                                        <label for="form-team-8">Дополнительная информация о команде</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <textarea id="form-team-8" class="input input_area" name="info"></textarea>
                                    </div>
                                </div>

                                <h2>Состав команды</h2>

                                <input type="hidden" name="team-create">
                                <input type="hidden" name="team-delete">

                                <div class="form-team__list">
                                    <div class="form-team__row form-team__master input-field row">
                                        <div class="form-team__col col-md-3 col-sm-4 col-12 align-self-center">
                                            <label for="form-team-1-1">Фамилия&nbsp;и&nbsp;имя</label>
                                            <input id="form-team-1-1" class="input" name="team[1][name]" type="text" required>
                                        </div>

                                        <div class="form-team__col col-md-2 col-sm-3 col-12 align-self-center">
                                            <label for="form-team-1-4">Дата рождения</label>
                                            <input id="form-team-1-4" class="input input_date" name="team[1][date]" type="text" required>
                                        </div>

                                        <div class="form-team__col col-md-2 col-sm-3 col-12 align-self-center">
                                            <label for="form-team-1-3">Телефон</label>
                                            <input id="form-team-1-3" class="input input_phone" name="team[1][phone]" type="text" required>
                                        </div>

                                        <div class="form-team__side"></div>

                                        <div class="form-team__col col-md-2 col-sm-3 col-12 align-self-center">
                                            <label for="form-team-1-2">E-mail</label>
                                            <input id="form-team-1-2" class="input" name="team[1][email]" type="email" required>
                                        </div>

                                        <div class="form-team__col col-md-3 col-sm-4 col-12 align-self-center form-team__del form-team__del_hidden">
                                            <button class="link team-del" type="button">Удалить участника</button>
                                        </div>
                                    </div>
                                </div>

                                <button class="link team-add" type="button">Добавить участника</button>

                                <div class="input-field row">
                                    <div class="col-md-10 col-sm-9 col-12">
                                        <button class="btn" type="submit">Сохранить</button>

                                        <p>
                                            Предоставляя личные данные, вы&nbsp;подтверждаете,
                                            что ознакомлены с&nbsp;<a href="#!">политикой конфиденциальности</a> и принимаете
                                            <a href="#!">пользовательское соглашение</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>