<?php

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
	$protocol = 'https://';
} else {
	$protocol = 'http://';
}

if (!defined('SITE_URL'))
	define('SITE_URL', $protocol.$_SERVER['HTTP_HOST']);

define("LEADER_GROUP_ID", 9);
define("EXPERT_GROUP_ID", 7);
define("PARTICIPANT_GROUP_ID", 8);

define("IBLOCK_TEAM", 1);
define("IBLOCK_PROJECT", 3);
define("PROJECT_DEFAULT_STATUS_ID" , 52);
define("IBLOCK_CONVERSATION", 4);