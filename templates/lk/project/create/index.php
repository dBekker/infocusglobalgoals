<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/shrot.php"; ?>

                <div class="row">
                    <div class="col col-12">
                        <h1>Личный кабинет</h1>

                        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/nav-sub.php"; ?>

                        <div class="status status_success">
                            Заявка одобрена
                        </div>

                        <div class="content">
                            <div class="row">
                                <div class="col col-md-9 col-12">
                                    <p>
                                        Заявка оформляется на&nbsp;русском&nbsp;и (или) на&nbsp;английском языке. Название и&nbsp;краткое описание проекта должны быть
                                        оформлены на&nbsp;двух языках.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <form id="form-project" class="form form-project" action="/templates/lk/project" method="post" data-ajax="false">
                            <div class="form__response">
                                Логин или пароль указаны неверно. Попробуйте ещё раз.
                            </div>

                            <div class="form__content">
                                <div class="row">
                                    <div class="form-project__col col col-md-6 col-12">
                                        <div class="form-project__cell">
                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label for="form-project-1">Название проекта</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <input id="form-project-1" class="input" name="name" type="text" required>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label for="form-project-2">Страна реализации</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <input id="form-project-2" class="input" name="country" type="text" required>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label for="form-project-3">Сфера</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <select id="form-project-3" class="input input_select" name="areas">
                                                        <option>Экология</option>
                                                        <option>Экономика</option>
                                                        <option>Общество</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label for="form-project-4">Цель</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <select id="form-project-4" class="input input_select" name="target">
                                                        <option>Текст</option>
                                                        <option>Текст</option>
                                                        <option>Текст</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12">
                                                    <label>Логотип проекта</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <div class="form-project__image preview-image" style="background-image: url('/templates/pics/familyguy.png')"></div>

                                                    <input id="form-project-6" class="input input_file" name="photo" type="file" placeholder="Загрузить">
                                                    <label for="form-project-6">Загрузить</label>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12">
                                                    <label for="form-project-7">Краткое описание проекта</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <textarea id="form-project-7" class="input input_area" name="text" required></textarea>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label>Был ли проект реализован ранее</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <div class="line-align">
                                                        <ul class="line-align__wrap">
                                                            <li class="line-align__item">
                                                                <input id="form-project-8" class="input input_radio input_radio-1" name="isreal" type="radio" value="1" checked>
                                                                <label for="form-project-8">Да</label>
                                                            </li>

                                                            <li class="line-align__item">
                                                                <input id="form-project-9" class="input input_radio input_radio-2" name="isreal" type="radio" value="2">
                                                                <label for="form-project-9">Нет</label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12">
                                                    <label for="form-project-10">Ожидаемые результаты</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <textarea id="form-project-10" class="input input_area" name="result" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-project__col col col-md-6 col-12">
                                        <h3 class="d-block d-sm-none">English version</h3>

                                        <div class="form-project__cell">
                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label for="form-project-21">Project name</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <input id="form-project-21" class="input" name="name-eng" type="text" required>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label for="form-project-22">Country of the project</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <input id="form-project-22" class="input" name="country-eng" type="text" required>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label for="form-project-23">Theme</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <select id="form-project-23" class="input input_select" name="domain-eng">
                                                        <option>Текст</option>
                                                        <option>Текст</option>
                                                        <option>Текст</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label for="form-project-24">Goal</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <select id="form-project-24" class="input input_select" name="target-eng">
                                                        <option>Текст</option>
                                                        <option>Текст</option>
                                                        <option>Текст</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12">
                                                    <label>Project logo</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <div class="form-project__image preview-image" style="background-image: url('/templates/pics/familyguy.png')"></div>

                                                    <input id="form-project-26" class="input input_file" name="photo-eng" type="file" placeholder="Upload file">
                                                    <label for="form-project-26">Upload file</label>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12">
                                                    <label for="form-project-27">Short description of the project</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <textarea id="form-project-27" class="input input_area" name="text-eng" required></textarea>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                                    <label>Has anybody already worked on this project</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <div class="line-align">
                                                        <ul class="line-align__wrap">
                                                            <li class="line-align__item">
                                                                <input id="form-project-28" class="input input_radio input_radio-1" name="isreal-eng" type="radio" value="1" checked>
                                                                <label for="form-project-28">Yes</label>
                                                            </li>

                                                            <li class="line-align__item">
                                                                <input id="form-project-29" class="input input_radio input_radio-2" name="isreal-eng" type="radio" value="2">
                                                                <label for="form-project-29">No</label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input-field row">
                                                <div class="col-md-5 col-sm-3 col-12">
                                                    <label for="form-project-30">Expected results</label>
                                                </div>

                                                <div class="col-md-6 col-sm-5 col-12 align-self-center">
                                                    <textarea id="form-project-10" class="input input_area" name="result-eng" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col col-12" style="text-align: center">
                                        <button class="btn" type="submit">Сохранить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>