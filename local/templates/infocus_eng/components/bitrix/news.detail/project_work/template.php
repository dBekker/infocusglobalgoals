<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if ($arResult['EXPERT']['NAME']): ?>
<div class="project-work">
    <div class="row">
        <div class="col col-md-3 col-sm-5 col-12">
            <div class="project-lead project-lead_lk align-self-center">
                <div class="project-lead__info">
                    <div class="project-lead__title">Куратор:</div>
                    <div class="project-lead__name"><?= $arResult['EXPERT']['NAME'] ?></div>
                    <div class="project-lead__phone"><a
                                href="tel:<?= $arResult['EXPERT']['PHONE'] ?>"><?= $arResult['EXPERT']['PHONE'] ?></a>
                    </div>
                    <div class="project-lead__mail"><a
                                href="mailto:<?= $arResult['EXPERT']['EMAIL'] ?>"><?= $arResult['EXPERT']['EMAIL'] ?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-md-5 col-sm-7 col-12 align-self-center">
			<?if($arResult["POINTS"]["SUM"]):?>
            <div class="project-points project-points_small">

                    <div class="project-points__result">
                        <div class="project-points__count"><?=$arResult["POINTS"]["SUM"]?></div>
                        <div class="project-points__title"><?=App\Helpers::formatSay($arResult["POINTS"]["SUM"], [" балл", " балла", " баллов"])?></div>
                    </div>

				<?if($arResult["POINTS"]["EXPERT"]):?>
                    <div class="project-points__info">
						<?php if ($arResult["POINTS"]["EXPERT"]): ?>
                            <?=$arResult["POINTS"]["EXPERT"]?> <?=App\Helpers::formatSay($arResult["POINTS"]["EXPERT"], [" балл", " балла", " баллов"])?> от эксперта<br>
						<?php endif ?>
                        <?php if ($arResult["POINTS"]["VOTE"]): ?>
                            и <?= $arResult["POINTS"]["VOTE"] ?> <?=App\Helpers::formatSay($arResult["POINTS"]["VOTE"], [" балл", " балла", " баллов"])?> по итогам голосования
						<?php endif ?>
                    </div>
                <?endif;?>
            </div>
			<?endif;?>
        </div>
    </div>
</div>
<?php endif;?>

<div class="row">
    <div class="col col-md-8 col-12">
        <div class="project-report">
			<?$APPLICATION->IncludeComponent(
				"bitrix:iblock.element.add.list",
				"conversation",
				Array(
					"ALLOW_DELETE" => "N",
					"ALLOW_EDIT" => $arResult["LEADER_USER"] ? "Y" : "N",
					"EDIT_URL" => "message",
					"ELEMENT_ASSOC" => "CREATED_BY",
					"CREATED_BY" => $arParams["LEADER_ID"] ? $arParams["LEADER_ID"] : "",
					"GROUPS" => array("1","7","9"),
					"IBLOCK_ID" => "4",
					"IBLOCK_TYPE" => "Competition",
					"MAX_USER_ENTRIES" => "100000",
					"NAV_ON_PAGE" => "10",
					"SEF_MODE" => "N",
					"STATUS" => "ANY"
				)
			);?>
        </div>
    </div>

    <div class="col col-md-4 col-12">
        <div class="project-files">
			<?$APPLICATION->IncludeComponent(
				"bitrix:iblock.element.add.list",
				"files",
				Array(
					"ALLOW_DELETE" => "N",
					"ALLOW_EDIT" => $arResult["LEADER_USER"] ? "Y" : "N",
					"EDIT_URL" => "file",
					"ELEMENT_ASSOC" => "CREATED_BY",
					"CREATED_BY" => $arParams["LEADER_ID"] ? $arParams["LEADER_ID"] : "",
					"GROUPS" => array("1","7","9"),
					"IBLOCK_ID" => "5",
					"IBLOCK_TYPE" => "Competition",
					"MAX_USER_ENTRIES" => "100000",
					"NAV_ON_PAGE" => "10",
					"SEF_MODE" => "N",
					"STATUS" => "ANY"
				)
			);?>
        </div>
    </div>
</div>