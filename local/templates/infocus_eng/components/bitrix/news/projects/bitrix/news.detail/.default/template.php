<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$expert_points = $arResult["PROPERTIES"]["POINTS_EXPERT"]["VALUE"];
$vote_points = $arResult["PROPERTIES"]["POINTS_VOTE"]["VALUE"] ? $arResult["PROPERTIES"]["POINTS_VOTE"]["VALUE"] : 0;
$total_points = $expert_points + $vote_points;
?>

<section class="project-header">
	<div class="row">
		<div class="col col-md-7 col-12">
			<h1 class="project-header__title"><?=$arResult['NAME']?></h1>

			<?php if ($total_points > 0): ?>
                <div class="project-points">
                    <div class="project-points__result">
                        <div class="project-points__count"><?= $total_points ?></div>
                        <div class="project-points__title"><?=App\Helpers::formatSay($total_points, [" балл", " балла", " баллов"])?></div>
                    </div>

                    <div class="project-points__info">
                        <?if($expert_points):?><?=$expert_points?> <?=App\Helpers::formatSay($expert_points, [" балл", " балла", " баллов"])?> от эксперта<br><?endif;?>
                        <?if($vote_points):?>и <?=$vote_points?> <?=App\Helpers::formatSay($vote_points, [" балл", " балла", " баллов"])?> по итогам голосования<?endif;?>
                    </div>
                </div>
			<?php endif ?>

			<div class="project-tags">
				<div class="project-tags__team icon icon_to-text icon_not-hover icon-team"><?=$arResult["TEAM_PROP"]['NAME']?></div>

				<div class="project-tags__position"><?=$arResult["PROPERTIES"]["GOAL"]["VALUE"]?></div>

                <?if($arResult["USER_PROPS"]["SPEAK_EN"]):?>
				    <div class="project-tags__lang">Английский язык</div>
                <?endif;?>
			</div>
		</div>
	</div>

	<div class="project-header__image" style="background-image: url('/templates/pics/project.jpg')"></div>
</section>

<section class="project-detail">
	<div class="row">
		<div class="col col-md-8 col-12">
			<div class="content">
                <?=$arResult["DISPLAY_PROPERTIES"]["DESCRIPTION"]["VALUE"]["TEXT"]?>
			</div>

			<div class="to-back">
				<a href="<?=$arResult["LIST_PAGE_URL"]?>">Другие проекты</a>
			</div>
		</div>

		<div class="col col-md-3 offset-md-1 col-12">
			<div class="project-lead">
				<div class="row">
					<div class="project-lead__element col col-md-12 col-sm-6 col-12 align-self-center">
						<div class="project-lead__image" style="background-image: url('<?=$arResult["EXPERT_PROPS"]["PHOTO"];?>')"></div>

						<div class="project-lead__info">
							<div class="project-lead__title">Куратор:</div>

                            <?if ($arResult["EXPERT_PROPS"]["ID"] > 0) :?>
                                <div class="project-lead__name"><a href="/experts/<?=$arResult["EXPERT_PROPS"]["ID"];?>"><?=$arResult["EXPERT_PROPS"]["NAME"];?></a></div>
                            <?else :?>
                                <div class="project-lead__name"><?=$arResult["EXPERT_PROPS"]["NAME"];?></div>
                            <?endif;?>
						</div>
					</div>

					<div class="project-lead__element col col-md-12 col-sm-6 col-12 align-self-center">
						<div class="project-lead__info">
							<div class="project-lead__title">Тим-лидер:</div>
							<div class="project-lead__name"><?=$arResult["LEADER_PROPS"]["NAME"];?></div>
							<div class="project-lead__phone"><a href="tel:<?=$arResult["LEADER_PROPS"]["PHONE"];?>"><?=$arResult["LEADER_PROPS"]["PHONE"];?></a></div>
							<div class="project-lead__mail"><a href="mailto:<?=$arResult["LEADER_PROPS"]["EMAIL"];?>"><?=$arResult["LEADER_PROPS"]["EMAIL"];?></a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>