<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

$colspan = 2;
if ($arResult["CAN_EDIT"] == "Y") $colspan++;
if ($arResult["CAN_DELETE"] == "Y") $colspan++;
?>

<?if (count($arResult["ELEMENTS"]) > 0 || $arParams["ALLOW_EDIT"] == "Y"):?>
    <h2>Файлы</h2>
<?endif;?>

<? if($arParams["ALLOW_EDIT"] == "Y"):?>
    <a class="btn" href="<?=$arParams["EDIT_URL"]?>?edit=Y">Добавить файл</a>
<?endif?>
<div class="project-files__wrap">
	<?if (count($arResult["ELEMENTS"]) > 0):?>
        <ul>
            <?foreach ($arResult["ELEMENTS"] as $arElement):?>
                <li><a href="<?=$arElement["PROPERTIES"]["FILE"]["SRC"]?>" target="_blank"><?=$arElement["NAME"]?> ( <?=$arElement["PROPERTIES"]["FILE"]["EXTENSION"]?>, <?=$arElement["PROPERTIES"]["FILE"]["FILE_SIZE"]?> )</a></li>
            <?endforeach?>
        </ul>
	<?endif?>
</div>