/**
 * Основное меню
 */
$(function () {
    var $action = $('.btn-menu-action'),
        $menu = $('.menu-mobile'),
		$layout = $('.page-header .layout'),
        $elClass = 'btn-menu-action',
        $scrollPos = 0,
        $document = $(document);

    $action.stop().on('click', function (event) {
		if (!$(this).hasClass($elClass + '_close')) {
			$scrollPos = $(window).scrollTop();
            $action.addClass($elClass + '_close');
			openObject($menu, $layout, $scrollPos);
        } else {
            $action.removeClass($elClass + '_close');
			closeObject($menu, $layout, $scrollPos);
        }

        event.preventDefault();
    });

	$layout.stop().on('click', function () {
		$action.removeClass($elClass + '_close');
		closeObject($menu, $layout, $scrollPos);
	});

    $document.on('keydown', function (event) {
        if (event.keyCode === 27) {
			$action.removeClass($elClass + '_close');
			closeObject($menu, $layout, $scrollPos);
        }
    });

    $(window).resize(function() {
        if (window.innerWidth > 991 && $menu.hasClass($menu[0].classList[0] + '_visible')) {
            $action.removeClass($elClass + '_close');
            closeObject($menu, $layout, $scrollPos);
        }
    });
});