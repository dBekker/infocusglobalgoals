<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php'); ?>

<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "slider-main",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "FIELD_CODE" => array("NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", ""),
        "FILTER_NAME" => "",
        "IBLOCK_ID" => "8",
        "IBLOCK_TYPE" => "Content",
        "NEWS_COUNT" => "20",
        "PROPERTY_CODE" => array("", ""),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
    )
); ?>

    <div class="accession">
        <div class="row">
            <div class="col col-md-8 col-12">
                <div class="accession__wrap">
                    <div class="accession__content">
                        <p>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "page",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => ""
                                )
                            ); ?>
                        </p>
                    </div>

                    <div class="accession__action">
                        <a class="btn btn_large btn_white" href="/auth/register/">Хочу изменить мир</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="accession__shrot shrot">
            <div class="shrot__item shrot__item_1"></div>
        </div>
    </div>

    <div class="procedure procedure_main">
        <div class="procedure__banner">Заяви<span>&ensp;</span>о<span>&ensp;</span>себе</div>

        <div class="procedure__element row">
            <div class="col col-md-7 offset-md-2 col-12">
                <div class="procedure__title">
                    <span class="procedure__count procedure__count_one">1</span>
                    Собери команду
                </div>

                <div class="procedure__description">
                    Сформируй молодежную команду от 2 до 10 человек.
                    <a href="/auth/register/">Зарегистрируйся</a> на Интернет-платформе #INFOCUS
                    и создай online-группу своей проектной команды.
                </div>
            </div>
        </div>

        <div class="procedure__element row">
            <div class="col col-md-7 offset-md-5 col-12">
                <div class="procedure__title">
                    <span class="procedure__count">2</span>
                    Предложи проект
                </div>

                <div class="procedure__description">
                    Разработай проект, направленный на решение общественной проблемы, чтобы помочь в достижении
                    <a href="/rules/activities/">17-ти Целей устойчивого развития</a>.
                </div>
            </div>
        </div>

        <div class="procedure__element row">
            <div class="col col-md-7 offset-md-2 col-12">
                <div class="procedure__title">
                    <span class="procedure__count">3</span>
                    Достигай результата
                </div>

                <div class="procedure__description">
                    Получи в помощь <a href="/experts/">эксперта-наставника</a>,
                    который поможет в разработке твоего проекта и достижении поставленной цели.
                </div>
            </div>
        </div>

        <div class="procedure__element row">
            <div class="col col-md-7 offset-md-5 col-12">
                <div class="procedure__title">
                    <span class="procedure__count">4</span>
                    Презентуй проект в ООН
                </div>

                <div class="procedure__description">
                    Представь результат своего проекта перед мировым сообществом
                    в Организации Объединенных Наций и на других крупных международных площадках.
                </div>
            </div>
        </div>

        <div class="procedure__action">
            <a class="btn btn_large" href="/auth/register/">Изменить мир</a>
        </div>

        <div class="procedure__shrot shrot">
            <div class="shrot__item shrot__item_2"></div>
            <div class="shrot__item shrot__item_3"></div>
        </div>
    </div>

    <div class="slider-banner">
        <div class="slider-banner__wrap">
            <div class="slider-banner__slide">
                <div class="slider-banner__image" style="background-image: url('/pics/banner/banner-1.png')"></div>
                <div class="slider-banner__title">Участники</div>
            </div>

            <div class="slider-banner__slide">
                <div class="slider-banner__image" style="background-image: url('/pics/banner/banner-6.png'); height: 350px; width: 723px;"></div>
                <div class="slider-banner__title">Эксперты</div>
            </div>

            <div class="slider-banner__slide">
                <div class="slider-banner__image" style="background-image: url('/pics/banner/banner-3.png')"></div>
                <div class="slider-banner__title">Проекты</div>
            </div>
        </div>

        <div class="slider-banner__pagination"></div>
    </div>

    <div class="organizers">
        <div class="organizers__title">
            <h2>Организаторы</h2>
        </div>

        <div class="organizers__wrap">
            <div class="organizers__image">
                <img src="/pics/partners/logo-4.png" alt="">
            </div>

            <div class="organizers__content">
                Ассоциация объединяет людей и&nbsp;компании вдохновляющими идеями, которые меняют мир. Мы&nbsp;формируем сообщество новаторов, помогая воплощать идеи в&nbsp;конкретные
                результаты&nbsp;&mdash; тем самым развивая экономику знаний и&nbsp;высокие гуманитарные технологии (high-hume). Мы&nbsp;вместе создаем среду, в&nbsp;которой
                образование становится инструментом для решения личных и&nbsp;корпоративных задач.
            </div>
        </div>
    </div>

    <div class="partners">
        <div class="partners__title">
            <h2>Партнеры</h2>
        </div>

        <div class="partners__wrap row">
            <div class="partners__item col col-md-2 col-sm-3 col-12 align-self-center">
                <a href="https://xn--80afcdbalict6afooklqi5o.xn--p1ai/" target="_blank" rel="noopener"><img src="/pics/partners/logo-2.png" alt=""></a>
            </div>
        </div>

        <div class="partners__shrot shrot">
            <div class="shrot__item shrot__item_5"></div>
        </div>
    </div>

<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>