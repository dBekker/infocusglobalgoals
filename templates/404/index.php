<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="error">
                            <div class="error__num">404</div>
                            <div class="error__title"><h1>Такой страницы нет</h1></div>
                            <div class="error__text">Перейдите на <a href="#!">главную</a> или воспользуйтесь навигацией сайта</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>