<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<div> <!--Very important div. It fights against bitrix ;) -->
<h1><?= GetMessage("AUTH_REGISTER") ?></h1>

<p>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "page",
            "AREA_FILE_SUFFIX" => "inc",
            "EDIT_TEMPLATE" => ""
        )
    );?>
</p>

<form id="form-sign-up" class="form form-sign-up" action="<?= POST_FORM_ACTION_URI ?>" name="regform" enctype="multipart/form-data" method="post" data-ajax="false">
    <?if (count($arResult["ERRORS"]) > 0):?>
        <div class="form__response form__response_error">
            <?foreach ($arResult["ERRORS"] as $key => $error):

                if (intval($key) == 0 && $key !== 0):
                    $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
                endif;
            endforeach;
            echo implode("<br />", $arResult["ERRORS"]);?>
        </div>
    <? elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"): ?>
        <div class="form__response">
            <?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?>
        </div>
	<?endif?>

    <div class="form__content">
        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-sign-up-1"><?=GetMessage("REGISTER_FIELD_NAME")?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-sign-up-1" class="input" name="REGISTER[NAME]" value="<?= $arResult["VALUES"]["NAME"] ?>" type="text" required>
            </div>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-2 col-sm-3 col-12 align-self-center">
            <label for="form-sign-up-2"><?=GetMessage("REGISTER_FIELD_EMAIL")?></label>
        </div>

        <div class="col-md-3 col-sm-5 col-12 align-self-center">
            <input id="form-sign-up-2" class="input" name="REGISTER[EMAIL]" value="<?= $arResult["VALUES"]["EMAIL"] ?>" type="email" required>
        </div>

        <div class="col-md-6 align-self-center d-none d-md-block">
            <div class="input-field__hint">
                Адрес электронной почты будет использоваться для входа в Личный кабинет
            </div>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-2 col-sm-3 col-12 align-self-center">
            <label for="form-sign-up-3"><?=GetMessage("REGISTER_FIELD_PERSONAL_PHONE")?></label>
        </div>

        <div class="col-md-3 col-sm-5 col-12 align-self-center">
            <input id="form-sign-up-3" class="input input_phone" name="REGISTER[PERSONAL_PHONE]" value="<?= $arResult["VALUES"]["PERSONAL_PHONE"] ?>" type="text" required>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-2 col-sm-3 col-12 align-self-center">
            <label for="form-sign-up-4"><?=GetMessage("REGISTER_FIELD_PERSONAL_BIRTHDAY")?></label>
        </div>

        <div class="col-md-3 col-sm-5 col-12 align-self-center">
            <input id="form-sign-up-4" class="input input_date" name="REGISTER[PERSONAL_BIRTHDAY]" value="<?= $arResult["VALUES"]["PERSONAL_BIRTHDAY"] ?>" type="text" required>
        </div>

        <div class="col-md-6 align-self-center d-none d-md-block">
            <div class="input-field__hint">
                Участниками проекта могут стать инноваторы в возрасте от 14 до 30 лет
            </div>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-2 col-sm-3 col-12 align-self-center">
            <label for="form-sign-up-5"><?=GetMessage("REGISTER_FIELD_PERSONAL_CITY")?></label>
        </div>

        <div class="col-md-3 col-sm-5 col-12 align-self-center">
            <input id="form-sign-up-5" class="input" name="REGISTER[PERSONAL_CITY]" value="<?= $arResult["VALUES"]["PERSONAL_CITY"] ?>" type="text">
        </div>

        <div class="col-md-6 align-self-center d-none d-md-block">
            <div class="input-field__hint">
                Укажите регион и населенный пункт проживания
            </div>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-3 col-sm-5 col-12 offset-md-2 offset-sm-3 align-self-center">
            <input id="form-sign-up-6" class="input input_file" name="REGISTER_FILES_PERSONAL_PHOTO" type="file">
            <label for="form-sign-up-6">Загрузить фотографию</label>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-3 col-sm-5 col-12 offset-md-2 offset-sm-3 align-self-center">
            <input type="hidden" value="0" name="UF_PERSONAL_SPEAK_EN">
            <input id="form-sign-up-7" class="input input_checkbox" type="checkbox" value="1" name="UF_PERSONAL_SPEAK_EN" <?=$arResult["VALUES"]["UF_PERSONAL_SPEAK_EN"] ? "checked='checked'" : ''?>>
            <label for="form-sign-up-7">Я говорю по-английски</label>
        </div>

        <div class="col-md-6 align-self-center d-none d-md-block">
            <div class="input-field__hint">
                В этом случае ваш проект сможет курировать англоязычный эксперт
            </div>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-2 col-sm-3 col-12 align-self-center">
            <label for="form-sign-up-8">Пароль</label>
        </div>

        <div class="col-md-3 col-sm-5 col-12 align-self-center">
            <input id="form-sign-up-8" class="input" name="REGISTER[PASSWORD]" type="password" required>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
            <input class="btn" type="submit" name="register_submit_button" value="<?= GetMessage("AUTH_REGISTER_SUBMIT") ?>">
            <?$APPLICATION->IncludeFile("/inc/policy.php", Array(), Array()); ?>
        </div>
    </div>
</form>
</div>