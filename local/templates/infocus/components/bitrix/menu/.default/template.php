<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;
?>

<ul class="nav-main__wrap line-align__wrap">
	<? foreach ($arResult as $itemIdex => $arItem): ?>
        <?if($arItem["SELECTED"]):?>
            <li class="nav-main__item nav-main__item_active line-align__item"><?= $arItem["TEXT"] ?></li>
        <?else:?>
            <li class="nav-main__item line-align__item"><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
        <?endif;?>
	<? endforeach; ?>
</ul>