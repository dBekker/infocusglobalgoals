<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Experts");
?>
<div class="row">
	<div class="col col-12">
		<?$arrFilter = array('GROUPS_ID' => array(0 => EXPERT_GROUP_ID));?>
		<?$APPLICATION->IncludeComponent(
			"dev2fun:user.list",
			"list",
			Array(
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "N",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"COUNT" => "999",
				"DETAIL_URL" => "/experts/#ID#/",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"FIELD_CODE" => array("ID","NAME","EMAIL","PERSONAL_PHOTO","PERSONAL_BIRTHDAY","PERSONAL_PROFESSION", "WORK_POSITION", ""),
				"FILTER_NAME" => "arrFilter",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => ".default",
				"PAGER_TITLE" => "Эксперты",
				"PREVIEW_TRUNCATE_LEN" => "",
				"PROPERTY_CODE" => array("UF_PERSONAL_SPEC","UF_USER_COUNTRY"),
				"RESIZE_PERSONAL_PHOTO" => "500*600",
				"RESIZE_TYPE" => "BX_RESIZE_IMAGE_PROPORTIONAL",
				"RESIZE_WORK_LOGO" => "500*600",
				"SET_STATUS_404" => "Y",
				"SHOW_404" => "Y",
                "SORT_BY1" => "UF_SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "NAME",
                "SORT_ORDER2" => "ASC"
			)
		);?>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>