<?php if($APPLICATION->GetCurPage(false) === '/'){$isMain = true;} ?>

<header class="page-header">
    <div class="header-main">
        <div class="container">
            <div class="header-main__wrap">
                <div class="logotype">
                   <?if(!$isMain):?>
					<a href="/en/"><span class="logotype__glyph">#</span>InFocus</a>
                    <?else:?>
                       <span class="logotype__glyph">#</span>InFocus
                    <?endif;?>
                </div>

                <nav class="nav-main line-align d-none d-md-block">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"",
						Array(
							"ROOT_MENU_TYPE" => "top",
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "Y",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => "",
						),
						false
					);?>
                </nav>

                <div class="hot-link line-align d-none d-md-block">
					<?$APPLICATION->IncludeFile("/en/inc/social_menu.php", Array(), Array()); ?>
                </div>

                <div class="auth-action line-align d-none d-md-block">
					<?$APPLICATION->IncludeFile("/en/inc/auth_menu.php", Array(), Array()); ?>
                </div>

                <div class="menu-action d-md-none">
                    <button class="btn-menu-action" type="button" aria-label="Меню"><span></span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="menu-mobile">
        <div class="container">
            <nav class="nav-main line-align line-align_vertical">
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"",
					Array(
						"ROOT_MENU_TYPE" => "top",
						"MAX_LEVEL" => "1",
						"CHILD_MENU_TYPE" => "left",
						"USE_EXT" => "Y",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"MENU_CACHE_TYPE" => "N",
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => "",
					),
					false
				);?>
            </nav>

            <div class="hot-link line-align line-align_vertical">
				<?$APPLICATION->IncludeFile("/en/inc/social_menu.php", Array(), Array()); ?>
            </div>

            <div class="auth-action line-align line-align_vertical">
				<?$APPLICATION->IncludeFile("/en/inc/auth_menu.php", Array(), Array()); ?>
            </div>

            <div class="menu-mobile__contacts">
                Пермь, Петропавловская, 185<br>
                <a class="menu-mobile__phone" href="tel:+74952412697">(495) 24-12-697</a><br>
                <a href="mailto:info@tetradka.org.ru">info@tetradka.org.ru</a><br>
                <a href="#!" target="_blank" rel="noopener">Facebook</a><br>
            </div>
            <div class="menu-mobile__lang">
                <a href="/" class="">In English</a>
            </div>
        </div>
    </div>

    <div class="layout layout_header"></div>
</header>