<header class="page-header">
    <div class="header-main">
        <div class="container">
            <div class="header-main__wrap">
                <div class="logotype">
                    <a href="#!"><span class="logotype__glyph">#</span>InFocus</a>
                </div>

                <nav class="nav-main line-align d-none d-md-block">
                    <ul class="nav-main__wrap line-align__wrap">
                        <li class="nav-main__item nav-main__item_active line-align__item">Правила участия</li>
                        <li class="nav-main__item line-align__item"><a href="#!">Эксперты</a></li>
                        <li class="nav-main__item line-align__item"><a href="#!">Проекты</a></li>
                    </ul>
                </nav>

                <div class="hot-link line-align d-none d-md-block">
                    <ul class="line-align__wrap">
                        <li class="line-align__item"><a class="icon icon_to-text icon-hot-link icon-hot-link_news" href="#!" target="_blank" rel="noopener">Новости</a></li>
                        <li class="line-align__item"><a class="icon icon_to-text icon-hot-link icon-hot-link_vote" href="#!" target="_blank" rel="noopener">Голосование</a></li>
                    </ul>
                </div>

                <div class="auth-action line-align d-none d-md-block">
                    <ul class="line-align__wrap">
                        <li class="line-align__item"><a class="icon icon_to-text icon-auth" href="#!">Вход</a></li>
                    </ul>
                </div>

                <div class="menu-action d-md-none">
                    <button class="btn-menu-action" type="button" aria-label="Меню"><span></span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="menu-mobile">
        <div class="container">
            <nav class="nav-main line-align line-align_vertical">
                <ul class="nav-main__wrap line-align__wrap">
                    <li class="nav-main__item nav-main__item_active line-align__item">Правила участия</li>
                    <li class="nav-main__item line-align__item"><a href="#!">Эксперты</a></li>
                    <li class="nav-main__item line-align__item"><a href="#!">Проекты</a></li>
                </ul>
            </nav>

            <div class="hot-link line-align line-align_vertical">
                <ul class="line-align__wrap">
                    <li class="line-align__item"><a class="icon icon_to-text icon-hot-link icon-hot-link_news" href="#!" target="_blank" rel="noopener">Новости</a></li>
                    <li class="line-align__item"><a class="icon icon_to-text icon-hot-link icon-hot-link_vote" href="#!" target="_blank" rel="noopener">Голосование</a></li>
                </ul>
            </div>

            <div class="auth-action line-align line-align_vertical">
                <ul class="line-align__wrap">
                    <li class="line-align__item"><a class="icon icon_to-text icon-auth" href="#!">Вход</a></li>
                </ul>
            </div>

            <div class="menu-mobile__contacts">
                Пермь, Петропавловская, 185<br>
                <a class="menu-mobile__phone" href="tel:+74952412697">(495) 24-12-697</a><br>
                <a href="mailto:info@tetradka.org.ru">info@tetradka.org.ru</a><br>
                <a href="#!" target="_blank" rel="noopener">Facebook</a><br>
            </div>
        </div>
    </div>

    <div class="layout layout_header"></div>
</header>