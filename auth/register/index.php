<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Регистрация — InFocus");
$APPLICATION->SetTitle("Регистрация");
?>
<div class="row">
    <div class="col col-12">
        <?$APPLICATION->IncludeComponent(
        "bitrix:main.register",
        "infocus",
            array(
				"AUTH" => "N",
                "REQUIRED_FIELDS" => array(
                    0 => "EMAIL",
                    1 => "NAME",
                    2 => "PERSONAL_BIRTHDAY",
                    3 => "PERSONAL_PHONE",
                ),
                "SET_TITLE" => "Y",
                "SHOW_FIELDS" => array(
                    0 => "EMAIL",
                    1 => "NAME",
                    2 => "PERSONAL_BIRTHDAY",
                    3 => "PERSONAL_PHOTO",
                    4 => "PERSONAL_PHONE",
                    5 => "PERSONAL_CITY",
                ),
                "SUCCESS_PAGE" => "/auth?success_reg=Y",
                "USER_PROPERTY" => array(
                    0 => "UF_PERSONAL_SPEAK_EN",
                ),
                "BIRTHDAY_LIMITS" => array(
                    "min" => "14",
                    "max" => "30",
                ),
                "USER_PROPERTY_NAME" => "",
                "USE_BACKURL" => "N",
                "COMPONENT_TEMPLATE" => "infocus"
            ),
            false
        );?>
    </div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>