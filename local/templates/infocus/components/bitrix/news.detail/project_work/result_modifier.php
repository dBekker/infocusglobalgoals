<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$expertId = $arResult["DISPLAY_PROPERTIES"]["EXPERT"]["VALUE"];
$rsUser = CUser::GetByID($expertId);
$arUser = $rsUser->Fetch();

$expertProps = array(
	"EMAIL" => $arUser['EMAIL'],
	"PHONE" => $arUser['PERSONAL_PHONE'],
	"NAME" => $arUser['NAME']
);

$arResult['EXPERT'] = $expertProps;


$arResult["POINTS"] = array(
	"EXPERT" => $arResult["PROPERTIES"]["POINTS_EXPERT"]["VALUE"],
	"VOTE" => $arResult["PROPERTIES"]["POINTS_VOTE"]['VALUE'],
	"SUM" => $arResult["PROPERTIES"]["POINTS_VOTE"]['VALUE'] + $arResult["PROPERTIES"]["POINTS_EXPERT"]["VALUE"]
);

$arResult["LEADER_USER"] = $USER->CanDoOperation('edit_own_profile', $USER->getId());

?>