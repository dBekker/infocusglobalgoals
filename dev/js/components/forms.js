/**
 *  Sign up form
 */
$(function () {
	var $form = $('.form-sign-up');

	if ($form.length) {
		masterForm($form);
	}
});

/**
 *  Sign in form
 */
$(function () {
    var $form = $('.form-sign-in');

    if ($form.length) {
        masterForm($form);
    }
});

/**
 *  Forgot form
 */
$(function () {
	var $form = $('.form-forgot');

	if ($form.length) {
		masterForm($form);
	}
});

/**
 *  Profile form
 */
$(function () {
	var $form = $('.form-profile');

	if ($form.length) {
		masterForm($form);
	}
});


/**
 *  Project lk form
 */
$(function () {
	var $form = $('.form-project');

	if ($form.length) {
		$('.input_radio').on('change', function () {

			var $el = $(this)[0].classList[2];

			if ($el) {
				$('.'+ $el).prop("checked",true);
			}
		});

		masterForm($form);
	}
});

/**
 *  Team lk form
 */
$(function () {
    var $form = $('.form-team');

    if ($form.length) {
        masterForm($form);
    }
});

/**
 *  Project report lk form
 */
$(function () {
    var $form = $('.form-project-report');

    if ($form.length) {
        masterForm($form);
    }
});

/**
 *  Project file lk form
 */
$(function () {
    var $form = $('.form-project-file');

    if ($form.length) {
        masterForm($form);
    }
});

/**
 *  Points lk form
 */
$(function () {
    var $form = $('.form-points');

    if ($form.length) {
        var $resetBtn = $form.find('.form__reset');

        masterForm($form);

        $resetBtn.on('click', function (event) {
            var $parentForm = $(this).parents('form');

            $parentForm.removeClass('form_success');
            $parentForm[0].reset();

            event.preventDefault();
        });
    }
});