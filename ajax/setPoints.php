<?php  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$project_id = $_POST["project"];
$points = $_POST["points"];

$response = [
	'response' => 'error',
	'context' => 'Ошибка, потому что гладиолус'
];

if(CModule::IncludeModule("iblock")){
	$ELEMENT_ID = $project_id;
	$PROPERTY_CODE = "POINTS_EXPERT";
	$PROPERTY_VALUE = $points;

	CIBlockElement::SetPropertyValueCode($ELEMENT_ID, $PROPERTY_CODE, $PROPERTY_VALUE);

	$response = [
		'response' => 'success',
		'context' => 'Добавлено'
	];
}


echo json_encode($response);