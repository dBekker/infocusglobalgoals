<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div> <!--Very important div. It fights against bitrix ;) -->
<h1>Вход в личный кабинет</h1>

<form id="form-sign-in" class="form form-sign-in" action="<?= $arResult["AUTH_URL"] ?>" method="post" data-ajax="false">
    <input type="hidden" name="AUTH_FORM" value="Y" />
    <input type="hidden" name="TYPE" value="AUTH" />
    <?if ($arResult['ERROR']):?>
        <div class="form__response form__response_error">
            <?=$arResult['ERROR_MESSAGE']['MESSAGE'];?>
        </div>
    <?endif;?>

    <?if($_GET["success_reg"] == "Y"):?>
        <div class="form__response">
            Для подтверждения профиля необходимо перейти по ссылке из письма, отправленного на адрес, указанный при заполнении формы регистрации. Участвуй и побеждай!
        </div>
    <?endif;?>

    <div class="form__content">
        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-sign-in-1"><?= GetMessage("AUTH_LOGIN") ?></label>
            </div>
            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-sign-in-1" class="input" name="USER_LOGIN" type="email" required>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-sign-in-2"><?= GetMessage("AUTH_PASSWORD") ?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-sign-in-2" class="input" name="USER_PASSWORD" type="password" required>
            </div>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
            <button class="btn" type="submit"><?= GetMessage("AUTH_LOGIN_BUTTON") ?></button>
            <a class="form-sign-in__forgot" href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>"><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a>

            <p>
                Впервые на сайте? <a href="<?= $arResult["AUTH_REGISTER_URL"] ?>">Зарегистрируйтесь</a>, чтобы стать частью проекта
            </p>
        </div>
    </div>
</form>
</div>

