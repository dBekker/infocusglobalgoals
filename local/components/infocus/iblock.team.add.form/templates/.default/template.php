<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<div class="content">
    <div class="row">
        <div class="col col-md-9 col-12">
            <p>
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "page",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => ""
					)
				);?>
            </p>
        </div>
    </div>
</div>

<h2>Команда</h2>

<form id="form-team" class="form form-team" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" data-ajax="false">
	<?=bitrix_sessid_post()?>
    <!--A bit of mess-->
	<?if (!empty($arResult["ERRORS"])):?>
        <div class="form__response form__response_error">
            <?ShowError(implode("<br />", $arResult["ERRORS"]))?>
        </div>
	<?endif;
	if (strlen($arResult["MESSAGE"] == "updated")):?>
        <div class="form__response">
            Команда успешно обновлена.
        </div>
	<?elseif (strlen($arResult["MESSAGE"] == "added")):?>
        <div class="form__response">
            Команда успешно добавлена. Теперь можно приступить к <a href="/profile/project/add/">созданию проекта</a>
        </div>
    <?endif;?>

    <div class="form__content">
        <?foreach ($arParams["PROPERTY_CODES"] as $propertyID):?>
            <?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"] == "PARTICIPANTS"){
                continue;
            }?>

            <div class="input-field row">
                <div class="col-md-2 col-sm-3 col-12 align-self-center">
                    <?if (intval($propertyID) > 0):?>
                        <label for="form-team-<?=$propertyID?>"><?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?></label>
                    <?else:?>
                        <label for="form-team-<?=$propertyID?>"><?=!empty($arParams["CUSTOM_TITLE_".$propertyID]) ? $arParams["CUSTOM_TITLE_".$propertyID] : GetMessage("IBLOCK_FIELD_".$propertyID)?></label>
                    <?endif?>
                </div>
                <div class="col-md-3 col-sm-5 col-12 align-self-center">
                    <?
                    if (intval($propertyID) > 0)
                    {
                        if (
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                        elseif (
                            (
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                                ||
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                            )
                            &&
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                        )
                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
                    }
                    elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
                    {
                        $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                        $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
                    }
                    else
                    {
                        $inputNum = 1;
                    }

                    if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                        $INPUT_TYPE = "USER_TYPE";
                    else
                        $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

                    switch ($INPUT_TYPE):
                        case "USER_TYPE":

                        case "T":
                            for ($i = 0; $i<$inputNum; $i++)
                            {

                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                }
                                elseif ($i == 0)
                                {
                                    $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                }
                                else
                                {
                                    $value = "";
                                }

                            ?>
                                <textarea id="form-team-<?=$propertyID?>" class="input input_area" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
                            <?
                            }
                        break;

                        case "S":
                        case "N":
                            for ($i = 0; $i<$inputNum; $i++)
                            {
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                {
                                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                }
                                elseif ($i == 0)
                                {
                                    $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                }
                                else
                                {
                                    $value = "";
                                }
                            ?>
                            <input id="form-team-<?=$propertyID?>" class="input" type="text" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]; ?>" value="<?=$value?>" />
                            <?
                            }
                        break;

                        case "L":

                            if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
                            else
                                $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";
                            ?>
                            <div class="line-align">
                                <ul class="line-align__wrap">
                                <?switch ($type):
                                    case "checkbox":
                                    case "radio":
                                        foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum)
                                        {
                                            $checked = false;
                                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                            {
                                                if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID]))
                                                {
                                                    foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum)
                                                    {
                                                        if ($arElEnum["VALUE"] == $key)
                                                        {
                                                            $checked = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if ($arEnum["DEF"] == "Y") $checked = true;
                                            }
                                            ?>
                                                <li class="line-align__item">
                                                    <input id="property_<?=$key?>" class="input input_radio" type="<?=$type?>" name="PROPERTY[<?=$propertyID?>]<?=$type == "checkbox" ? "[".$key."]" : ""?>" value="<?=$key?>" id="property_<?=$key?>"<?=$checked ? " checked=\"checked\"" : ""?> />
                                                    <label for="property_<?=$key?>"><?=$arEnum["VALUE"]?></label>
                                                </li>
                                            <?
                                        }
                                    break;
                                endswitch;?>
                                </ul>
                            </div>
                        <?break;
                    endswitch;?>
                </div>
            </div>
        <?endforeach;?>

	    <h2>Состав команды</h2>

        <?foreach ($arResult["PROPERTY_LIST"] as $propertyID):?>
	        <?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"] == "PARTICIPANTS"){?>
		        <?
		        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
                {
                    $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID])-1 : 0;
                    $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
                }
                else
                {
                    $inputNum = 1;
                }?>

		        <div class="form-team__list">

			        <?
	                for ($i = 0; $i<$inputNum; $i++)
	                {
	                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
	                    {
							$AttachedUser = $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"];
	                        if (is_array($AttachedUser)) {
								$u_name = $AttachedUser["NAME"];
								$u_email = $AttachedUser["EMAIL"];
								$u_birthday = $AttachedUser["PERSONAL_BIRTHDAY"];
								$u_phone = $AttachedUser["PERSONAL_PHONE"];
                            } else {
								$userId = $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"];
								$rsUser = CUser::GetByID($userId);
								$arUser = $rsUser->Fetch();

								$u_name = $arUser["NAME"];
								$u_email = $arUser["EMAIL"];
								$u_birthday = $arUser["PERSONAL_BIRTHDAY"];
								$u_phone = $arUser["PERSONAL_PHONE"];
                            }
	                    }
	                    ?>
                        <div class="form-team__row <?=$i == 0 ? "form-team__master" : ""?> input-field row" rel="<?=$i?>">
                            <div class="form-team__col col-md-3 col-sm-4 col-12 align-self-center">
                                <label for="form-team-<?=$i?>-1">Фамилия&nbsp;и&nbsp;имя</label>
                                <input id="form-team-<?=$i?>-1" class="input" name="team[<?=$i?>][name]" <?=$userId === $USER->GetID() ? "": ""?> type="text" value="<?=$u_name?>" required>
                            </div>

                            <div class="form-team__col col-md-2 col-sm-3 col-12 align-self-center">
                                <label for="form-team-<?=$i?>-4">Дата рождения</label>
                                <input id="form-team-<?=$i?>-4" class="input input_date" name="team[<?=$i?>][birthday]" <?=$userId === $USER->GetID() ? "": ""?> type="text" value="<?=$u_birthday?>" required>
                            </div>

                            <div class="form-team__col col-md-2 col-sm-3 col-12 align-self-center">
                                <label for="form-team-<?=$i?>-3">Телефон</label>
                                <input id="form-team-<?=$i?>-3" class="input input_phone" name="team[<?=$i?>][phone]" <?=$userId === $USER->GetID() ? "": ""?> type="text" value="<?=$u_phone?>" required>
                            </div>

                            <div class="form-team__side"></div>

                            <div class="form-team__col col-md-2 col-sm-3 col-12 align-self-center">
                                <label for="form-team-<?=$i?>-2">E-mail</label>
                                <input id="form-team-<?=$i?>-2" class="input" name="team[<?=$i?>][email]" type="email" <?=$userId === $USER->GetID() ? "": ""?> value="<?=$u_email?>" required>
                            </div>
                            
                            <div class="form-team__col col-md-3 col-sm-4 col-12 align-self-center form-team__del <?=$i == 0 ? "form-team__del_hidden": ""?>">
                                <button class="link team-del" type="button">Удалить участника</button>
                            </div>
                        </div>
                    <?
	                }
			        ?>

		        </div>

	        <?}?>
	    <?endforeach;?>

	    <button class="link team-add" type="button">Добавить участника</button>

	    <div class="input-field row">
		    <div class="col-md-10 col-sm-9 col-12">
		        <input type="hidden" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
                <button type="submit" class="btn" ><?=GetMessage("IBLOCK_FORM_SUBMIT")?></button>
		        <?if (strlen($arParams["LIST_URL"]) > 0):?>
		            <input type="hidden" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
                    <button type="submit" class="btn"><?=GetMessage("IBLOCK_FORM_APPLY")?></button>
		        <?endif?>
                <?$APPLICATION->IncludeFile("/inc/policy.php", Array(), Array()); ?>
		    </div>
	    </div>


	</div>
</form>