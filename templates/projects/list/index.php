<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/shrot.php"; ?>

                <div class="row">
                    <div class="col col-12">
                        <h1>Проекты</h1>

                        <div class="expert-list">
                            <div class="expert-list__row row">
                                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                                    <div class="expert-card" href="#!">
                                        <a href="#!">
                                            <div class="expert-card__image" style="background-image: url('/templates/pics/familyguy.png')">
                                                <div class="expert-card__result">
                                                    <div class="expert-card__count">180</div>
                                                    <div class="expert-card__text">баллов</div>
                                                </div>
                                            </div>

                                            <div class="expert-card__title">
                                                Мир, правосудие и эффективные институты
                                            </div>
                                        </a>

                                        <div class="expert-card__position">Начиная с 1990 года, доля людей, живущих в крайней нищете, сократилась вдвое.</div>

                                        <div class="expert-card__position">2. Ликвидация голода</div>

                                        <div class="expert-card__team icon icon_to-text icon_not-hover icon-team">Команда №1</div>

                                        <div class="expert-card__lead">
                                            <b>Куратор:</b> Петров Иван Иванович
                                        </div>
                                    </div>
                                </div>

                                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                                    <div class="expert-card" href="#!">
                                        <a href="#!">
                                            <div class="expert-card__image" style="background-image: url('/templates/pics/familyguy.png')">
                                                <div class="expert-card__result">
                                                    <div class="expert-card__count">180</div>
                                                    <div class="expert-card__text">баллов</div>
                                                </div>
                                            </div>

                                            <div class="expert-card__title">
                                                Мир, правосудие и эффективные институты
                                            </div>
                                        </a>

                                        <div class="expert-card__position">Начиная с 1990 года, доля людей, живущих в крайней нищете, сократилась вдвое.</div>

                                        <div class="expert-card__position">2. Ликвидация голода</div>

                                        <div class="expert-card__team icon icon_to-text icon_not-hover icon-team">Команда №1</div>

                                        <div class="expert-card__lead">
                                            <b>Куратор:</b> Петров Иван Иванович
                                        </div>
                                    </div>
                                </div>

                                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                                    <div class="expert-card" href="#!">
                                        <a href="#!">
                                            <div class="expert-card__image" style="background-image: url('/templates/pics/familyguy.png')">
                                                <div class="expert-card__result">
                                                    <div class="expert-card__count">180</div>
                                                    <div class="expert-card__text">баллов</div>
                                                </div>
                                            </div>

                                            <div class="expert-card__title">
                                                Мир, правосудие и эффективные институты
                                            </div>
                                        </a>

                                        <div class="expert-card__position">Начиная с 1990 года, доля людей, живущих в крайней нищете, сократилась вдвое.</div>

                                        <div class="expert-card__position">2. Ликвидация голода</div>

                                        <div class="expert-card__team icon icon_to-text icon_not-hover icon-team">Команда №1</div>

                                        <div class="expert-card__lead">
                                            <b>Куратор:</b> Петров Иван Иванович
                                        </div>
                                    </div>
                                </div>

                                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                                    <div class="expert-card" href="#!">
                                        <a href="#!">
                                            <div class="expert-card__image" style="background-image: url('/templates/pics/familyguy.png')">
                                                <div class="expert-card__result">
                                                    <div class="expert-card__count">180</div>
                                                    <div class="expert-card__text">баллов</div>
                                                </div>
                                            </div>

                                            <div class="expert-card__title">
                                                Мир, правосудие и эффективные институты
                                            </div>
                                        </a>

                                        <div class="expert-card__position">Начиная с 1990 года, доля людей, живущих в крайней нищете, сократилась вдвое.</div>

                                        <div class="expert-card__position">2. Ликвидация голода</div>

                                        <div class="expert-card__team icon icon_to-text icon_not-hover icon-team">Команда №1</div>

                                        <div class="expert-card__lead">
                                            <b>Куратор:</b> Петров Иван Иванович
                                        </div>
                                    </div>
                                </div>

                                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                                    <div class="expert-card" href="#!">
                                        <a href="#!">
                                            <div class="expert-card__image" style="background-image: url('/templates/pics/familyguy.png')">
                                                <div class="expert-card__result">
                                                    <div class="expert-card__count">180</div>
                                                    <div class="expert-card__text">баллов</div>
                                                </div>
                                            </div>

                                            <div class="expert-card__title">
                                                Мир, правосудие и эффективные институты
                                            </div>
                                        </a>

                                        <div class="expert-card__position">Начиная с 1990 года, доля людей, живущих в крайней нищете, сократилась вдвое.</div>

                                        <div class="expert-card__position">2. Ликвидация голода</div>

                                        <div class="expert-card__team icon icon_to-text icon_not-hover icon-team">Команда №1</div>

                                        <div class="expert-card__lead">
                                            <b>Куратор:</b> Петров Иван Иванович
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>