<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="form__response">
    <?echo $arResult["MESSAGE_TEXT"]?></p>
</div>

<?if($arResult["SHOW_FORM"]):?>

    <form method="post" class="form" action="<?echo $arResult["FORM_ACTION"]?>" data-ajax="false">
        <div class="form__content">
            <div class="input-field row">
                <div class="col-md-2 col-sm-3 col-12 align-self-center">
                    <label for="form-sign-in-1"><?echo GetMessage("CT_BSAC_LOGIN")?>:</label>
                </div>
                <div class="col-md-3 col-sm-5 col-12 align-self-center">
                    <input type="text" class="input" name="<?echo $arParams["LOGIN"]?>" value="<?echo $arResult["LOGIN"]?>"/>
                </div>
            </div>
            <div class="input-field row">
                <div class="col-md-2 col-sm-3 col-12 align-self-center">
					<?echo GetMessage("CT_BSAC_CONFIRM_CODE")?>:
                </div>
                <div class="col-md-3 col-sm-5 col-12 align-self-center">
                    <input type="text" class="input" name="<?echo $arParams["CONFIRM_CODE"]?>" maxlength="50" value="<?echo $arResult["CONFIRM_CODE"]?>" size="17" />
                </div>
            </div>
            <div class="input-field row">
                <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                    <button class="btn" type="submit"><?echo GetMessage("CT_BSAC_CONFIRM")?></button>
                    <input type="hidden" value="<?echo GetMessage("CT_BSAC_CONFIRM")?>" />
                </div>
            </div>
        </div>

        <input type="hidden" name="<?echo $arParams["USER_ID"]?>" value="<?echo $arResult["USER_ID"]?>" />
    </form>

<?elseif(!$USER->IsAuthorized()):?>
	<?$APPLICATION->IncludeComponent("bitrix:system.auth.authorize", "", array());?>
<?endif?>