<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/shrot.php"; ?>

                <div class="row">
                    <div class="col-12">
                        <div class="expert-detail">
                            <div class="row">
                                <div class="expert-detail__sidebar col col-md-3 col-sm-5 col-12">
                                    <div class="expert-detail__image" style="background-image: url('/templates/pics/familyguy.png')"></div>

                                    <blockquote class="expert-detail__blockquote">
                                        &laquo;Чтобы изменить мир вокруг, вы&nbsp;должны изменить себя&raquo;.
                                    </blockquote>
                                </div>

                                <div class="col col-md-8 offset-md-1 col-12">
                                    <h1>Петрова Ирина Ивановна</h1>

                                    <div class="expert-detail__position">
                                        Председатель совета<br>
                                        Название компании
                                    </div>

                                    <div class="expert-detail__tags line-align">
                                        <ul class="line-align__wrap">
                                            <li class="line-align__item">
                                                <div class="tag">Экономика</div>
                                            </li>

                                            <li class="line-align__item">
                                                <div class="tag">Россия</div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="expert-detail__content">
                                        <h3>Курирует проекты</h3>

                                        <ul>
                                            <li>Мир, правосудие и эффективные институты</li>
                                            <li>Проект экологии</li>
                                        </ul>

                                        <h3>Биография</h3>

                                        <div class="content">
                                            <p>
                                                Русский поэт, драматург и прозаик, заложивший основы русского реалистического направления, критик
                                                и теоретик литературы, историк, публицист; один из самых авторитетных литературных деятелей первой
                                                трети XIX века
                                            </p>
                                        </div>

                                        <h3>Интересы и хобби</h3>

                                        <div class="content">
                                            <p>
                                                Русский поэт, драматург и прозаик, заложивший основы русского реалистического направления, критик
                                                и теоретик литературы, историк, публицист; один из самых авторитетных литературных деятелей первой
                                                трети XIX века
                                            </p>
                                        </div>
                                    </div>

                                    <div class="to-back">
                                        <a href="#!">Все эксперты</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>