<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="content">
    <div class="row">
        <div class="col col-md-9 col-12">
			<? if (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
				<? echo $arResult["DETAIL_TEXT"]; ?>
			<? endif; ?>
        </div>
    </div>
</div>