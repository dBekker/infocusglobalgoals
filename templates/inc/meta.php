<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">

    <title>#INFOCUS CONFERENCE</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="Описание">
    <meta property="og:site_name" content="Project" />
    <meta property="og:locale" content="ru_RU" />

    <meta property="og:title" content="Project" />
    <meta property="og:url" content="http://site.ru/" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="http://site.ru/pics/favicon/facebook.png" />
    <meta property="og:description" content="Описание" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Project" />
    <meta name="twitter:description" content="Описание" />
    <meta name="twitter:image" content="https://site.ru/pics/favicon/facebook.png" />

    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="/build/css/main.min.css?v=3">

    <link rel="shortcut icon" href="/favicon.ico">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/pics/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/pics/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/pics/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/pics/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/pics/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/pics/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/pics/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/pics/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/pics/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/pics/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/pics/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="Project"/>
    <meta name="msapplication-square70x70logo" content="/pics/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/pics/favicon/mstile-150x150.png" />
    <meta name="msapplication-square310x310logo" content="/pics/favicon/mstile-310x310.png" />

    <meta name="theme-color" content="#ffffff">

    <link rel="manifest" href="/manifest.webmanifest">
</head>
<body>

<!--[if lt IE 10]>
<div class="browsehappy">
    <div class="center">
        <p class="chromeframe">Ваш браузер <strong>давно устарел</strong>. Пожалуйста <a href="http://browsehappy.com/">обновите браузер</a> или <a href="http://www.google.com/chrome/">установите Google Chrome</a> для комфортной работы с этим сайтом.</p>
    </div>
</div>
<![endif]-->