<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-visual">
            <div class="visual">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 d-none d-sm-block align-self-end">
                            <div class="visual__image">
                                <img src="/templates/pics/visual-1.png">
                            </div>
                        </div>

                        <div class="col-sm-6 col-12 align-self-center">
                            <div class="visual__content">
                                <h1 class="visual__title">
                                    Поздравляем! Ты&nbsp;стал частью глобального движения! Нас уже 150&nbsp;со всего мира!
                                </h1>

                                <div class="visual__description">
                                    Хотите поучаствовать в&nbsp;проекте в&nbsp;качестве эксперта? Пишите&nbsp;<a href="mailto:info@tetradka.org.ru">info@tetradka.org.ru</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <h1>Вход в личный кабинет</h1>

                        <form id="form-sign-in" class="form form-sign-in" action="/templates/auth/signin" method="post" data-ajax="false">
                            <div class="form__response">
                                Логин или пароль указаны неверно. Попробуйте ещё раз.
                            </div>

                            <div class="form__content">
                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-sign-in-1">Электронная почта</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-sign-in-1" class="input" name="email" type="email" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-sign-in-2">Пароль</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-sign-in-2" class="input" name="password" type="password" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                                        <button class="btn" type="submit">Войти</button>
                                        <a class="form-sign-in__forgot" href="#!">Забыли пароль?</a>

                                        <p>
                                            Или <a href="#!">зарегистрируйтесь</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>