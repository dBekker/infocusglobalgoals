<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="slider-main">
                    <div class="slider-main__wrap">
                        <div class="slider-main__slide">
                            <div class="row">
                                <div class="col col-sm-5 col-12">
                                    <h1 class="slider-main__title">Развивай гуманитарное отрудничество между странами</h1>
                                </div>
                            </div>

                            <div class="slider-main__image" style="background-image: url('/templates/pics/slide-1.jpg')"></div>
                        </div>

                        <div class="slider-main__slide">
                            <div class="row">
                                <div class="col col-sm-5 col-12">
                                    <h1 class="slider-main__title">Реализуй проекты в социальной, экологической или экономической сфере!</h1>
                                </div>
                            </div>

                            <div class="slider-main__image" style="background-image: url('/templates/pics/project.jpg')"></div>
                        </div>
                    </div>

                    <div class="slider-main__pagination"></div>
                </div>

                <div class="accession">
                    <div class="row">
                        <div class="col col-md-8 col-12">
                            <div class="accession__wrap">
                                <div class="accession__content">
                                    <p>
                                        Стань современным героем и&nbsp;предложи вместе
                                        с&nbsp;командой идеи по&nbsp;преобразованию сообщества, приняв участие в&nbsp;Международной молодежной сети социальных инноваторов #INFOCUS
                                    </p>
                                </div>

                                <div class="accession__action">
                                    <a class="btn btn_large btn_white" href="#!">Хочу изменить мир</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="accession__shrot shrot">
                        <div class="shrot__item shrot__item_1"></div>
                    </div>
                </div>

                <div class="procedure procedure_main">
                    <div class="procedure__banner">Заяви<span>&ensp;</span>о<span>&ensp;</span>себе</div>

                    <div class="procedure__element row">
                        <div class="col col-md-7 offset-md-2 col-12">
                            <div class="procedure__title">
                                <span class="procedure__count procedure__count_one">1</span>
                                Собери команду
                            </div>

                            <div class="procedure__description">
                                Сформировать молодежную команду от 2 до 10 человек.
                                Зарегистрироваться на Интернет-платформе Молодежной сети #inFocus
                                и создать online-группу своей проектной команды.
                            </div>
                        </div>
                    </div>

                    <div class="procedure__element row">
                        <div class="col col-md-7 offset-md-5 col-12">
                            <div class="procedure__title">
                                <span class="procedure__count">2</span>
                                Представь идею
                            </div>

                            <div class="procedure__description">
                                Проблема должна соответствовать одной из 17-ти Целей устойчивого
                                развития, провозглашенных ООН на период до 2030 года
                            </div>
                        </div>
                    </div>

                    <div class="procedure__element row">
                        <div class="col col-md-7 offset-md-2 col-12">
                            <div class="procedure__title">
                                <span class="procedure__count">3</span>
                                Вместе с экспертом раработай проект
                            </div>

                            <div class="procedure__description">
                                Проекты, прошедшие проверку, отправляются экспертам. Эксперты
                                знакомятся с предложенными проектами и выбирают те, наставниками
                                которых они хотели бы быть.
                            </div>
                        </div>
                    </div>

                    <div class="procedure__element row">
                        <div class="col col-md-7 offset-md-5 col-12">
                            <div class="procedure__title">
                                <span class="procedure__count">4</span>
                                Презентуй проект в ООН
                            </div>

                            <div class="procedure__description">
                                Решение выбранной проблемы должно содействовать достижению одной
                                или нескольким Целям устойчивого развития.
                            </div>
                        </div>
                    </div>

                    <div class="procedure__action">
                        <a class="btn btn_large" href="#!">Изменить мир</a>
                    </div>

                    <div class="procedure__shrot shrot">
                        <div class="shrot__item shrot__item_2"></div>
                        <div class="shrot__item shrot__item_3"></div>
                    </div>
                </div>

                <div class="slider-banner">
                    <div class="slider-banner__wrap">
                        <div class="slider-banner__slide">
                            <div class="slider-banner__image" style="background-image: url('/pics/banner/banner-1.png')"></div>
                            <div class="slider-banner__title">Участники</div>
                        </div>

                        <div class="slider-banner__slide">
                            <div class="slider-banner__image" style="background-image: url('/templates/pics/familyguy.png')"></div>
                            <div class="slider-banner__title">Участники 2</div>
                        </div>
                    </div>

                    <div class="slider-banner__pagination"></div>
                </div>

                <div class="organizers">
                    <div class="organizers__title">
                        <h2>Организаторы</h2>
                    </div>

                    <div class="organizers__wrap">
                        <div class="organizers__image">
                            <img src="/pics/partners/logo-1.png" alt="">
                        </div>

                        <div class="organizers__content">
                            <a href="http://tetradka.org.ru/" target="_blank" rel="noopener">«Тетрадка Дружбы»</a> — это детско-молодёжная общероссийская общественная организация, основанная в 2016 году. Наша миссия: помочь каждому ребёнку поверить
                            в себя, реализовать свои таланты, и способности во имя дружбы, творчества и развития. Наш девиз: «Общее дело, общее детство, общее будущее»
                        </div>
                    </div>
                </div>

                <div class="partners">
                    <div class="partners__title">
                        <h2>Партнеры</h2>
                    </div>

                    <div class="partners__wrap row">
                        <div class="partners__item col col-md-2 col-sm-3 col-12 align-self-center">
                            <a href="#!" target="_blank" rel="noopener"><img src="/pics/partners/logo-2.png" alt=""></a>
                        </div>

                        <div class="partners__item col col-md-2 col-sm-3 col-12 align-self-center">
                            <a href="#!" target="_blank" rel="noopener"><img src="/pics/partners/logo-3.png" alt=""></a>
                        </div>
                    </div>

                    <div class="partners__shrot shrot">
                        <div class="shrot__item shrot__item_5"></div>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>