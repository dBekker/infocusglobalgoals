<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if ($arResult["DESCRIPTION"]): ?>
    <div class="content">
        <div class="row">
            <div class="col col-md-9 col-12">
                <p>
					<? echo $arResult["DESCRIPTION"]; ?>
                </p>
            </div>
        </div>
    </div>
<?php endif ?>

<div class="activities">
    <div class="activities__wrap row">
        <?foreach($arResult["ITEMS"] as $key=> $arItem):?>
            <div class="activities__element col col-md-2 col-sm-3 col-6">
                <a class="popup-open" href="javascript:void(0)" rel="popup-activities-<?=$key+1?>" aria-label="Открыть">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
                </a>
            </div>
        <?endforeach;?>
    </div>

    <div class="layout"></div>


	<?foreach($arResult["ITEMS"] as $key=> $arItem):?>
        <div class="popup" id="popup-activities-<?=$key+1?>">
            <button class="popup__close icon-popup-close" type="button" aria-label="Закрыть"></button>

            <div class="popup__container">
                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">

                <h2 class="popup__title"><?=$arItem['NAME']?></h2>

                <div class="content">
                    <p>
                       <?=$arItem['DETAIL_TEXT'];?>
                    </p>
                </div>
            </div>
        </div>
	<?endforeach; ?>
</div>
