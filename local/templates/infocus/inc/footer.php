<footer class="page-footer">
    <div class="footer-social line-align">
        <div class="container">
            <ul class="footer-social__wrap line-align__wrap">
                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_vk" href="https://vk.com/infocusglobalgoals" target="_blank" rel="noopener">vk.com</a></li>
<!--                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_ok" href="#!" target="_blank" rel="noopener">ok.ru</a></li>-->
                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_fb" href="https://www.facebook.com/infocusglobalgoals.net" target="_blank" rel="noopener">facebook.com</a></li>
<!--                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_in" href="#!" target="_blank" rel="noopener">instagram.com</a></li>-->
            </ul>
        </div>
    </div>

    <div class="footer-info vcard">
        <div class="container">
            <div class="row">
                <div class="col col-sm-4 col-12 footer-info__col">
                    Молодежная сеть социальных инноваторов<br>Напишите нам: <a href="mailto:info@infocusglobalgoals.net">info@infocusglobalgoals.net</a>

                </div>

                <div class="col col-sm-4 col-12 footer-info__col">

                </div>

                <div class="col col-sm-4 col-12 footer-info__col">
                    <div class="footer-info__links">
                        <a href="/personal">Политика конфиденциальности</a><br>
                        <a href="/policy">Пользовательское соглашение</a><br>
                        <a href="http://rktv.ru/" target="_blank" rel="noopener">Разработано в Реактиве</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>