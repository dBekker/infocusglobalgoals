<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h1><?$APPLICATION->ShowTitle(false)?></h1>

<?if(count($arResult["ITEMS"])>0):?>
<div class="expert-list">
    <div class="expert-list__row row">
		<?foreach($arResult["ITEMS"] as $arResult):?>
            <div class="expert-list__col col col-md-4 col-sm-6 col-12" id="<?=$this->GetEditAreaId($arResult['ID']);?>">
                <div class="expert-card">
                    <a href="<?=$arResult["DETAIL_URL"]?>">
                        <? if($arResult['PERSONAL_PHOTO']):
                            if($arParams['RESIZE_PERSONAL_PHOTO']){
                                $imgSrc = $arResult["PERSONAL_PHOTO"]["RESIZE"]['src'];
                            } else {
                                $imgSrc = $arResult["PERSONAL_PHOTO"]['SRC'];
                            } ?>
                            <div class="expert-card__image" style="background-image: url('<?=$imgSrc?>')"></div>
                        <?endif;?>

                        <div class="expert-card__title">
                           <?=$arResult['NAME'];?>
                        </div>
                    </a>

					<?php if ($arResult["PERSONAL_PROFESSION"] || $arResult["WORK_POSITION"]): ?>
                        <div class="expert-card__position"><?= $arResult["PERSONAL_PROFESSION"] ?>
                            <br><?= $arResult["WORK_POSITION"] ?></div>
					<?php endif ?>

                    <div class="expert-card__tags line-align line-align_center">
                        <ul class="line-align__wrap">
                            <?if($arResult["UF_PERSONAL_SPEC"]):?>
                                <li class="line-align__item">
                                    <div class="tag"><?=$arResult["UF_PERSONAL_SPEC"];?></div>
                                </li>
                            <?endif;?>
							<?php if ($arResult["UF_USER_COUNTRY"]): ?>
                                <li class="line-align__item">
                                    <div class="tag"><?= $arResult["UF_USER_COUNTRY"]; ?></div>
                                </li>
							<?php endif ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?endforeach;?>
    </div>

    <div class="expert-list__footer">
        Хотите участвовать в проекте в качестве эксперта?<br>Пишите <a href="mailto:info@infocusglobalgoals.net">info@infocusglobalgoals.net</a>
    </div>
</div>
<?else:?>
    <p>Пока нет экспертов.</p>
<?endif;?>