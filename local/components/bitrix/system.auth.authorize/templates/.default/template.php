<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */
?>

<form id="form-sign-in" class="form form-sign-in" name="form_auth" action="<?= $arResult["AUTH_URL"] ?>" method="post" data-ajax="false">
    <input type="hidden" name="AUTH_FORM" value="Y" />
    <input type="hidden" name="TYPE" value="AUTH" />
	<?if ($arResult['ERROR_MESSAGE'] <> ''):
		$text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']);
        ?>
        <div class="form__response form__response_error">
			<?=nl2br(htmlspecialcharsbx($text))?>
        </div>
	<?endif;?>
	<?foreach ($arResult["POST"] as $key => $value):?>
        <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>


    <div class="form__content">
        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-sign-in-1"><?= GetMessage("AUTH_LOGIN") ?></label>
            </div>
            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-sign-in-1" class="input" name="USER_LOGIN" type="email" required>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-sign-in-2"><?= GetMessage("AUTH_PASSWORD") ?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-sign-in-2" class="input" name="USER_PASSWORD" type="password" required>
            </div>
        </div>
    </div>

    <div class="input-field row">
        <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
            <input class="btn" type="submit" name="Login"><?= GetMessage("AUTH_LOGIN_BUTTON") ?>
            <a class="form-sign-in__forgot" href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>"><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a>

            <p>
                Или <a href="<?= $arResult["AUTH_REGISTER_URL"] ?>"><?=GetMessage("AUTH_REGISTER")?></a>
            </p>
        </div>
    </div>
</form>
