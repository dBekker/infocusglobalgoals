<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<div> <!--Very important div. It fights against bitrix ;) -->
<h1><?$APPLICATION->ShowTitle(false)?></h1>

<?$APPLICATION->IncludeFile("/inc/submenu.php", Array(), Array()); ?>

<form id="form-profile" class="form form-profile" action="<?=$arResult["FORM_TARGET"]?>" name="form1" method="post" enctype="multipart/form-data" data-ajax="false">
    <?php if (!empty($arResult["strProfileError"])): ?>
        <div class="form__response form__response_error">
            <?ShowError($arResult["strProfileError"]); ?>
        </div>
    <?php endif ?>

    <?if ($arResult['DATA_SAVED'] == 'Y'):?>
        <div class="form__response">
            <?ShowNote(GetMessage('PROFILE_DATA_SAVED'));; ?>
        </div>
    <?endif;?>

	<?php if (!$arResult["LEADER_USER"]): ?>
        <div class="form__response">
            Для редактирования данных необходимо обратиться к лидеру своей команды
        </div>
	<?php endif ?>
	<?=$arResult["BX_SESSION_CHECK"]?>
    <input type="hidden" name="lang" value="<?=LANG?>" />
    <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
    <div class="form__content">
        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-1"><?=GetMessage('NAME')?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-1" class="input" name="NAME" type="text" value="<?=$arResult["arUser"]["NAME"]?>" required <?=$arResult["IS_READONLY"] ? "readonly" : ""?>>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-2"><?=GetMessage('EMAIL')?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-2" class="input" name="EMAIL" type="email" value="<?= $arResult["arUser"]["EMAIL"]?>" required <?=$arResult["IS_READONLY"] ? "readonly" : ""?>>
            </div>

            <div class="col-md-6 align-self-center d-none d-md-block">
                <div class="input-field__hint">

                </div>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-3"><?=GetMessage('USER_PHONE')?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-3" class="input input_phone" name="PERSONAL_PHONE" type="text" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" required <?=$arResult["IS_READONLY"] ? "readonly" : ""?>>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-4"><?=GetMessage("USER_BIRTHDAY_DT")?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-4" class="input <?=!$arResult["IS_READONLY"] ? "input_date" : ""?>" name="PERSONAL_BIRTHDAY" type="text" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" <?=$arResult["IS_READONLY"] ? "readonly" : ""?>>
            </div>

            <div class="col-md-6 align-self-center d-none d-md-block">
                <div class="input-field__hint">
                    Участниками проекта могут стать инноваторы в возрасте от 14 до 30 лет
                </div>
            </div>
        </div>

		<?php if ($arResult["LEADER_USER"]): ?>
            <div class="input-field row">
                <div class="col-md-2 col-sm-3 col-12 align-self-center">
                    <label for="form-profile-5"><?= GetMessage('USER_CITY') ?></label>
                </div>

                <div class="col-md-3 col-sm-5 col-12 align-self-center">
                    <input id="form-profile-5" class="input" name="PERSONAL_CITY" type="text"
                           value="<?= $arResult["arUser"]["PERSONAL_CITY"] ?>" <?=$arResult["IS_READONLY"] ? "readonly" : ""?>>
                </div>

                <div class="col-md-6 align-self-center d-none d-md-block">
                    <div class="input-field__hint">
                        Укажите регион и населенный пункт проживания
                    </div>
                </div>
            </div>

            <div class="input-field row">
                <div class="col-md-3 col-sm-5 col-12 offset-md-2 offset-sm-3 align-self-center">

                    <?php if ($arResult["arUser"]["PERSONAL_PHOTO"]): ?>
                        <div class="form-profile__image preview-image"
                             style="background-image: url('<?= CFile::GetPath($arResult["arUser"]["PERSONAL_PHOTO"]) ?>')"></div>
                    <?php endif ?>

                    <?if (!$arResult["IS_READONLY"]):?>
                        <input id="form-profile-6" class="input input_file" name="PERSONAL_PHOTO" type="file" placeholder="Загрузить фотографию">
                        <label for="form-profile-6">Загрузить фотографию</label>
                    <?endif;?>
                </div>
            </div>

            <div class="input-field row">
                <div class="col-md-3 col-sm-5 col-12 offset-md-2 offset-sm-3 align-self-center">
                    <input type="hidden" value="0" name="UF_PERSONAL_SPEAK_EN">
                    <input id="form-sign-up-7" class="input input_checkbox" type="checkbox" value="1" name="UF_PERSONAL_SPEAK_EN" <?=$arResult["arUser"]["UF_PERSONAL_SPEAK_EN"] ? "checked='checked'" : ''?> <?=$arResult["IS_READONLY"] ? "disabled" : ""?>>
                    <label for="form-sign-up-7">Я говорю по-английски</label>
                </div>

                <div class="col-md-6 align-self-center d-none d-md-block">
                    <div class="input-field__hint">
                        В этом случае ваш проект сможет курировать англоязычный эксперт
                    </div>
                </div>
            </div>


            <div class="input-field row">
                <div class="col-md-2 col-sm-3 col-12 align-self-center">
                    <label for="form-profile-9"><?=GetMessage('NEW_PASSWORD_REQ')?></label>
                </div>

                <div class="col-md-3 col-sm-5 col-12 align-self-center">
                    <input id="form-profile-9" class="input" name="NEW_PASSWORD" autocomplete="off" type="password">
                </div>
            </div>

            <div class="input-field row">
                <div class="col-md-2 col-sm-3 col-12 align-self-center">
                    <label for="form-profile-10"><?=GetMessage('NEW_PASSWORD_CONFIRM')?></label>
                </div>

                <div class="col-md-3 col-sm-5 col-12 align-self-center">
                    <input id="form-profile-10" class="input" name="NEW_PASSWORD_CONFIRM" type="password" autocomplete="off">
                </div>
            </div>

            <div class="input-field row">
                <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                    <input type="hidden" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                    <button type="submit" class="btn"><?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?></button>
                    <?$APPLICATION->IncludeFile("/inc/policy.php", Array(), Array()); ?>
                </div>
            </div>
		<?php endif ?>
    </div>
</form>
</div>