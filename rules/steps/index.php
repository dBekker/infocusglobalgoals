<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Правила участия — InFocus");
$APPLICATION->SetTitle("Правила участия");
?>
<div class="row">
	<div class="col col-12">
	    <h1><?$APPLICATION->ShowTitle(false)?></h1>

	    <?$APPLICATION->IncludeFile("/inc/submenu.php", Array(), Array()); ?>

	    <div class="content">
	        <div class="row">
	            <div class="col col-md-9 col-12">
	                <p>
	                    <?$APPLICATION->IncludeComponent(
	                        "bitrix:main.include",
	                        "",
	                        Array(
	                            "AREA_FILE_SHOW" => "page",
	                            "AREA_FILE_SUFFIX" => "inc",
	                            "EDIT_TEMPLATE" => ""
	                        )
	                    );?>
	                </p>
	                <p>
                        Вот что нужно сделать:
	                </p>
	            </div>
	        </div>
	    </div>

	    <div class="procedure">
	        <div class="procedure__element row">
	            <div class="col col-md-7 col-12">

	                <div class="procedure__title">
	                    <span class="procedure__count procedure__count_one">1</span>
	                    Сформировать молодежную команду
	                </div>

	                <div class="procedure__description">
	                    от 2-х до 10 человек
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 offset-md-3 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">2</span>
	                    Зарегистрироваться на сайте
	                </div>

	                <div class="procedure__description">
                        рассказав о себе и своих друзьях, которых ты берешь в команду
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">3</span>
	                    Определить проблему, на решение которой будет направлен проект
	                </div>

	                <div class="procedure__description">
                        При создании проекта ты сможешь выбрать самую интересную для себя и команды тему из списка <a href="/rules/activities/">17-ти Целей устойчивого развития</a>
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 offset-md-3 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">4</span>
	                    Описать свой проект, заполнив все поля online-заявки в Личном кабинете
	                </div>

	                <div class="procedure__description">
	                    <p>
	                        Проекты могут:
	                    </p>

	                    <ul>
	                        <li>
	                            быть уже реализованными, но требующими доработки и улучшения для расширения географии их реализации и трансляции в другие регионы и
	                            страны мира;
	                        </li>

	                        <li>
	                            находиться на стадии реализации;
	                        </li>

	                        <li>
	                            находиться на стадии идеи. В данном случае, проект должен иметь четко обозначенную проблему и прописанный механизм ее решения.
	                        </li>
	                    </ul>

	                    <p>
	                        Обращаем Ваше внимание на то, что заявка оформляется на русском и (или) на английском языке. Название и краткое описание проекта должны
	                        быть
	                        оформлены на двух языках. Название и краткое описание размещаются на платформе в публичном доступе.
	                    </p>

	                    <p>
	                        После заполнения online-заявки проект проходит первичную проверку на соответствие основным требованиям. Если во время проверки в проекте
	                        были выявлены недостатки (неверно или не в полном объеме заполненные поля, расхождение в цели и методах ее достижения и т.п.), проект
	                        отправляется на доработку. Проект может быть доработан не более 3-х раз. Если после 3-го раза проект так и не прошел проверку, он не
	                        допускается к дальнейшему участию в Молодежной сети #INFOCUS.
	                    </p>
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">5</span>
	                    Получить эксперта-наставника
	                </div>

	                <div class="procedure__description">
	                    Проекты, прошедшие проверку, отправляются <a href="/experts/">экспертам</a>. Эксперты знакомятся с предложенными проектами и выбирают те, наставниками которых они
	                    хотели бы быть. Информация о том, какой эксперт выбрал проект, отображается в Личных кабинетах участников.
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 offset-md-3 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">6</span>
	                    Приступить к реализации проекта под руководством эксперта-наставника
	                </div>

	                <div class="procedure__description">
						Работа над проектом будет длиться в течение 24 недель, затем проект будет оценен экспертами и всеми желающими. По итогам экспертного и публичного голосования определятся победители, которые будут приглашены для участия в Международной конференции #INFOCUS в женевском Отделении Организации Объединенных Наций в июне 2019 года. Именно здесь команды смогут лично познакомятся со своими наставниками, экспертами международного уровня, найти партнеров для дальнейшего развития проектов.
                    </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>