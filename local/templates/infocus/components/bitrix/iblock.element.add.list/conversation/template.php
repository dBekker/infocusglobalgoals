<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

$colspan = 2;
if ($arResult["CAN_EDIT"] == "Y") $colspan++;
if ($arResult["CAN_DELETE"] == "Y") $colspan++;
?>
<?if (count($arResult["ELEMENTS"]) > 0 || $arParams["ALLOW_ADD"] !="N"):?>
    <h2>Общения</h2>
<?endif;?>

<? if($arParams["ALLOW_EDIT"] == "Y" && $arParams["ALLOW_ADD"]!="N"):?>
    <a class="btn" href="<?=$arParams["EDIT_URL"]?>?edit=Y">Добавить запись</a>
<?endif?>

<div class="project-report__wrap">
    <?if($arResult["NO_USER"] == "N"):?>
        <?if (count($arResult["ELEMENTS"]) > 0):?>
            <?foreach ($arResult["ELEMENTS"] as $arElement):?>
                <div class="project-report__element">
                    <div class="project-report_date"><nobr><b>Дата общения с куратором:</b></nobr> <?=$arElement["PROPERTIES"]["DATE"]?></div>
                    <div class="project-report_port"><nobr><b>Способ общения:</b></nobr> <?=$arElement["PROPERTIES"]["TYPE"]?></div>

                    <div class="project-report__content content">
                        <div class="project-report__content-title"><b>Результаты общения (вопросы и решения)</b></div>
                        <?=$arElement["PROPERTIES"]["TEXT"];?>
                    </div>

                    <?if ($arResult["CAN_EDIT"] == "Y"):?>
                        <div class="project-report_action">
                            <a href="<?=$arParams["EDIT_URL"]?>?edit=Y&amp;CODE=<?=$arElement["ID"]?>">Редактировать</a>
                        </div>
                    <?endif?>
                </div>
            <?endforeach?>
        <?endif?>
    <?endif?>
</div>

