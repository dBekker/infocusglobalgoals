<?
$MESS["IBLOCK_ADD_ERROR_REQUIRED"] = "Поле '#PROPERTY_NAME#' должно быть заполнено!";
$MESS["IBLOCK_ADD_ELEMENT_NOT_FOUND"] = "Элемент не найден";
$MESS["IBLOCK_ADD_ACCESS_DENIED"] = "Нет доступа";
$MESS["IBLOCK_FORM_WRONG_CAPTCHA"] = "Неверно введено слово с картинки";
$MESS["IBLOCK_ADD_MAX_ENTRIES_EXCEEDED"] = "Превышено максимальное количество записей";
$MESS["IBLOCK_ADD_MAX_LEVELS_EXCEEDED"] = "Превышено максимальное количество разделов - #MAX_LEVELS#";
$MESS["IBLOCK_ADD_LEVEL_LAST_ERROR"] = "Разрешено добавление только в разделы последнего уровня";
$MESS["IBLOCK_USER_MESSAGE_ADD_DEFAULT"] = "Заявка на создание проекта отправлена на модерацию. Мы сообщим вам о результатах ее рассмотрения";
$MESS["IBLOCK_USER_MESSAGE_EDIT_DEFAULT"] = "Исправления приняты и отправлены на модерацию. Мы сообщим вам о результатах рассмотрения";
$MESS["IBLOCK_ERROR_FILE_TOO_LARGE"] = "Размер загруженного файла превышает допустимое значение";
$MESS["CC_BIEAF_ACCESS_DENIED_STATUS"] = "У вас нет прав на редактирование данной записи в ее текущем статусе";
$MESS["CC_BIEAF_IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль Информационных блоков не установлен";

$MESS["PROJECT_STATUS_PROCESS"] = "На рассмотрении";
$MESS["PROJECT_STATUS_ERRORS"] = "Необходимы доработки";
$MESS["PROJECT_STATUS_SUCCESS"] = "Заявка одобрена";
$MESS["PROJECT_STATUS_REJECT"] = "Заявка отклонена";

?>