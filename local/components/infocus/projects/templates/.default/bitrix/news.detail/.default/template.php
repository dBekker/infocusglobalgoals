<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$expert_points = $arResult["PROPERTIES"]["POINTS_EXPERT"]["VALUE"];
$vote_points = $arResult["PROPERTIES"]["POINTS_VOTE"]["VALUE"] ? $arResult["PROPERTIES"]["POINTS_VOTE"]["VALUE"] : 0;
$total_points = $expert_points + $vote_points;
?>

<section class="project-header">
    <div class="row">
        <div class="col col-md-7 col-12">
            <h1 class="project-header__title"><?=$arResult["NAME"]?></h1>

            <div class="project-points">
				<?php if ($total_points): ?>
                    <div class="project-points__result">
                        <div class="project-points__count"><?=$total_points?></div>
                        <div class="project-points__title"><?=App\Helpers::formatSay($total_points, [" балл", " балла", " баллов"])?></div>
                    </div>
				<?php endif ?>

				<?php if ($total_points): ?>
                    <div class="project-points__info">
						<?if($expert_points):?>
                            <?= $expert_points ?><?=App\Helpers::formatSay($expert_points, [" балл", " балла", " баллов"])?> от эксперта<br>
                        <?endif;?>
                        <?if($vote_points):?>
                            и <?= $vote_points ?><?=App\Helpers::formatSay($vote_points, [" балл", " балла", " баллов"])?> по итогам голосования
                        <?endif;?>
                    </div>
				<?php endif ?>
            </div>

            <div class="project-tags">
                <div class="project-tags__team icon icon_to-text icon_not-hover icon-team"><?=$arResult["TEAM_PROP"]["NAME"]?></div>

                <div class="project-tags__position"><?=$arResult["DISPLAY_PROPERTIES"]["GOAL"]["VALUE"]?></div>

				<?php if ($arResult["USER_PROPS"]["SPEAK_EN"]): ?>
                    <div class="project-tags__lang">Английский язык</div>
				<?php endif ?>

                <div class="project-tags__back"><a href="../">Другие проекты</a></div>
            </div>
        </div>
    </div>

    <div class="project-header__image" style="background-image: url('/templates/pics/project.jpg')"></div>
</section>

<?php if ($arResult["EXPERTS"]["IS_ME"]): ?>
    <div class="project-edit">
        <div class="row">
            <div class="col col-md-8 col-12">
                <h2>Редактирование проекта</h2>

				<?php if (!$expert_points && $arResult["EXPERTS"]["IS_ME"]): ?>
                    <div class="project-edit__points">
                        <h3>Баллы</h3>

                        <form id="form-points" class="form form-points" action="/ajax/setPoints.php" method="post">
                            <div class="form__content">
                                <div class="input-field input-field_inline">
                                    <input class="input" name="project" type="hidden" value="<?= $arResult['ID'] ?>">
                                    <input class="input" name="points" type="number"
                                           placeholder="Введите количество баллов"
                                           required>
                                </div>

                                <div class="input-field  input-field_inline">
                                    <button class="btn" type="submit">Добавить</button>
                                </div>
                            </div>

                            <div class="form__loading form__loading_text-center">
                                <div class="loader loader_dark"></div>
                            </div>

                            <div class="form__success">
                                <p>
                                    Баллы добавлены.
                                </p>
                            </div>

                            <div class="form__error">
                                <p>
                                    Извините, произошла ошибка. Попробуйте отправить сообщение позже.
                                </p>
                            </div>
                        </form>
                    </div>
				<?php endif ?>
            </div>

            <div class="col col-md-8 col-12">
                <div class="project-edit__report project-report">
					<? $APPLICATION->IncludeComponent(
						"infocus:iblock.message.add",
						"",
						Array(
							"AJAX_MODE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "N",
							"ALLOW_DELETE" => "N",
							"ALLOW_EDIT" => "Y",
							"ALLOW_ADD" => "N",
							"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
							"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
							"CUSTOM_TITLE_DETAIL_PICTURE" => "",
							"CUSTOM_TITLE_DETAIL_TEXT" => "",
							"CUSTOM_TITLE_IBLOCK_SECTION" => "",
							"CUSTOM_TITLE_NAME" => "",
							"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
							"CUSTOM_TITLE_PREVIEW_TEXT" => "",
							"CUSTOM_TITLE_TAGS" => "",
							"DEFAULT_INPUT_SIZE" => "30",
							"DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
							"ELEMENT_ASSOC" => "CREATED_BY",
							"ELEMENT_ASSOC_PROPERTY" => "",
							"GROUPS" => array("1", "7"),
							"IBLOCK_ID" => "4",
							"IBLOCK_TYPE" => "Competition",
							"LEVEL_LAST" => "Y",
							"MAX_FILE_SIZE" => "0",
							"MAX_LEVELS" => "100000",
							"MAX_USER_ENTRIES" => "100000",
							"NAV_ON_PAGE" => "10",
							"PREVIEW_TEXT_USE_HTML_EDITOR" => "Y",
							"PROPERTY_CODES" => array("34", "35", "PREVIEW_TEXT"),
							"PROPERTY_CODES_REQUIRED" => array("34", "35", "PREVIEW_TEXT"),
							"RESIZE_IMAGES" => "N",
							"SEF_FOLDER" => "/profile-expert/projects/",
							"SEF_MODE" => "Y",
							"STATUS" => "ANY",
							"STATUS_NEW" => "N",
							"USER_MESSAGE_ADD" => "",
							"USER_MESSAGE_EDIT" => "",
							"USE_CAPTCHA" => "N",
							"CREATED_BY" => $arResult['CREATED_BY'],
							"CUSTOM_TITLE_PREVIEW_TEXT" => "Результаты общения (вопросы и решения)",
						)
					); ?>
                </div>
            </div>

            <div class="col col-md-4 col-12">
                <div class="project-files">
					<?$APPLICATION->IncludeComponent(
						"bitrix:iblock.element.add.list",
						"files",
						Array(
							"ALLOW_DELETE" => "N",
							"ALLOW_EDIT" => "N",
							"EDIT_URL" => "file",
							"ELEMENT_ASSOC" => "CREATED_BY",
							"CREATED_BY" => array($arResult["CREATED_BY"]),
							"GROUPS" => array("1","7","9"),
							"IBLOCK_ID" => "5",
							"IBLOCK_TYPE" => "Competition",
							"MAX_USER_ENTRIES" => "100000",
							"NAV_ON_PAGE" => "10",
							"SEF_MODE" => "N",
							"STATUS" => "ANY"
						)
					);?>
                </div>
            </div>

        </div>
    </div>
<?php endif ?>