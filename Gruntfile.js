module.exports = function (grunt) {

	require('time-grunt')(grunt);
	require('jit-grunt')(grunt);

	grunt.initConfig({

		/*Copy for watch task*/
		copy: {
			main: {
				files: [
					{
						expand: true,
						cwd: 'dev/js/vendor/',
						src: ['**/*.js'],
						dest: 'build/js/vendor/'
					}
				]
			},
			templates: {
				expand: true,
				cwd: 'dev/js/app/templates/',
				src: ['**/*.html'],
				dest: 'templates/app'
			}
		},

		ngAnnotate: {
			dist: {
				files: [{
					expand: true,
					src: ['dev/js/app/**/*.js'],
					extDot: 'last'
				}]
			}
		},

		/*Compress JS*/
		uglify: {
			options: {
				mangle: true
			},
			compress: {
				files: {
					'build/js/main.min.js': ['dev/js/vendor/jquery-3.3.1.min.js', 'dev/js/vendor/angular.min.js', 'dev/js/vendor/angular-ui-router.min.js', 'dev/js/libs/**/*.js', 'dev/js/app/**/*.js', 'dev/js/main.js', 'dev/js/components/**/*.js']
				}
			}
		},

		/*Concat JS for watch JS*/
		concat: {
			dist: {
				files: {
					'build/js/main.min.js': ['dev/js/vendor/jquery-3.3.1.min.js', 'dev/js/vendor/angular.min.js', 'dev/js/vendor/angular-ui-router.min.js', 'dev/js/libs/**/*.js', 'dev/js/app/**/*.js', 'dev/js/main.js', 'dev/js/components/**/*.js']
				}
			}
		},

		/*Sass*/
		sass: {
			dist: {
				options: {
					outputStyle: 'compressed',
					sourceMap: false
				},
				files: {
					"build/css/main.min.css": ["dev/sass/main.scss"]
				}
			},
			fast: {
				options: {
					outputStyle: 'nested',
					sourceMap: false
				},
				files: {
					"build/css/main.min.css": ["dev/sass/main.scss"]
				}
			}
		},

		/*Watch for file changes in dev/ */
		watch: {
			css: {
				files: 'dev/sass/**',
				tasks: ['sass:fast']
			},
			scripts: {
				files: 'dev/js/**',
				tasks: ['concat', 'copy:main', 'copy:templates']
			}
		}
	});

	grunt.registerTask('default', ['concat', 'copy:main', 'copy:templates', 'sass:fast']);
	grunt.registerTask('release', ['ngAnnotate', 'uglify', 'copy:main', 'copy:templates', 'sass:dist']);

};