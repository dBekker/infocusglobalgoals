<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<div class="row">
	<div class="col col-12">
		<?$arrFilter = array("ID" => $_GET["USER_ID"]);?>
		<?$APPLICATION->IncludeComponent(
			"dev2fun:user.list",
			"detail",
			Array(
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"AJAX_MODE" => "N",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"LIST_URL" => "/experts/",
				"FIELD_CODE" => array("ID","NAME","EMAIL","PERSONAL_PHOTO","PERSONAL_BIRTHDAY","PERSONAL_PROFESSION", "WORK_POSITION", "WORK_NOTES", "PERSONAL_NOTES", "WORK_PROFILE"),
				"FILTER_NAME" => "arrFilter",
				"PROPERTY_CODE" => array("UF_PERSONAL_SPEC","UF_USER_COUNTRY", "UF_ASSIGNED_PROJ"),
				"RESIZE_PERSONAL_PHOTO" => "500*600",
				"RESIZE_TYPE" => "BX_RESIZE_IMAGE_PROPORTIONAL",
				"RESIZE_WORK_LOGO" => "500*600",
				"SET_STATUS_404" => "Y",
			)
		);?>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>