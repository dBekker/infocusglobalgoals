<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


foreach ($arResult["ITEMS"] as $key => $arItem) {

	/* Team's properties*/
	$arSelect = Array();
	$arFilter = Array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "CREATED_BY" => $arItem["CREATED_BY"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	if ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();

		$arResult["ITEMS"][$key]['TEAM_PROP'] = array(
			"NAME" => $arFields['NAME']
		);
	}

	$arResult["ITEMS"][$key]['USER_PROPS'] = array(
		"SPEAK_EN" => $arUserLeader["UF_PERSONAL_SPEAK_EN"]
	);

	// Sum Points
	$arResult["ITEMS"][$key]["PROPERTIES"]["SUM_POINT"] = $arItem["PROPERTIES"]["POINTS_EXPERT"]["VALUE"] + $arItem["PROPERTIES"]["POINTS_VOTE"]["VALUE"];

	// Expert props
	$rsUser = CUser::GetByID($arItem["DISPLAY_PROPERTIES"]["EXPERT"]["VALUE"]);
	$arUser = $rsUser->Fetch();
	$arResult["ITEMS"][$key]["PROPERTIES"]['EXPERT'] = $arUser["NAME"];
};
?>