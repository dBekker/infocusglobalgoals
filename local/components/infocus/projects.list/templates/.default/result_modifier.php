<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$rsUserExpert = CUser::GetByID($USER->GetID());
$arUserExpert = $rsUserExpert->Fetch();


foreach ($arResult["ITEMS"] as $key => $arItem) {
	$rsUserLeader = CUser::GetByID($arItem["CREATED_BY"]);
	$arUserLeader = $rsUserLeader->Fetch();

	/*Add Project props*/
	$arResult["ITEMS"][$key]["EXPERTS"] = Array(
		"HAS_EXPERT" => $arItem["DISPLAY_PROPERTIES"]["EXPERT"]["VALUE"],
		"IS_ME" => in_array($arItem["ID"], $arUserExpert["UF_ASSIGNED_PROJ"]) ? "Y" : ""
	);

	/* Team's properties*/
	$arSelect = Array();
	$arFilter = Array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "CREATED_BY" => $arUserLeader["ID"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	if ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();

		$arResult["ITEMS"][$key]['TEAM_PROP'] = array(
			"NAME" => $arFields['NAME']
		);
	}

	$arResult["ITEMS"][$key]['USER_PROPS'] = array(
		"SPEAK_EN" => $arUserLeader["UF_PERSONAL_SPEAK_EN"]
	);

};


/*Bubble project with current expert*/
foreach ($arResult["ITEMS"] as $key => $arItem){
	if($arItem["DISPLAY_PROPERTIES"]["EXPERT"]["VALUE"] == $USER->GetID()){
		$projectOfExpert = $arItem;
		array_splice($arResult["ITEMS"], $key, 1);
		array_unshift($arResult["ITEMS"], $projectOfExpert);
	}
}

?>