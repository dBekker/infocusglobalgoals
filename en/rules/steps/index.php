<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Rules — InFocus");
$APPLICATION->SetTitle("Rules");
?>
<div class="row">
	<div class="col col-12">
	    <h1><?$APPLICATION->ShowTitle(false)?></h1>

		<?$APPLICATION->IncludeFile("/en/inc/submenu.php", Array(), Array()); ?>

	    <div class="content">
	        <div class="row">
	            <div class="col col-md-9 col-12">
	                <p>
	                    <?$APPLICATION->IncludeComponent(
	                        "bitrix:main.include",
	                        "",
	                        Array(
	                            "AREA_FILE_SHOW" => "page",
	                            "AREA_FILE_SUFFIX" => "inc",
	                            "EDIT_TEMPLATE" => ""
	                        )
	                    );?>
	                </p>
	                <p>
                        Here's what you need to do:
	                </p>
	            </div>
	        </div>
	    </div>

	    <div class="procedure">
	        <div class="procedure__element row">
	            <div class="col col-md-7 col-12">

	                <div class="procedure__title">
	                    <span class="procedure__count procedure__count_one">1</span>
	                    Build a youth team 
	                </div>

	                <div class="procedure__description">
	                    of 2 to 10 people
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 offset-md-3 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">2</span>
	                    Sign up on our website
	                </div>

	                <div class="procedure__description">
                        and tell us about yourself and your friends who will be on the team
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">3</span>
	                    Specify the issue that the project will aim to deal with
	                </div>

	                <div class="procedure__description">
                        When creating your project, you can choose the from <a href="/rules/activities/">the 17 Sustainable Development Goals</a> to find a topic that excites your team the most
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 offset-md-3 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">4</span>
	                    Describe your project by filling out all the fields of the online application in Your Account
	                </div>

	                <div class="procedure__description">
	                    <p>
	                        Projects can be one of the following:
	                    </p>

	                    <ul>
	                        <li>
	                            Completed but require improvement to expand their geographical scope and transfer to other regions and countries of the world;
	                        </li>

	                        <li>
	                            In the implementation stage;
	                        </li>

	                        <li>
	                            In the idea stage. In the latter case, your project should focus on a specific issue and have a well-thought-out solution to it.
	                        </li>
	                    </ul>

	                    <p>
	                        Please note that the application form must be filled out in Russian and/or English. The project synopsis and title must be written in both languages. The project's title and summary are publicly accessible on the platform.
						</p>

	                    <p>
	                        After submission of the application form filled out online, the project is reviewed to see if it meets the basic requirements. If the verification procedure has revealed any deficiencies (incorrectly or incompletely filled out fields, differences in the objective and ways of achieving it, etc.), the project is sent back for revision. The project may be revised a maximum of 3 times. If after 3 revisions the project has not been approved, it shall be denied access to participation in the #INFOCUS Youth Network.
	                    </p>
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">5</span>
	                    Find an expert adviser
	                </div>

	                <div class="procedure__description">
						Approved projects are sent to <a href="/en/experts/">experts</a>. Experts familiarize themselves with the proposed projects and choose the ones they would like to supervise. Information about which expert has chosen a particular project is visible in participants' Accounts.
	                </div>
	            </div>
	        </div>

	        <div class="procedure__element row">
	            <div class="col col-md-7 offset-md-3 col-12">
	                <div class="procedure__title">
	                    <span class="procedure__count">6</span>
	                    Start working on the project under the guidance of your expert adviser
	                </div>

	                <div class="procedure__description">
                        Work on the project shall last 24 weeks. Then the project will be assessed by experts and all others wishing to do so. Expert and public voting will determine the winners who will then be invited to participate in the international conference #INFOCUS in United Nations Office in Geneva, in June 2019. Winning projects will receive support in transferring to Russian regions and countries interested in their implementation.
                    </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>