<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наш проект");
?>
<div class="row">
    <div class="col col-12">
        <h1><?$APPLICATION->ShowTitle(false)?></h1>

        <?$APPLICATION->IncludeFile("/inc/submenu.php", Array(), Array()); ?>

        <?$APPLICATION->IncludeComponent(
        "infocus:iblock.project.add.form",
        "",
        Array(
            "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
            "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
            "CUSTOM_TITLE_DETAIL_PICTURE" => "",
            "CUSTOM_TITLE_DETAIL_TEXT" => "",
            "CUSTOM_TITLE_IBLOCK_SECTION" => "",
            "CUSTOM_TITLE_NAME" => "Название проекта",
            "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
            "CUSTOM_TITLE_PREVIEW_TEXT" => "",
            "CUSTOM_TITLE_TAGS" => "",
            "DEFAULT_INPUT_SIZE" => "30",
            "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
            "ELEMENT_ASSOC" => "CREATED_BY",
            "GROUPS" => array("1","9","8"),
            "IBLOCK_ID" => "3",
            "IBLOCK_TYPE" => "Competition",
            "LEVEL_LAST" => "Y",
            "LIST_URL" => "",
            "MAX_FILE_SIZE" => "0",
            "MAX_LEVELS" => "100000",
            "MAX_USER_ENTRIES" => "100000",
            "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
            "PROPERTY_CODES" => array("11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","30","NAME"),
            "PROPERTY_CODES_REQUIRED" => array("11","12","13","15","17","18","19","20","21","23","25","NAME"),
            "PROPERTY_CODES_GROUP_1" => array("NAME", "11", "12", "13", "14", "15", "16", "17"),
            "PROPERTY_CODES_GROUP_2" => array("18", "19", "20", "21", "22", "23", "24", "25"),
            "RESIZE_IMAGES" => "Y",
            "SEF_MODE" => "N",
            "STATUS" => "",
            "STATUS_NEW" => "NEW",
            "USER_MESSAGE_ADD" => "",
            "USER_MESSAGE_EDIT" => "",
        )
    );?>
    </div>
</div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>