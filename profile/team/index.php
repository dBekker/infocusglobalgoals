<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Моя команда");
?>
<div class="row">
    <div class="col col-12">
        <h1><?$APPLICATION->ShowTitle(false)?></h1>
         <?$APPLICATION->IncludeFile("/inc/submenu.php", Array(), Array()); ?>

         <?$APPLICATION->IncludeComponent(
        "infocus:iblock.team.add.form",
        "",
        Array(
            "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
            "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
            "CUSTOM_TITLE_DETAIL_PICTURE" => "",
            "CUSTOM_TITLE_DETAIL_TEXT" => "",
            "CUSTOM_TITLE_IBLOCK_SECTION" => "",
            "CUSTOM_TITLE_NAME" => "Название команды",
            "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
            "CUSTOM_TITLE_PREVIEW_TEXT" => "Дополнительная информация о команде",
            "CUSTOM_TITLE_TAGS" => "",
            "DEFAULT_INPUT_SIZE" => "30",
            "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
            "ELEMENT_ASSOC" => "CREATED_BY",
            "ELEMENT_ASSOC_PROPERTY" => "",
            "GROUPS" => array("1","9"),
            "IBLOCK_ID" => "1",
            "IBLOCK_TYPE" => "Competition",
            "LEVEL_LAST" => "Y",
            "LIST_URL" => "",
            "MAX_FILE_SIZE" => "0",
            "MAX_LEVELS" => "100000",
            "MAX_USER_ENTRIES" => "100000",
            "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
            "PROPERTY_CODES" => array("NAME", "1","2","3","4","8","PREVIEW_TEXT","7"),
            "PROPERTY_CODES_REQUIRED" => array("1","2","3","8","NAME"),
            "RESIZE_IMAGES" => "N",
            "SEF_MODE" => "N",
            "STATUS" => "ANY",
            "STATUS_NEW" => "N",
            "USER_MESSAGE_ADD" => "added",
            "USER_MESSAGE_EDIT" => "updated",
            "USE_CAPTCHA" => "N",
        )
    );?>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>