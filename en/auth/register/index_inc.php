We invite team leaders to sign up on our website to then be able to add other team members to the project.<br>

Want to participate in a project but nobody invited you to join a team? Just contact the team leader.<br>

<a class="popup-open" href="javascript:void(0)" rel="popup-video" aria-label="Открыть">Video</a>

<div class="layout"></div>

<div class="popup" id="popup-video">
    <button class="popup__close icon-popup-close" type="button" aria-label="Закрыть"></button>

    <div class="popup__container">
        <iframe width="100%" height="479" src="https://www.youtube.com/embed/dTPT7wK8t4U?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media"
                allowfullscreen></iframe>
    </div>
</div>
