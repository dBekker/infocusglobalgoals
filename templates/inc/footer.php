<footer class="page-footer">
    <div class="footer-social line-align">
        <div class="container">
            <ul class="footer-social__wrap line-align__wrap">
                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_vk" href="#!" target="_blank" rel="noopener">vk.com</a></li>
                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_ok" href="#!" target="_blank" rel="noopener">ok.ru</a></li>
                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_fb" href="#!" target="_blank" rel="noopener">facebook.com</a></li>
                <li class="footer-social__item line-align__item"><a class="icon icon-social icon-social_in" href="#!" target="_blank" rel="noopener">instagram.com</a></li>
            </ul>
        </div>
    </div>

    <div class="footer-info vcard">
        <div class="container">
            <div class="row">
                <div class="col col-sm-4 col-12 footer-info__col">
                    Национальная ассоциация развития образования &laquo;Тетрадка Дружбы&raquo;
                </div>

                <div class="col col-sm-4 col-12 footer-info__col">
                    <address>
                        <span class="adr"><span class="locality">Пермь</span>, <span class="street-address">Петропавловская, 185</span></span><br>
                        <a class="tel" href="tel:+74952412697">(495) 24-12-697</a><br>
                        <a class="email" href="mailto:info@tetradka.org.ru">info@tetradka.org.ru</a>
                    </address>
                </div>

                <div class="col col-sm-4 col-12 footer-info__col">
                    <div class="footer-info__links">
                        <a href="#!">Политика конфиденциальности</a><br>
                        <a href="#!">Пользовательское соглашение</a><br>
                        <a href="http://rktv.ru/" target="_blank" rel="noopener">Разработано в Реактиве</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>