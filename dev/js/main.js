/**
 *  Ширина скрола
 */
function scrollbarWidth() {
	var $div = document.createElement('div'),
		$body = $('body');

	$div.style.overflowY = 'scroll';
	$div.style.width = '50px';
	$div.style.height = '50px';
	$div.style.visibility = 'hidden';

	document.body.appendChild($div);
	var scrollWidth = $div.offsetWidth - $div.clientWidth;
	document.body.removeChild($div);

	if ($body.get(0).scrollHeight > $body.height()) {
		return scrollWidth;
	} else {
		return 0;
	}
}

/**
 * Form validate and send
 *
 * @param $this {object}
 */
function masterForm($this) {
	var $form = {
		action: $this.attr('action'),
		method: $this.attr('method'),
		ajax: $this.data('ajax') !== false ? true : void 0
	};

	function validateField($element) {
		var $field = {
			type: $element.attr('type'),
			val: $element.val()
		};

		switch($field.type){
			case 'text':
				return $field.val.replace(/\s+/g, '');
				break;
			case 'email':
				var pattern = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

				return pattern.test($field.val);
				break;
			default:
				return $field.val.replace(/\s+/g, '');
				break;
		}
	}

	function validateForm() {
		var $inputs = $this.find('input, select, textarea').filter('[required]:visible'),
			$error = false;

		$inputs.removeClass('input_wrong');

		$inputs.each(function () {
			var $this = $(this);

			if (!validateField($this)) {
				$this.addClass('input_wrong');
				$error = true;
			}
		});

		return $error
	}

	$this.on('submit', function (event) {

		if (!validateForm()) {
			if ($form.ajax) {
				$this.addClass('form_loading');

				$.ajax({
					type: $form.method,
					url: $form.action,
					data: new FormData($this[0]),
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						var $date = JSON.parse(data);

						$this.removeClass('form_loading');

						if ($date.response === 'success') {
							$this.addClass('form_success');
							$this[0].reset();
						}

						if ($date.response === 'already') {
							$this.addClass('form_already');

							setTimeout(function () {
								$this.removeClass('form_already');
							}, 6000);
						}

						if ($date.response !== 'success' && $date.response !== 'already') {
							$this.addClass('form_error');

							setTimeout(function () {
								$this.removeClass('form_error');
							}, 6000);
						}
					},
					error: function () {
						$this.removeClass('form_loading');
						$this.addClass('form_error');

						setTimeout(function () {
							$this.removeClass('form_error');
						}, 6000);
					}
				});
			} else {
				return true
			}
		}

		event.preventDefault();
	});
}

/**
 *  Open object popup
 */
function openObject($object, $layout, $scrollPos) {
	var $pageWrap = $('.page-wrapper'),
		$scrollbarWidth = scrollbarWidth();

	$pageWrap.css({'margin-top': -$scrollPos, 'margin-right': $scrollbarWidth});

	$('html').addClass('scroll-disable');

	$layout.addClass($layout[0].classList[0] + '_visible');
	$object.addClass($object[0].classList[0] + '_visible');
}

/**
 *  Close object popup
 */
function closeObject($object, $layout, $scrollPos) {
	var $pageWrap = $('.page-wrapper'),
		$formObject = $object.find('form');

	if ($object.hasClass($object[0].classList[0] + '_visible')) {
		$object.removeClass($object[0].classList[0] + '_visible');
		$layout.removeClass($layout[0].classList[0] + '_visible');

		setTimeout(function () {
			if ($formObject.length > 0) {
				$formObject.removeClass('form_success');
			}

			$('html').removeClass('scroll-disable');

			$pageWrap.css({'margin-top': 0, 'margin-right': 0});

			$(window).scrollTop($scrollPos);
		}, 410);
	}
}

/**
 * Кастомный input file
 */
$(function () {
    var $inputFile = $('.input_file');

    $inputFile.on('change', function () {
        var $this = $(this)[0].files[0],
            $label = $("[for=" + $(this).attr('id') + "]"),
            $placeholder = $(this).attr('placeholder'),
			$preview = $(this).parent('div').find('.preview-image');

        if ($this) {
			$preview ? $preview.hide() : void 0;

            if ($(this)[0].files.length > 1) {
                $label.html('Файлов выбранно: ' + $(this)[0].files.length);
            } else {
                $label.html('Выбран файл: ' + $this.name);
            }
        } else {
			$preview ? $preview.show() : void 0;
            $label.html($placeholder);
        }
    });
});

/**
 * Кастомный input phone
 */
$(function () {
    var $phoneInput = $('input.input_phone');

    $phoneInput.mask("+0 (000) 000-0000", {clearIfNotMatch: true});
});

/**
 * Кастомный input date
 */
$(function () {
    var $birthdayInput = $('input.input_date');

    $birthdayInput.mask("00.00.0000", {clearIfNotMatch: true});
	$birthdayInput.datepicker({
		autoClose: true
	});
});