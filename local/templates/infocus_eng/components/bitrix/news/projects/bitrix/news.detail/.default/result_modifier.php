<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/* Team's properties*/
$arSelect = Array();
$arFilter = Array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "CREATED_BY" => $arResult["CREATED_BY"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
if ($ob = $res->GetNextElement()) {
	$arFields = $ob->GetFields();

	$arResult["TEAM_PROP"] = array(
		"NAME" => $arFields['NAME']
	);
}

$arResult["USER_PROPS"] = array(
	"SPEAK_EN" => $arUserLeader["UF_PERSONAL_SPEAK_EN"]
);

// Leaders props
$rsUser = CUser::GetByID($arResult["CREATED_BY"]);
$arUser = $rsUser->Fetch();

$arResult['LEADER_PROPS'] = array(
	"NAME"  => $arUser["NAME"],
	"PHOTO" => CFile::GetPath($arUser["PERSONAL_PHOTO"]),
	"PHONE" => $arUser["PERSONAL_PHONE"],
	"EMAIL" => $arUser["EMAIL"],
);

// Expert props
$rsUser = CUser::GetByID($arResult["PROPERTIES"]["EXPERT"]['VALUE']);
$arUser = $rsUser->Fetch();

$arResult['EXPERT_PROPS'] = array(
	"ID" => $arUser["ID"],
	"NAME" => $arUser["NAME"],
	"PHOTO" => CFile::GetPath($arUser["PERSONAL_PHOTO"])
);

?>