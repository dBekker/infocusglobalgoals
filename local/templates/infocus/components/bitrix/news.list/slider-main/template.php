<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="slider-main">
	<div class="slider-main__wrap">
		<?foreach ($arResult['ITEMS'] as  $atItem):?>
			<div class="slider-main__slide">
				<div class="row">
					<div class="col col-sm-5 col-12">
						<h1 class="slider-main__title"><?=$atItem['NAME'];?></h1>
					</div>
				</div>

				<div class="slider-main__image" style="background-image: url('<?=$atItem['PREVIEW_PICTURE']["SRC"];?>')"></div>
			</div>
		<?endforeach;?>
	</div>

	<div class="slider-main__pagination"></div>
</div>