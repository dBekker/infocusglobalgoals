<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/shrot.php"; ?>

                <div class="row">
                    <div class="col col-12">
                        <h1>Личный кабинет</h1>

                        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/nav-sub.php"; ?>

                        <div class="status status_success">
                            Заявка одобрена
                        </div>

                        <div class="project-work">
                            <div class="row">
                                <div class="col col-md-3 col-sm-5 col-12">
                                    <div class="project-lead project-lead_lk align-self-center">
                                        <div class="project-lead__info">
                                            <div class="project-lead__title">Куратор:</div>
                                            <div class="project-lead__name">Петрова Ирина Ивановна</div>
                                            <div class="project-lead__phone"><a href="tel:+79197022112">+7 (919) 702-21-12</a></div>
                                            <div class="project-lead__mail"><a href="mailto:hello@mail.ru">hello@mail.ru</a></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col col-md-5 col-sm-7 col-12 align-self-center">
                                    <div class="project-points project-points_small">
                                        <div class="project-points__result">
                                            <div class="project-points__count">180</div>
                                            <div class="project-points__title">баллов</div>
                                        </div>

                                        <div class="project-points__info">
                                            160 баллов от эксперта<br>
                                            и 20 по итогам голосования
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-md-8 col-12">
                                <div class="project-report">
                                    <h2>Общения</h2>

                                    <a class="btn" href="#!">Добавить запись</a>

                                    <div class="project-report__wrap">
                                        <div class="project-report__element">
                                            <div class="project-report_date"><nobr><b>Дата общения с куратором:</b></nobr> 15.05.2018</div>
                                            <div class="project-report_port"><nobr><b>Способ общения:</b></nobr> Телефон</div>

                                            <div class="project-report__content content">
                                                <div class="project-report__content-title"><b>Результаты общения (вопросы и решения)</b></div>
                                                Каждый раз нужно вникать в смысл текста и привязывать предлоги. Привязывать предлоги
                                                и союзы к следующему за ними слову, а частицы — к предыдущему ледующему за ними слову,
                                                а частицы — к предыдущему
                                            </div>
                                            <div class="project-report_action">
                                                <a href="#!">Редактировать</a>
                                            </div>
                                        </div>

                                        <div class="project-report__element">
                                            <div class="project-report_date"><nobr><b>Дата общения с куратором:</b></nobr> 15.05.2018</div>
                                            <div class="project-report_port"><nobr><b>Способ общения:</b></nobr> Телефон</div>

                                            <div class="project-report__content content">
                                                <div class="project-report__content-title"><b>Результаты общения (вопросы и решения)</b></div>
                                                Каждый раз нужно вникать в смысл текста и привязывать предлоги. Привязывать предлоги
                                                и союзы к следующему за ними слову, а частицы — к предыдущему ледующему за ними слову,
                                                а частицы — к предыдущему
                                            </div>
                                            <div class="project-report_action">
                                                <a href="#!">Редактировать</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-md-4 col-12">
                                <div class="project-files">
                                    <h2>Файлы</h2>

                                    <a class="btn" href="#!">Добавить файл</a>

                                    <div class="project-files__wrap">
                                        <ul>
                                            <li><a href="#!">Ссылка на файл (Pdf, 1.5 Мб)</a></li>
                                            <li><a href="#!">Ссылка на файл (Pdf, 1.5 Мб)</a></li>
                                            <li><a href="#!">Ссылка на файл (Pdf, 1.5 Мб)</a></li>
                                            <li><a href="#!">Ссылка на файл (Pdf, 1.5 Мб)</a></li>
                                            <li><a href="#!">Ссылка на файл (Pdf, 1.5 Мб)</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>