<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <h1>Личный кабинет</h1>

                        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/nav-sub.php"; ?>

                        <form id="form-project-report" class="form form-project-report" action="/templates/lk/project/report" method="post" data-ajax="false">
                            <div class="form__response">
                                Логин или пароль указаны неверно. Попробуйте ещё раз.
                            </div>

                            <div class="form__content">
                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-project-report-1">Дата общения</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-project-report-1" class="input input_date" name="date" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-project-report-2">Способ общения</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-project-report-2" class="input" name="port" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12">
                                        <label for="form-project-report-3">Результаты общения (вопросы и решения)</label>
                                    </div>

                                    <div class="col-md-5 col-sm-5 col-12 align-self-center">
                                        <textarea id="form-project-report-3" class="input input_area" name="text" required></textarea>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                                        <button class="btn" type="submit">Сохранить</button>

                                        <p>
                                            <a href="#!">Отменить редактирования</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>