<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

foreach ($arResult["ELEMENTS"] as $key => $element){
	$arSelect = Array();
	$arFilter = Array("IBLOCK_ID"=>$element['IBLOCK_ID'], "ID"=>$element['ID']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arProps = $ob->GetProperties();

		$file              = CFile::GetFileArray($arProps["FILE"]['VALUE']);
		$file['FILE_SIZE'] = CFile::FormatSize($file['FILE_SIZE']);
		$file['FILE_SIZE'] = str_replace('Б', 'б', $file['FILE_SIZE']);
		$t                 = pathinfo($file['FILE_NAME']);
		$file['EXTENSION'] = $t['extension'];

		$arResult["ELEMENTS"][$key]["PROPERTIES"]['FILE'] = $file;
	}
}
?>