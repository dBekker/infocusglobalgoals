var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	del = require('del'),
	rename = require('gulp-rename'),
	ngAnnotate = require('gulp-ng-annotate');

var paths = {
	scripts: [
		'dev/js/vendor/jquery-3.3.1.min.js',
		'dev/js/vendor/angular.min.js',
		'dev/js/vendor/angular-ui-router.min.js',
		'dev/js/libs/**/*.js',
		'dev/js/app/app.js',
		'dev/js/app/**/*.js',
		'dev/js/main.js',
		'dev/js/components/**/*.js'
	],
	styles: ['dev/sass/main.scss'],
	templates: ['dev/js/app/templates/**/*.html']
};


/* Templates */
gulp.task('templates', function(done) {
	gulp.src(paths.templates)
		.pipe(gulp.dest('templates/app'));
	done();
});

/* Scripts */
gulp.task('scripts', function (done) {
	gulp.src(paths.scripts)
		.pipe(concat('main.min.js'))
		.pipe(ngAnnotate())
		.pipe(uglify())
		.pipe(gulp.dest('build/js'));
	done();
});

/* Sass */
gulp.task('sass', function (done) {
	gulp.src(paths.styles)
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(rename('main.min.css'))
		.pipe(gulp.dest('build/css'));
	done();
});

gulp.task('watch', function (done) {
	gulp.watch('dev/sass/**/*.scss', ['sass']);
	gulp.watch('dev/js/**/*.js', ['scripts']);
	gulp.watch('dev/js/app/templates/**/*.html', ['templates']);
	done();
});

gulp.task('default', gulp.series('templates', 'scripts', 'sass'));