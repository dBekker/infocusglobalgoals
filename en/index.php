<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php'); ?>

<? $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"slider-main", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "PREVIEW_PICTURE",
			3 => "",
		),
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "9",
		"IBLOCK_TYPE" => "Content",
		"NEWS_COUNT" => "20",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "slider-main",
		"DETAIL_URL" => "",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "News",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"MESSAGE_404" => ""
	),
	false
); ?>

    <div class="accession">
        <div class="row">
            <div class="col col-md-8 col-12">
                <div class="accession__wrap">
                    <div class="accession__content">
                        <p>
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "page",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => ""
                                )
                            ); ?>
                        </p>
                    </div>

                    <div class="accession__action">
						<a class="btn btn_large btn_white" href="/en/auth/register/">I want to change the world</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="accession__shrot shrot">
            <div class="shrot__item shrot__item_1"></div>
        </div>
    </div>

    <div class="procedure procedure_main">
        <div class="procedure__banner">Declare<span>&ensp;</span>yourself</div>

        <div class="procedure__element row">
            <div class="col col-md-7 offset-md-2 col-12">
                <div class="procedure__title">
                    <span class="procedure__count procedure__count_one">1</span>
                    Build a team
                </div>

                <div class="procedure__description">
                    Put together a team of 2 to 10 young people.
					<a href="/en/auth/register/">Sign up</a> on the #INFOCUS online platform
                    and create an online group for your project team.
                </div>
            </div>
        </div>

        <div class="procedure__element row">
            <div class="col col-md-7 offset-md-5 col-12">
                <div class="procedure__title">
                    <span class="procedure__count">2</span>
                    Propose a project
                </div>

                <div class="procedure__description">
                    Design a project aimed at resolving a social problem, thus contributing to achieving
					<a href="/en/rules/activities/">the 17 Sustainable Development Goals</a>.
                </div>
            </div>
        </div>

        <div class="procedure__element row">
            <div class="col col-md-7 offset-md-2 col-12">
                <div class="procedure__title">
                    <span class="procedure__count">3</span>
                    Achieve results
                </div>

                <div class="procedure__description">
					Get help from <a href="/en/experts/">an expert coach</a>,
                    who will help you develop your project and achieve your objective.
                </div>
            </div>
        </div>

        <div class="procedure__element row">
            <div class="col col-md-7 offset-md-5 col-12">
                <div class="procedure__title">
                    <span class="procedure__count">4</span>
                    Present your project at the UN
                </div>

                <div class="procedure__description">
                    Present the outcomes of your project to the international community at the United Nations and on other major international platforms.
                </div>
            </div>
        </div>

        <div class="procedure__action">
			<a class="btn btn_large" href="/en/auth/register/">I want to change the world</a>
        </div>

        <div class="procedure__shrot shrot">
            <div class="shrot__item shrot__item_2"></div>
            <div class="shrot__item shrot__item_3"></div>
        </div>
    </div>

    <div class="slider-banner">
        <div class="slider-banner__wrap">
            <div class="slider-banner__slide">
                <div class="slider-banner__image" style="background-image: url('/pics/banner/banner-1.png')"></div>
                <div class="slider-banner__title">Participants</div>
            </div>

            <div class="slider-banner__slide">
                <div class="slider-banner__image" style="background-image: url('/pics/banner/banner-6.png')"></div>
                <div class="slider-banner__title">Experts</div>
            </div>

            <div class="slider-banner__slide">
                <div class="slider-banner__image" style="background-image: url('/pics/banner/banner-3.png')"></div>
                <div class="slider-banner__title">Projects</div>
            </div>
        </div>

        <div class="slider-banner__pagination"></div>
    </div>

    <div class="organizers">
        <div class="organizers__title">
            <h2>Organizers</h2>
        </div>

        <div class="organizers__wrap">
            <div class="organizers__image">
                <img src="/pics/partners/logo-4.png" alt="">
            </div>

            <div class="organizers__content">
                Our Association brings people and companies together through stimulating world-changing ideas. We are building a community of innovators and helping them translate ideas into results, thus developing a knowledge economy and high-hume technologies. We are working together to create an environment in which education serves as a tool for accomplishing personal and corporate challenges.
            </div>
        </div>
    </div>

    <div class="partners">
        <div class="partners__title">
            <h2>Partners</h2>
        </div>

        <div class="partners__wrap row">
            <div class="partners__item col col-md-2 col-sm-3 col-12 align-self-center">
                <a href="https://xn--80afcdbalict6afooklqi5o.xn--p1ai/" target="_blank" rel="noopener"><img src="/pics/partners/logo-2.png" alt=""></a>
            </div>
        </div>

        <div class="partners__shrot shrot">
            <div class="shrot__item shrot__item_5"></div>
        </div>
    </div>

<? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>