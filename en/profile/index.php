<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
?>

<div class="row">
    <div class="col col-12">
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.profile",
            "participants",
            Array(
                "CHECK_RIGHTS" => "N",
                "SEND_INFO" => "N",
                "SET_TITLE" => "Y",
                "USER_PROPERTY" => array(
                    0 => "UF_PERSONAL_SPEAK_EN",
                    1 => "UF_USER_COUNTRY",
                    2 => "UF_PERSONAL_LANGUAGE"
                ),
                "USER_PROPERTY_NAME" => ""
            )
        );?>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>