<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <section class="page-content">
            <div class="container">
                <ul>
                    <li><a href="/templates/list">Страница типа Лист</a></li>
                    <li><a href="/templates/main">Главная</a></li>
                    <li><a href="/templates/404">404</a></li>
                </ul>

                <h3>Личный кабинет</h3>

                <ul>
                    <li><a href="/templates/auth/signup">Регистрация</a></li>
                    <li><a href="/templates/auth/signin">Авторизация</a></li>
                    <li><a href="/templates/auth/forgot">Востановить пароль</a></li>
                </ul>

                <ul>
                    <li><a href="/templates/lk/team">Моя команда</a></li>
                    <li><a href="/templates/lk/project/create">Наш проект</a></li>
                    <li><a href="/templates/lk/project/work">Работа по проекту</a></li>
                    <li><a href="/templates/lk/project/report">Добавление общения</a></li>
                    <li><a href="/templates/lk/project/file">Добавление файла</a></li>
                    <li><a href="/templates/lk/profile">Профиль</a></li>
                </ul>

                <h3>Личный кабинет эксперта</h3>

                <ul>
                    <li><a href="/templates/lk-expert/profile">Профиль</a></li>
                    <li><a href="/templates/lk-expert/project/list">Выбор проектов</a></li>
                    <li><a href="/templates/lk-expert/project/detail">Проект</a></li>
                </ul>

                <h3>Правила участия</h3>

                <ul>
                    <li><a href="/templates/rules/procedure">Порядок участия</a></li>
                    <li><a href="/templates/rules/activities">Направления деятельности</a></li>
                </ul>

                <h3>Проекты</h3>

                <ul>
                    <li><a href="/templates/projects/list">Список</a></li>
                    <li><a href="/templates/projects/detail">Страница</a></li>
                </ul>

                <h3>Эксперты</h3>

                <ul>
                    <li><a href="/templates/experts/list">Список</a></li>
                    <li><a href="/templates/experts/detail">Страница</a></li>
                </ul>
            </div>
        </section>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>