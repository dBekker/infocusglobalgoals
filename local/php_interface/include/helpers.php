<?php
namespace App;

Class Helpers{
	public static function ValidateAge($date, $max = 30, $min = 12){
		$birthday_timestamp = strtotime($date);
		$age = date('Y') - date('Y', $birthday_timestamp);
		if (date('md', $birthday_timestamp) > date('md'))
			$age--;

		if ($age > $max || $age < $min) {
			return false;
		}
		return true;
	}

	public static function formatSay($number, $titles)
	{
		$cases = [2, 0, 1, 1, 1, 2];
		return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[($number % 10 < 5) ? $number % 10 : 5]];
	}
}

