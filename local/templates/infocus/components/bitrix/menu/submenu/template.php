<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult))
	return;
?>
<?php
$arGroups = CUser::GetUserGroup($USER->GetID()); // массив групп, в которых состоит пользователь
$is_leader = $USER->CanDoOperation('edit_own_profile', $USER->getId());
?>

<ul class="nav-sub__wrap line-align__wrap">
	<? foreach ($arResult as $itemIdex => $arItem): ?>
        <!-- FOR LEADERS -->
	    <?if(trim($arItem["PARAMS"]["USER_GROUP"]) == "LEADER" && $is_leader):?>
			<?if($arItem["SELECTED"]):?>
                <li class="nav-sub__item nav-sub__item_active line-align__item"><?= $arItem["TEXT"] ?></li>
			<?else:?>
                <li class="nav-sub__item line-align__item"><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
			<?endif;?>
        <? elseif(!$arItem["PARAMS"]["USER_GROUP"] || $USER->IsAdmin()): ?>
			<?if($arItem["SELECTED"]):?>
                <li class="nav-sub__item nav-sub__item_active line-align__item"><?= $arItem["TEXT"] ?></li>
			<?else:?>
                <li class="nav-sub__item line-align__item"><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
			<?endif;?>
		<? endif; ?>
	<? endforeach; ?>
</ul>