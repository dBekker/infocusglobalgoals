<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<?php $APPLICATION->IncludeFile('inc/meta.php', array(), array('SHOW_BORDER' => false)); ?>

<div class="page-wrapper">
    <?php $APPLICATION->IncludeFile('inc/header.php', array(), array('SHOW_BORDER' => false)); ?>

	<?php if($APPLICATION->GetCurPage(false) === '/auth/register/'): ?>
        <section class="page-visual">
            <div class="visual">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 d-none d-sm-block align-self-end">
                            <div class="visual__image">
                                <img src="/templates/pics/visual-1.png">
                            </div>
                        </div>

                        <div class="col-sm-6 col-12 align-self-center">
                            <div class="visual__content">
                                <h1 class="visual__title">
                                    Cтань частью глобального движения! Нас уже 150&nbsp;со всего
                                    мира!
                                </h1>

                                <div class="visual__description">
                                    Хотите участвовать в&nbsp;проекте в&nbsp;качестве эксперта? Пишите&nbsp;<a
                                            href="mailto:info@tetradka.org.ru">info@tetradka.org.ru</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	<?php endif ?>

    <section class="page-content">
        <div class="container">
			<?if($APPLICATION->GetCurPage(false) != '/'): ?>
			    <?php include $_SERVER['DOCUMENT_ROOT'] . "/inc/shrot.php"; ?>
            <?endif;?>
