<?php if(CSite::InDir(SITE_DIR.'profile/') || CSite::InDir(SITE_DIR.'profile-expert/')){$isLK = true;}?>
<ul class="line-align__wrap">
	<li class="line-align__item">
		<?if(!$USER->IsAuthorized()):?>
			<a class="icon icon_to-text icon-auth" href="/auth/">Вход</a>
		<?elseif ($USER->IsAuthorized() && $isLK):?>
			<a class="icon icon_to-text icon-auth" href="/auth/?logout=yes">Выйти</a>
		<?else:?>
			<a class="icon icon_to-text icon-auth" href="/profile/">Профиль</a>
		<?endif;?>
	</li>
    <li class="line-align__item line-align__item_accent d-none d-md-inline-block">
		<a href="/en/">EN</a>
    </li>
</ul>