<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
?>

<h1><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></h1>

<form id="form-forgot" class="form form-forgot" name="bform" action="<?=$arResult["AUTH_URL"]?>" method="post" data-ajax="false">
    <input type="hidden" name="AUTH_FORM" value="Y">
    <input type="hidden" name="TYPE" value="SEND_PWD">
    <?if (strlen($arResult["BACKURL"]) > 0) {?>
        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
    <?}?>

    <?if(!empty($arResult["ERROR_MESSAGE"])):?>
        <div class="form__response form__response_error">
            <?ShowMessage($arResult['ERROR_MESSAGE']);?>
        </div>
    <?endif;?>

	<?if(!empty($arParams["~AUTH_RESULT"])):?>
        <div class="form__response">
			<?ShowMessage($arParams["~AUTH_RESULT"]); ?>
        </div>
	<?endif;?>

    <div class="form__content">
        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-forgot-1"><?=GetMessage("AUTH_EMAIL")?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-forgot-1" class="input" name="USER_EMAIL" type="email" required>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                <input type="hidden" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>">
                <button type="submit" class="btn"><?=GetMessage("AUTH_SEND")?></button>
            </div>
        </div>
    </div>
</form>
