<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div> <!-- This div should be here. Belive me -->
    <h1><?=GetMessage("AUTH_CHANGE_PASSWORD")?></h1>
    <form id="form-forgot" class="form form-forgot" name="bform" action="<?=$arResult["AUTH_FORM"]?>" method="post" data-ajax="false">
        <input type="hidden" name="AUTH_FORM" value="Y">
        <input type="hidden" name="TYPE" value="CHANGE_PWD">
        <?if (strlen($arResult["BACKURL"]) > 0) {?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?}?>

        <?if(!empty($arResult["ERROR_MESSAGE"])):?>
            <div class="form__response form__response_error">
                <?ShowMessage($arResult['ERROR_MESSAGE']);?>
            </div>
        <?endif;?>

		<?if(!empty($arParams["~AUTH_RESULT"])):?>
            <div class="form__response">
				<?ShowMessage($arParams["~AUTH_RESULT"]); ?>
            </div>
		<?endif;?>

        <div class="form__content">
            <div class="input-field row">
                <div class="col-md-2 col-sm-3 col-12 align-self-center">
                    <label for="form-forgot-1"><?=GetMessage("AUTH_LOGIN")?></label>
                </div>
                <div class="col-md-3 col-sm-5 col-12 align-self-center">
                    <input id="form-forgot-1" class="input" name="USER_LOGIN" type="email" value="<?=$arResult["LAST_LOGIN"]?>" required>
                </div>
            </div>

             <input class="input" name="USER_CHECKWORD" type="hidden" value="<?=$arResult["USER_CHECKWORD"]?>" required>

            <div class="input-field row">
                <div class="col-md-2 col-sm-3 col-12 align-self-center">
                    <label for="form-sign-up-8"><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?></label>
                </div>

                <div class="col-md-3 col-sm-5 col-12 align-self-center">
                    <input id="form-sign-up-8" class="input" name="USER_PASSWORD" value="<?=$arResult["USER_PASSWORD"]?>" type="password" required>
                </div>
            </div>

            <div class="input-field row">
                <div class="col-md-2 col-sm-3 col-12 align-self-center">
                    <label for="form-sign-up-8"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></label>
                </div>

                <div class="col-md-3 col-sm-5 col-12 align-self-center">
                    <input id="form-sign-up-8" class="input" name="USER_CONFIRM_PASSWORD" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" type="password" required>
                </div>
            </div>

            <div class="input-field row">
                <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                    <input class="btn" type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>">
                </div>
            </div>
        </div>
    </form>
</div>