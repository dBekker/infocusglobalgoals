<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <h1>Порядок участия</h1>

                        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/nav-sub.php"; ?>

                        <div class="content">
                            <div class="row">
                                <div class="col col-md-9 col-12">
                                    <p>
                                        К участию приглашаются команды молодых социальных инноваторов в возрасте от 14 до 25 лет
                                        из разных уголков земного шара, желающие внести свой вклад в достижение общемировой Повестки
                                        дня в области устойчивого развития до 2030 года и оставить след в истории Земли.
                                    </p>

                                    <p>
                                        Для этого необходимо:
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="procedure">
                            <div class="procedure__element row">
                                <div class="col col-md-7 col-12">

                                    <div class="procedure__title">
                                        <span class="procedure__count procedure__count_one">1</span>
                                        Сформировать молодежную команду
                                    </div>

                                    <div class="procedure__description">
                                        от 2-х до 10 человек
                                    </div>
                                </div>
                            </div>

                            <div class="procedure__element row">
                                <div class="col col-md-7 offset-md-3 col-12">
                                    <div class="procedure__title">
                                        <span class="procedure__count">2</span>
                                        Зарегистрироваться на сайте
                                    </div>

                                    <div class="procedure__description">
                                        Предоставив информацию о себе и членах своей команды
                                    </div>
                                </div>
                            </div>

                            <div class="procedure__element row">
                                <div class="col col-md-7 col-12">
                                    <div class="procedure__title">
                                        <span class="procedure__count">3</span>
                                        Определить проблему, на решение которой будет направлен проект
                                    </div>

                                    <div class="procedure__description">
                                        Проблема должна соответствовать одной из 17-ти Целей устойчивого развития, провозглашенных ООН на период до 2030 года
                                    </div>
                                </div>
                            </div>

                            <div class="procedure__element row">
                                <div class="col col-md-7 offset-md-3 col-12">
                                    <div class="procedure__title">
                                        <span class="procedure__count">4</span>
                                        Описать свой проект, заполнив все поля online-заявки в Личном кабинете
                                    </div>

                                    <div class="procedure__description">
                                        <p>
                                            Проекты могут:
                                        </p>

                                        <ul>
                                            <li>
                                                быть уже реализованными, но требующими доработки и улучшения для расширения географии их реализации и трансляции в другие регионы и
                                                страны мира;
                                            </li>

                                            <li>
                                                находиться на стадии реализации;
                                            </li>

                                            <li>
                                                находиться на стадии идеи. В данном случае, проект должен иметь четко обозначенную проблему и прописанный механизм ее решения.
                                            </li>
                                        </ul>

                                        <a href="#!">Общие рекомендации по написанию социально значимых проектов.</a>

                                        <p>
                                            Обращаем Ваше внимание на то, что заявка оформляется на русском и (или) на английском языке. Название и краткое описание проекта должны
                                            быть
                                            оформлены на двух языках. Название и краткое описание размещаются на платформе в публичном доступе.
                                        </p>

                                        <p>
                                            После заполнения online-заявки проект проходит первичную проверку на соответствие основным требованиям. Если во время проверки в проекте
                                            были выявлены недостатки (неверно или не в полном объеме заполненные поля, расхождение в цели и методах ее достижения и т.п.), проект
                                            отправляется на доработку. Проект может быть доработан не более 3-х раз. Если после 3-го раза проект так и не прошел проверку, он не
                                            допускается к дальнейшему участию в Молодежной сети #INFOCUS.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="procedure__element row">
                                <div class="col col-md-7 col-12">
                                    <div class="procedure__title">
                                        <span class="procedure__count">5</span>
                                        Получить эксперта-наставника
                                    </div>

                                    <div class="procedure__description">
                                        Проекты, прошедшие проверку, отправляются экспертам. Эксперты знакомятся с предложенными проектами и выбирают те, наставниками которых они
                                        хотели бы быть. Информация о том, какой эксперт выбрал проект, отображается в Личных кабинетах участников.
                                    </div>
                                </div>
                            </div>

                            <div class="procedure__element row">
                                <div class="col col-md-7 offset-md-3 col-12">
                                    <div class="procedure__title">
                                        <span class="procedure__count">6</span>
                                        Приступить к реализации проекта под руководством эксперта-наставника
                                    </div>

                                    <div class="procedure__description">
                                        Работа над проектом будет длиться в течение 24 недель, затем проект будет оценен, по итогам оценки определяться подбедители, Победители
                                        приглашаются к участию в Международном форуме «Доброволец России - 2018» в Перми (октябрь 2018 года). Проекты команд-победителей получат
                                        поддержку для трансляции в регионы России и страны мира, заинтересованные в их реализации.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>