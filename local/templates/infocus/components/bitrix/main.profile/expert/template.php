<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<div> <!--Very important div. It fights against bitrix ;) -->
<h1>Профиль эксперта</h1>

<?$APPLICATION->IncludeFile("/inc/submenu.php", Array(), Array()); ?>

<form id="form-profile" class="form form-profile" action="<?=$arResult["FORM_TARGET"]?>" name="form1" method="post" enctype="multipart/form-data" data-ajax="false">
	<?=$arResult["BX_SESSION_CHECK"]?>
	<?php if (!empty($arResult["strProfileError"])): ?>
        <div class="form__response form__response_error">
			<?ShowError($arResult["strProfileError"]); ?>
        </div>
	<?php endif ?>

	<?if ($arResult['DATA_SAVED'] == 'Y'):?>
        <div class="form__response">
			<?ShowNote(GetMessage('PROFILE_DATA_SAVED'));; ?>
        </div>
	<?endif;?>

    <input type="hidden" name="lang" value="<?=LANG?>" />
    <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
    <div class="form__content">
        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-1"><?=GetMessage('NAME')?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-1" class="input" name="NAME" type="text" value="<?=$arResult["arUser"]["NAME"]?>" required>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-2">Страна</label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-2" class="input" name="UF_USER_COUNTRY" value="<?=$arResult["arUser"]["UF_USER_COUNTRY"]?>" type="text">
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-3">Язык</label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-3" class="input" name="UF_PERSONAL_LANGUAGE" value="<?=$arResult["arUser"]["UF_PERSONAL_LANGUAGE"]?>" type="text" required>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-4"><?=GetMessage("USER_BIRTHDAY_DT")?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-4" class="input input_date" name="PERSONAL_BIRTHDAY" type="text" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" >
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-5">Сфера специализации</label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <?
				$rsEnum = CUserFieldEnum::GetList(array(), array("USER_FIELD_NAME"=>"UF_PERSONAL_SPEC", "VALUE" =>$star));
				while ($arEnum = $rsEnum->GetNext()){
				    $item_spec = array("CODE" =>$arEnum["XML_ID"], "VALUE" =>$arEnum["VALUE"], "ID" => $arEnum["ID"]);
					$arSpecs[] = $item_spec;
                };

				$rsUser = CUser::GetList($by, $order,
					array(
						"ID" => $USER->GetID(),
					),
					array(
						"SELECT" => array(
							"UF_PERSONAL_SPEC",
						),
					)
				);
				if($arUser = $rsUser->Fetch())
				{
					$rsSpecialization = CUserFieldEnum::GetList(array(), array(
						"ID" => $arUser["UF_PERSONAL_SPEC"],
					));
					if($arSpec = $rsSpecialization->GetNext()){
						$specialization = $arSpec["XML_ID"];
                    }
				}

                ?>
               <select id="form-profile-5" class="input input_select" name="UF_PERSONAL_SPEC">
                   <?foreach ($arSpecs as $spec):?>
                        <option value="<?=$spec["ID"]?>"<?=$spec["CODE"] == $specialization ? " SELECTED=\"SELECTED\"" : ""?>><?=$spec["VALUE"]?></option>
                   <?endforeach;?>
                </select>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-3"><?=GetMessage('EMAIL')?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-3" class="input" name="EMAIL" type="email" value="<?= $arResult["arUser"]["EMAIL"]?>" required>
            </div>

            <div class="col-md-6 align-self-center d-none d-md-block">
                <div class="input-field__hint">
                    Адрес электронной почты будет использоваться для входа в Личный кабинет
                </div>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-3"><?=GetMessage('USER_PHONE')?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-3" class="input input_phone" name="PERSONAL_PHONE" type="text" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" required>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-7">Должность</label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-7" class="input" name="PERSONAL_PROFESSION"  value="<?=$arResult["arUser"]["PERSONAL_PROFESSION"]?>" type="text">
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-8">Место работы</label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-8" class="input" name="WORK_POSITION" value="<?=$arResult["arUser"]["WORK_POSITION"]?>" type="text">
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12">
                <label for="form-profile-9">Краткая биография</label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <textarea id="form-profile-9" class="input input_area" name="PERSONAL_NOTES"><?=$arResult["arUser"]["PERSONAL_NOTES"]?></textarea>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-10">Интересы и хобби</label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <textarea id="form-profile-10" class="input input_area" name="WORK_PROFILE"><?=$arResult["arUser"]["WORK_PROFILE"]?></textarea>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-11">Цитата</label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-11" class="input" name="WORK_NOTES" value="<?=$arResult["arUser"]["WORK_NOTES"]?>" type="text">
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-3 col-sm-5 col-12 offset-md-2 offset-sm-3 align-self-center">

				<?php if ($arResult["arUser"]["PERSONAL_PHOTO"]): ?>
                    <div class="form-profile__image preview-image"
                         style="background-image: url('<?= CFile::GetPath($arResult["arUser"]["PERSONAL_PHOTO"]) ?>')"></div>
				<?php endif ?>

                <input id="form-profile-6" class="input input_file" name="PERSONAL_PHOTO" type="file" placeholder="Загрузить фотографию">
                <label for="form-profile-6">Загрузить фотографию</label>
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-9"><?=GetMessage('NEW_PASSWORD_REQ')?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-9" class="input" name="NEW_PASSWORD" autocomplete="off" type="password">
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-2 col-sm-3 col-12 align-self-center">
                <label for="form-profile-10"><?=GetMessage('NEW_PASSWORD_CONFIRM')?></label>
            </div>

            <div class="col-md-3 col-sm-5 col-12 align-self-center">
                <input id="form-profile-10" class="input" name="NEW_PASSWORD_CONFIRM" type="password" autocomplete="off">
            </div>
        </div>

        <div class="input-field row">
            <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                <input type="hidden" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                <button type="submit" class="btn"><?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?></button>
				<?$APPLICATION->IncludeFile("/inc/policy.php", Array(), Array()); ?>
            </div>
        </div>
    </div>
</form>
</div>