<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$rsUserExpert = CUser::GetByID($USER->GetID());
$arUserExpert = $rsUserExpert->Fetch();

$rsUserLeader = CUser::GetByID($arResult["FIELDS"]["CREATED_BY"]);
$arUserLeader = $rsUserLeader->Fetch();

/*Add Project props*/
$arResult["EXPERTS"] = Array(
	"HAS_EXPERT" => $arResult["DISPLAY_PROPERTIES"]["EXPERT"]["VALUE"],
	"IS_ME" => in_array($arResult["ID"], $arUserExpert["UF_ASSIGNED_PROJ"]) ? "Y" : ""
);

/* Team's properties*/
$arSelect = Array();
$arFilter = Array("IBLOCK_ID" => 1, "ACTIVE" => "Y", "CREATED_BY" => $arUserLeader["ID"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
if ($ob = $res->GetNextElement()) {
	$arFields = $ob->GetFields();

	$arResult["TEAM_PROP"] = array(
		"NAME" => $arFields['NAME']
	);
}

$arResult["USER_PROPS"] = array(
	"SPEAK_EN" => $arUserLeader["UF_PERSONAL_SPEAK_EN"]
);

?>