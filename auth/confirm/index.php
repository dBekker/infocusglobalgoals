<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Подтверждение пароля — InFocus");
$APPLICATION->SetTitle("Подтверждение пароля");
?>
<div class="row">
    <div class="col col-12">
        <?$APPLICATION->IncludeComponent("bitrix:system.auth.confirmation",
            "",
            Array(
                "USER_ID" => "confirm_user_id",
                "CONFIRM_CODE" => "confirm_code",
                "LOGIN" => "login"
            )
        );?>
    </div>
</div>
