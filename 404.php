<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");?>

<section class="page-content">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="error">
					<div class="error__num">404</div>
					<div class="error__title"><h1>Такой страницы нет</h1></div>
					<div class="error__text">Перейдите на <a href="/">главную</a> или воспользуйтесь навигацией сайта</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>