<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-visual">
            <div class="visual">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 d-none d-sm-block align-self-end">
                            <div class="visual__image">
                                <img src="/templates/pics/visual-1.png">
                            </div>
                        </div>

                        <div class="col-sm-6 col-12 align-self-center">
                            <div class="visual__content">
                                <h1 class="visual__title">
                                    Поздравляем! Ты&nbsp;стал частью глобального движения! Нас уже 150&nbsp;со всего мира!
                                </h1>

                                <div class="visual__description">
                                    Хотите поучаствовать в&nbsp;проекте в&nbsp;качестве эксперта? Пишите&nbsp;<a href="mailto:info@tetradka.org.ru">info@tetradka.org.ru</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="page-content">
            <div class="container">
                <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/shrot.php"; ?>

                <div class="row">
                    <div class="col col-12">
                        <h1>Регистрация</h1>

                        <form id="form-sign-up" class="form form-sign-up" action="/templates/auth/signup" method="post" data-ajax="false">
                            <div class="form__response">
                                Логин или пароль указаны неверно. Попробуйте ещё раз.
                            </div>

                            <div class="form__response form__response_error">
                                Логин или пароль указаны неверно. Попробуйте ещё раз.
                            </div>

                            <div class="form__content">
                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-sign-up-1">Имя</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-sign-up-1" class="input" name="name" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-sign-up-2">Электронная почта</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-sign-up-2" class="input" name="email" type="email" required>
                                    </div>

                                    <div class="col-md-6 align-self-center d-none d-md-block">
                                        <div class="input-field__hint">
                                            Адрес электронной почты будет использоваться для входа в Личный кабинет
                                        </div>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-sign-up-3">Телефон</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-sign-up-3" class="input input_phone" name="phone" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-sign-up-4">Дата рождения</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-sign-up-4" class="input input_date" name="date" type="text">
                                    </div>

                                    <div class="col-md-6 align-self-center d-none d-md-block">
                                        <div class="input-field__hint">
                                            Участниками проекта могут стать инноваторы в возрасте от 14 до 30 лет
                                        </div>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-sign-up-5">Место проживания</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-sign-up-5" class="input" name="place" type="text">
                                    </div>

                                    <div class="col-md-6 align-self-center d-none d-md-block">
                                        <div class="input-field__hint">
                                            Укажите регион и населенный пункт проживания
                                        </div>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-3 col-sm-5 col-12 offset-md-2 offset-sm-3 align-self-center">
                                        <input id="form-sign-up-6" class="input input_file" name="photo" type="file" placeholder="Загрузить фотографию">
                                        <label for="form-sign-up-6">Загрузить фотографию</label>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-3 col-sm-5 col-12 offset-md-2 offset-sm-3 align-self-center">
                                        <input id="form-sign-up-7" class="input input_checkbox" name="is-eng" type="checkbox">
                                        <label for="form-sign-up-7">Я говорю по-английски</label>
                                    </div>

                                    <div class="col-md-6 align-self-center d-none d-md-block">
                                        <div class="input-field__hint">
                                            Вы можете общаться на английском с куратором напрямую
                                        </div>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-sign-up-8">Пароль</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-sign-up-8" class="input" name="password" type="password" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                                        <button class="btn" type="submit">Хочу изменить мир</button>

                                        <p>
                                            Предоставляя личные данные, вы&nbsp;подтверждаете,
                                            что ознакомлены с&nbsp;<a href="#!">политикой конфиденциальности</a> и принимаете
                                            <a href="#!">пользовательское соглашение</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>