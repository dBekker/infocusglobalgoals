<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <section class="project-header">
                    <div class="row">
                        <div class="col col-md-7 col-12">
                            <h1 class="project-header__title">Мир, правосудие и эффективные институты</h1>

                            <div class="project-points">
                                <div class="project-points__result">
                                    <div class="project-points__count">180</div>
                                    <div class="project-points__title">баллов</div>
                                </div>

                                <div class="project-points__info">
                                    160 баллов от эксперта<br>
                                    и 20 по итогам голосования
                                </div>
                            </div>

                            <div class="project-tags">
                                <div class="project-tags__team icon icon_to-text icon_not-hover icon-team">Команда №1</div>

                                <div class="project-tags__position">2. Ликвидация голода</div>

                                <div class="project-tags__lang">Английский язык</div>

                                <div class="project-tags__back"><a href="#!">Другие проекты</a></div>
                            </div>
                        </div>
                    </div>

                    <div class="project-header__image" style="background-image: url('/templates/pics/project.jpg')"></div>
                </section>

                <div class="project-edit">
                    <div class="row">
                        <div class="col col-md-8 col-12">
                            <h2>Редактирование проекта</h2>

                            <div class="project-edit__points">
                                <h3>Баллы</h3>

                                <form id="form-points" class="form form-points" action="/ajax/setPoints.php" method="post">
                                    <div class="form__content">
                                        <div class="input-field input-field_inline">
                                            <input class="input" name="project" type="hidden" value="22">
                                            <input class="input" name="count" type="number" placeholder="Введите количество баллов" required>
                                        </div>

                                        <div class="input-field  input-field_inline">
                                            <button class="btn" type="submit">Добавить</button>
                                        </div>
                                    </div>

                                    <div class="form__loading form__loading_text-center">
                                        <div class="loader loader_dark"></div>
                                    </div>

                                    <div class="form__success">
                                        <p>
                                            Баллы добавлены.
                                        </p>
                                    </div>

                                    <div class="form__error">
                                        <p>
                                            Извините, произошла ошибка. Попробуйте отправить сообщение позже.
                                        </p>
                                    </div>
                                </form>
                            </div>

                            <div class="project-edit__report project-report">
                                <h3>Итоги общения</h3>

                                <div class="project-report__wrap">
                                    <div class="project-report__element">
                                        <div class="project-report_date">
                                            <nobr><b>Дата общения с куратором:</b></nobr>
                                            15.05.2018
                                        </div>
                                        <div class="project-report_port">
                                            <nobr><b>Способ общения:</b></nobr>
                                            Телефон
                                        </div>

                                        <div class="project-report__content content">
                                            <div class="project-report__content-title"><b>Результаты общения (вопросы и решения)</b></div>
                                            Каждый раз нужно вникать в смысл текста и привязывать предлоги. Привязывать предлоги
                                            и союзы к следующему за ними слову, а частицы — к предыдущему ледующему за ними слову,
                                            а частицы — к предыдущему
                                        </div>
                                        <div class="project-report_action">
                                            <a href="#!">Редактировать</a>
                                        </div>
                                    </div>

                                    <div class="project-report__element">
                                        <div class="project-report_date">
                                            <nobr><b>Дата общения с куратором:</b></nobr>
                                            15.05.2018
                                        </div>
                                        <div class="project-report_port">
                                            <nobr><b>Способ общения:</b></nobr>
                                            Телефон
                                        </div>

                                        <div class="project-report__content content">
                                            <div class="project-report__content-title"><b>Результаты общения (вопросы и решения)</b></div>
                                            Каждый раз нужно вникать в смысл текста и привязывать предлоги. Привязывать предлоги
                                            и союзы к следующему за ними слову, а частицы — к предыдущему ледующему за ними слову,
                                            а частицы — к предыдущему
                                        </div>
                                        <div class="project-report_action">
                                            <a href="#!">Редактировать</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>