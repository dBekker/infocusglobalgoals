<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="expert-detail">
    <div class="row">
        <div class="expert-detail__sidebar col col-md-3 col-sm-5 col-12">
			<? if($arResult['PERSONAL_PHOTO']):
				if($arParams['RESIZE_PERSONAL_PHOTO']){
					$imgSrc = $arResult["PERSONAL_PHOTO"]["RESIZE"]['src'];
				} else {
					$imgSrc = $arResult["PERSONAL_PHOTO"]['SRC'];
				} ?>
                <div class="expert-detail__image" style="background-image: url('<?=$imgSrc?>')"></div>
			<?endif;?>

			<?php if ($arResult["WORK_NOTES"]): ?>
                <blockquote class="expert-detail__blockquote">
                    «<?= $arResult["WORK_NOTES"]; ?>»
                </blockquote>
			<?php endif ?>
        </div>

        <div class="col col-md-8 offset-md-1 col-12">
            <h1><?=$arResult["NAME"]?></h1>

            <div class="expert-detail__position">
                <?=$arResult["PERSONAL_PROFESSION"];?><br>
				<?=$arResult["WORK_POSITION"];?>
            </div>

            <div class="expert-detail__tags line-align">
                <ul class="line-align__wrap">
					<?php if ($arResult["UF_PERSONAL_SPEC"]): ?>
                        <li class="line-align__item">
                            <div class="tag"><?= $arResult["UF_PERSONAL_SPEC"]; ?></div>
                        </li>
					<?php endif ?>
					<?php if ($arResult["UF_USER_COUNTRY"]): ?>
                        <li class="line-align__item">
                            <div class="tag"><?= $arResult["UF_USER_COUNTRY"]; ?></div>
                        </li>
					<?php endif ?>
                </ul>
            </div>

            <div class="expert-detail__content">
				<?php if (!empty($arResult['PROJECTS'])): ?>
                    <h3>Курирует проекты</h3>
                    <ul>
                        <?foreach ($arResult['PROJECTS'] as $project):?>
                            <li><a href="<?=$project['URL']?>"><?=$project['NAME']?></a></li>
                        <?endforeach;?>
                    </ul>
				<?php endif ?>

				<?php if ($arResult["PERSONAL_NOTES"]): ?>
                    <h3>Биография</h3>
                    <div class="content">
                        <p>
                          <?=$arResult["PERSONAL_NOTES"];?>
                        </p>
                    </div>
				<?php endif ?>

				<?php if ($arResult["WORK_PROFILE"]): ?>
                    <h3>Интересы и хобби</h3>
                    <div class="content">
                        <p>
                            <?=$arResult["WORK_PROFILE"]?>
                        </p>
                    </div>
				<?php endif ?>
            </div>

            <div class="to-back">
                <a href="<?=$arParams["LIST_URL"]?>">Все эксперты</a>
            </div>
        </div>
    </div>
</div>