<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col col-md-9 col-12">
                        <h1>Heading 1</h1>

                        <div class="content">
                            <p>
                                <a href="#!">Lorem ipsum dolor sit amet</a>
                            </p>

                            <p>
                                Отче наш, Иже еси на небесех! Да святится имя Твое; да приидет Царствие Твое; да будет воля Твоя, яко на небеси и на земли; хлеб наш насущный даждь
                                нам днесь; и остави нам долги наша якоже и мы оставляем должником нашим; и не введи нас во искушение, но избави нас от лукаваго; яко Твое есть
                                Царство и сила и слава во веки. Аминь.
                            </p>

                            <p>
                                Отче наш, Иже еси на небесех! Да святится имя Твое; да приидет Царствие Твое; да будет воля Твоя, яко на небеси и на земли; хлеб наш насущный даждь
                                нам днесь; и остави нам долги наша якоже и мы оставляем должником нашим; и не введи нас во искушение, но избави нас от лукаваго; яко Твое есть
                                Царство и сила и слава во веки. Аминь.
                            </p>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda earum iusto, laborum placeat quam rerum sed sint tenetur velit? Aspernatur at
                                <a href="#!">Lorem ipsum dolor sit amet</a>
                                blanditiis consequatur nam. Animi at consequatur consequuntur culpa cupiditate doloremque dolorum ducimus eaque enim est et eum hic illum inventore
                                itaque iusto labore laboriosam laborum laudantium magnam magni natus neque nostrum odio odit officiis placeat quae quas ratione rem repellat
                                repellendus reprehenderit sint unde vero, voluptates voluptatibus. At aut debitis hic inventore quaerat! Animi aperiam consectetur dolorem in iusto
                                maxime omnis repellat saepe similique, vitae. Aliquam consequuntur ea explicabo facilis fugiat, harum id inventore libero nesciunt obcaecati,
                                perferendis sed!
                            </p>

                            <table>
                                <thead>
                                    <tr>
                                        <th>123123</th>
                                        <th>fdgdfgfdg</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>234324</td>
                                        <td>fdgfdgfd</td>
                                    </tr>
                                    <tr>
                                        <td>234324</td>
                                        <td>dfgfdgfdg</td>
                                    </tr>
                                </tbody>
                            </table>

                            <h2>Heading 2</h2>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda earum iusto, laborum placeat quam rerum sed sint tenetur velit? Aspernatur at
                                blanditiis consequatur nam. Animi at consequatur consequuntur culpa cupiditate doloremque dolorum ducimus eaque enim est et eum hic illum inventore
                                itaque iusto labore laboriosam laborum laudantium magnam magni natus neque nostrum odio odit officiis placeat quae quas ratione rem repellat
                                repellendus reprehenderit sint unde vero, voluptates voluptatibus. At aut debitis hic inventore quaerat! Animi aperiam consectetur dolorem in iusto
                                maxime omnis repellat saepe similique, vitae. Aliquam consequuntur ea explicabo facilis fugiat, harum id inventore libero nesciunt obcaecati,
                                perferendis sed!
                            </p>

                            <ul>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing</li>
                            </ul>

                            <h3>Heading 3</h3>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda earum iusto, laborum placeat quam rerum sed sint tenetur velit? Aspernatur at
                                blanditiis consequatur nam. Animi at consequatur consequuntur culpa cupiditate doloremque dolorum ducimus eaque enim est et eum hic illum inventore
                                itaque iusto labore laboriosam laborum laudantium magnam magni natus neque nostrum odio odit officiis placeat quae quas ratione rem repellat
                                repellendus reprehenderit sint unde vero, voluptates voluptatibus. At aut debitis hic inventore quaerat! Animi aperiam consectetur dolorem in iusto
                                maxime omnis repellat saepe similique, vitae. Aliquam consequuntur ea explicabo facilis fugiat, harum id inventore libero nesciunt obcaecati,
                                perferendis sed!
                            </p>

                            <ol>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>