<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Файлы");
?>
<div class="row">
    <div class="col col-12">
        <h1><? $APPLICATION->ShowTitle(false) ?></h1>
        <? $APPLICATION->IncludeFile("/inc/submenu.php", Array(), Array()); ?>
        <? $APPLICATION->IncludeComponent(
            "infocus:iblock.file.add.form",
            ".default",
            array(
                "DEFAULT_INPUT_SIZE" => "30",
                "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                "ELEMENT_ASSOC" => "CREATED_BY",
                "GROUPS" => array(
                    0 => "1",
                    1 => "7",
                    2 => "9",
                ),
                "IBLOCK_ID" => "5",
                "IBLOCK_TYPE" => "Competition",
                "LEVEL_LAST" => "Y",
                "LIST_URL" => "/profile/project/work/",
                "MAX_FILE_SIZE" => "0",
                "MAX_LEVELS" => "100000",
                "MAX_USER_ENTRIES" => "100000",
                "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                "PROPERTY_CODES" => array(
                    0 => "NAME",
                    1 => "36",
                ),
                "PROPERTY_CODES_REQUIRED" => array(
                    0 => "36",
                    1 => "NAME",
                ),
                "RESIZE_IMAGES" => "N",
                "SEF_MODE" => "N",
                "STATUS" => "ANY",
                "STATUS_NEW" => "N",
                "USER_MESSAGE_ADD" => "",
                "USER_MESSAGE_EDIT" => "",
                "USE_CAPTCHA" => "N",
                "COMPONENT_TEMPLATE" => ".default",
                "CUSTOM_TITLE_NAME" => "Название файла",
                "CUSTOM_TITLE_TAGS" => "",
                "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                "CUSTOM_TITLE_DETAIL_TEXT" => "",
                "CUSTOM_TITLE_DETAIL_PICTURE" => ""
            ),
            false
        ); ?>
    </div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>