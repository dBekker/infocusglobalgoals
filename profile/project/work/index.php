<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Работа по проекту");
?>
<?
$arFilter["IBLOCK_ID"] = IBLOCK_PROJECT;
if (CModule::IncludeModule('IBlock')) {
	if (in_array(PARTICIPANT_GROUP_ID, $USER->GetUserGroupArray())) {
		$rsUser = CUser::GetByID($USER->GetID());
		$arUser = $rsUser->Fetch();
		$MyTeamId = $arUser['UF_MY_TEAM'];

		if ($MyTeamId) {
			$arSelectTeam = Array("ID", "NAME", "CREATED_BY");
			$arFilterTeam = Array("IBLOCK_ID" => 1, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $MyTeamId);
			$res = CIBlockElement::GetList(Array(), $arFilterTeam, false, Array(), $arSelectTeam);
			if ($ob = $res->GetNextElement()) {
				$arFieldsTeam = $ob->GetFields();
				$MyLeaderId = $arFieldsTeam["CREATED_BY"];
			}
			$arFilter["CREATED_BY"] = $MyLeaderId;
		}
	} else {
		$arFilter["CREATED_BY"] = $USER->GetID();
	}

    // Get user's project
	$rsIBlockElements = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter);

	if ($ob = $rsIBlockElements->GetNextElement()) {
		$arFields = $ob->GetFields();
		$arParams["ID"] = $arFields["ID"];
	}
}
?>

<div class="row">
    <div class="col col-12">
        <h1><? $APPLICATION->ShowTitle(false) ?></h1>
        <? $APPLICATION->IncludeFile("/inc/submenu.php", Array(), Array()); ?>
        <? if ($arFilter["CREATED_BY"]): ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:news.detail",
                "project_work",
                Array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_ELEMENT_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "BROWSER_TITLE" => "-",
                    "CACHE_GROUPS" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "N",
                    "CHECK_DATES" => "N",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "ELEMENT_CODE" => "",
                    "ELEMENT_ID" => $arParams["ID"],
                    "FIELD_CODE" => array("ID", "NAME", "CREATED_BY", "CREATED_USER_NAME", ""),
                    "IBLOCK_ID" => "3",
                    "IBLOCK_TYPE" => "Competition",
                    "IBLOCK_URL" => "",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                    "MESSAGE_404" => "",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Страница",
                    "PROPERTY_CODE" => array("POINTS_VOTE", "POINTS_EXPERT", "EXPERT"),
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_CANONICAL_URL" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "STRICT_SECTION_CHECK" => "N",
                    "USE_PERMISSIONS" => "N",
                    "USE_SHARE" => "N",
                    "LEADER_ID" => $arFilter["CREATED_BY"]
                )
            ); ?>
        <? else: ?>
            <p>Лидер вашей команды еще не создал проект.</p>
        <? endif; ?>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>