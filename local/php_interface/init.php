<?php

include("include/options.php");
include("include/helpers.php");

// User's Events
AddEventHandler("main", "OnBeforeUserRegister", Array("Auth","OnBeforeUserUpdateHandler"));
AddEventHandler("main", "OnBeforeUserUpdate", Array("Auth","OnBeforeUserUpdateHandler"));

AddEventHandler("main", "OnAfterUserAdd", Array("Auth", "OnAfterUserAddHandler"));

// Project's Events
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("InfoBlocks", "OnAfterIBlockElementAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("InfoBlocks", "OnBeforeIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnIBlockElementSetPropertyValues", Array("InfoBlocks", "OnIBlockElementSetPropertyValuesHandler"));

// Resize images
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("ImageProcess", "OnBeforeIBlockElementAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("ImageProcess", "OnBeforeIBlockElementAddHandler"));
AddEventHandler("main", "OnBeforeUserUpdate", Array("ImageProcess", "OnBeforeUserAddHandler"));
AddEventHandler("main", "OnBeforeUserAdd", Array("ImageProcess", "OnBeforeUserAddHandler"));

// 404 Controller
//AddEventHandler('main', 'OnEpilog', 'controller404', 1001);

function controller404() {
	if(defined('ERROR_404') && ERROR_404 == 'Y') {
		GLOBAL $APPLICATION;
		$APPLICATION->RestartBuffer();
		require $_SERVER['DOCUMENT_ROOT'].'/local/templates/infocus/header.php';
		require $_SERVER['DOCUMENT_ROOT'].'/404.php';
		require $_SERVER['DOCUMENT_ROOT'].'/local/templates/infocus/footer.php';
		return false;
	}
};

class ImageProcess
{
	function OnBeforeUserAddHandler(&$arFields)
	{
		CAllFile::ResizeImage(
			$arFields["PERSONAL_PHOTO"],
			array("width" => "500", "height" => "500"),
			BX_RESIZE_IMAGE_PROPORTIONAL);
	}

	function OnBeforeIBlockElementAddHandler(&$arFields)
	{
		foreach($arFields["PROPERTY_VALUES"][14] as &$file):
			CAllFile::ResizeImage(
				$file,
				array("width" => "500", "height" => "500"),
				BX_RESIZE_IMAGE_PROPORTIONAL);
		endforeach;
		foreach($arFields["PROPERTY_VALUES"][22] as &$file):
			CAllFile::ResizeImage(
				$file,
				array("width" => "500", "height" => "500"),
				BX_RESIZE_IMAGE_PROPORTIONAL);
		endforeach;
	}
}

class Auth {
	function OnBeforeUserUpdateHandler(&$arFields)
	{
		foreach ($arFields["GROUP_ID"] as $groupItem){
			if($groupItem["GROUP_ID"] == 1){
				$isAdmin = true;
			}
		}

		if(!$isAdmin){
			$arFields["LOGIN"] = $arFields["EMAIL"];
		}

		return $arFields;
	}


    function OnAfterUserAddHandler(&$arFields)
    {
        if (intval($arFields["ID"])>0)
        {
			$toSend = Array();
			$toSend["PASSWORD"] = strlen(trim($arFields["CONFIRM_PASSWORD"]))>0?$arFields["CONFIRM_PASSWORD"]:$arFields["PASSWORD_CONFIRM"];
			$toSend["EMAIL"] = $arFields["EMAIL"];
			$toSend["USER_ID"] = $arFields["ID"];
			$toSend["USER_IP"] = $arFields["USER_IP"];
			$toSend["USER_HOST"] = $arFields["USER_HOST"];
			$toSend["LOGIN"] = $arFields["LOGIN"];
			$toSend["URL_LOGIN"] = $arFields["LOGIN"];
			$toSend["CONFIRM_CODE"] = $arFields["CONFIRM_CODE"];
			$toSend["CHECKWORD"] = $arFields["CHECKWORD"];
			$toSend["NAME"] = (trim ($arFields["NAME"]) == "")? $toSend["NAME"] = htmlspecialchars(): $arFields["NAME"];
			$toSend["LAST_NAME"] = (trim ($arFields["LAST_NAME"]) == "")? $toSend["LAST_NAME"] = htmlspecialchars(): $arFields["LAST_NAME"];

        	if($arFields["GROUP_ID"][0]["GROUP_ID"] == EXPERT_GROUP_ID){
				CEvent::Send ("ADD_NEW_EXPERT", Array("s1"), $toSend);
			}elseif($arFields["GROUP_ID"][0] != LEADER_GROUP_ID){
				CEvent::Send ("ADD_NEW_USER", Array("s1"), $toSend);
			}
        }
        return $arFields;
    }

}

class InfoBlocks
{
	// создаем обработчик события "OnAfterIBlockElementAdd"
	function OnAfterIBlockElementAddHandler(&$arFields)
	{
		if($arFields["IBLOCK_ID"] == IBLOCK_CONVERSATION){
			$UsersToEmail = array();
			$UserId = $arFields["CREATED_BY"];
			$UsersToEmail[] = $UserId; // to Leader

			$arFilter = Array("IBLOCK_ID"=>IBLOCK_PROJECT, "CREATED_BY" => $UserId);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), Array());
			if($ob = $res->GetNextElement())
			{
				$arElementFields = $ob->GetProperties();
				$UsersToEmail[] = $arElementFields["EXPERT"]["VALUE"]; // to Expert
			}

			$arFilter = Array("IBLOCK_ID"=>IBLOCK_TEAM, "CREATED_BY" => $UserId);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), Array());
			if($ob = $res->GetNextElement())
			{
				$arElementFields = $ob->GetProperties();
				foreach ($arElementFields["PARTICIPANTS"]['VALUE'] as $participant_id){
					$UsersToEmail[] = $participant_id; // to Participants
				}
			}

			// Send Email

			$MailProps = Array();
			foreach($UsersToEmail as $userID){
				$rsUser = CUser::GetByID($userID);
				$arUser = $rsUser->Fetch();
				$email_to = $arUser['EMAIL'];
				$MailProps["EMAIL_TO"][] = $email_to;
			}
			$MailProps["EMAIL_TO"] = implode(", ", $MailProps["EMAIL_TO"]);

			CEvent::Send ("NEW_MESSAGE_PROJECT", Array("s1"), $MailProps);
		}

		if($arFields["IBLOCK_ID"] == IBLOCK_PROJECT){
			$toSend = Array();
			$toSend["ID_PROJECT"] = $arFields['ID'];

			CEvent::Send ("ADD_NEW_PROJECT", Array("s1"), $toSend);
		}
	}

	function OnBeforeIBlockElementUpdateHandler(&$arFields)
	{
		global $APPLICATION;
		if($arFields["IBLOCK_ID"] == IBLOCK_PROJECT){
			// Get user e-mail
			$arSelect = Array("ID", "NAME", "CREATED_BY");
			$arFilter = Array("IBLOCK_ID"=>IBLOCK_PROJECT, "ID" => $arFields["ID"]);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
			if($ob = $res->GetNextElement())
			{
				$arElementFields = $ob->GetFields();
				$userID = $arElementFields["CREATED_BY"];
				$rsUser = CUser::GetByID($userID);
				$arUser = $rsUser->Fetch();
				$email_to = $arUser['EMAIL'];
			}

			// process statuses
			$current_status = $arFields["PROPERTY_VALUES"][26][0]["VALUE"];

			$rsIBLockPropertyList = CIBlockProperty::GetList(array(), array("ACTIVE"=>"Y", "IBLOCK_ID"=>IBLOCK_PROJECT, "CODE"=> "STATUS"));
			while ($arProperty = $rsIBLockPropertyList->GetNext())
			{
				$rsPropertyEnum = CIBlockProperty::GetPropertyEnum($arProperty["ID"]);
				$arProperty["ENUM"] = array();
				while ($arPropertyEnum = $rsPropertyEnum->GetNext())
				{
					$itemStatus['CODE'] = $arPropertyEnum["XML_ID"];
					$itemStatus['VALUE'] = $arPropertyEnum["VALUE"];

					$arrStatus[$arPropertyEnum["ID"]] = $itemStatus;
				}
			}

			// If the status is "PROCESS"
			if($arrStatus[$current_status]["CODE"] == "ERRORS") {
				if (!empty($arFields["PROPERTY_VALUES"][29])) {
					$arFields["PROPERTY_VALUES"][29] = "";

					// Check comments
					$comments_text = current($arFields["PROPERTY_VALUES"][30])["VALUE"]["TEXT"];
					if (empty($comments_text)) {
						$APPLICATION->throwException("Введите комментарии по доработке");
						return false;
					}

					$number_of_corrections = current($arFields["PROPERTY_VALUES"][27])['VALUE'] ? current($arFields["PROPERTY_VALUES"][27])['VALUE'] : 0;

					if ($number_of_corrections >= 3) {
						$APPLICATION->throwException("Количество доработок больше трех. Переведите заявку в статус «Отклонена»");
						return false;
					}

					foreach ($arFields["PROPERTY_VALUES"][27] as $key => $value) {
						$arFields["PROPERTY_VALUES"][27][$key]['VALUE'] = ++$number_of_corrections;
					}

					// Notify user about errors
					$MailProps = Array();
					$MailProps["ID_PROJECT"] = $arFields["ID"];
					$MailProps["EMAIL_TO"] = $email_to;
					$MailProps["ADMIN_TEXT"] = $comments_text;
					CEvent::Send ("SEND_TO_CORRECTION", Array("s1"), $MailProps);
				}
			} elseif ($arrStatus[$current_status]["CODE"] == "SUCCESS" || $arrStatus[$current_status]["CODE"] == "REJECT"){
				// Notify user about changed status
				$MailProps["ID_PROJECT"] = $arFields["ID"];
				$MailProps["EMAIL_TO"] = $email_to;
				$MailProps["STATUS"] = $arrStatus[$current_status]["VALUE"];
				CEvent::Send ("CHANGED_PROJECT_STATUS", Array("s1"), $MailProps);
			}
		}
	}

	function OnIBlockElementSetPropertyValuesHandler($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUES, $PROPERTY_CODE, $ar_prop, $arDBProps)
	{
		if($PROPERTY_CODE == "EXPERT" && $IBLOCK_ID == IBLOCK_PROJECT){
			$expertId = $PROPERTY_VALUES[0];

			/*Get current attached projects*/
			$rsUser = CUser::GetByID($expertId);
			$arUser = $rsUser->Fetch();
			$arAttachedProjects = $arUser["UF_ASSIGNED_PROJ"];

			/*Attache project to expert*/
			$arAttachedProjects[] = $ELEMENT_ID;

			$user = new CUser;
			$updatedUserFields = Array(
				"UF_ASSIGNED_PROJ" => $arAttachedProjects,
			);
			$user->Update($expertId, $updatedUserFields);
		}
	}
}
?>