<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/shrot.php"; ?>

                <div class="row">
                    <div class="col col-12">
                        <h1>Порядок участия</h1>

                        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/nav-sub.php"; ?>

                        <div class="content">
                            <div class="row">
                                <div class="col col-md-9 col-12">
                                    <p>
                                        Для участия в Международной молодежной сети #INFOCUS командам необходимо разработать проект,
                                        направленный на решение проблем локального сообщества. Решение выбранной проблемы должно
                                        содействовать достижению одной или нескольким Целям устойчивого развития.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="activities">
                            <div class="activities__wrap row">
                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/1.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/2.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/3.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/4.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/5.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/6.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/7.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/8.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/9.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/10.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/11.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/12.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/13.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/14.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/15.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/16.jpg" alt="">
                                    </a>
                                </div>

                                <div class="activities__element col col-md-2 col-sm-3 col-6">
                                    <a class="popup-open" href="javascript:void(0)" rel="popup-activities-1" aria-label="Открыть">
                                        <img src="/pics/activities/17.jpg" alt="">
                                    </a>
                                </div>
                            </div>

                            <div class="layout"></div>

                            <div class="popup" id="popup-activities-1">
                                <button class="popup__close icon-popup-close" type="button" aria-label="Закрыть"></button>

                                <div class="popup__container">
                                    <img src="/pics/activities/1.jpg" alt="">

                                    <h2 class="popup__title">Цель 1: Повсеместная ликвидация нищеты во всех ее формах нищеты во всех ее формах нищеты во всех ее формах</h2>

                                    <div class="content">
                                        <p>
                                            Устойчивое потребление и производство предполагает стимулирование эффективности использования ресурсов и энергии; сооружение устойчивой инфраструктуры; предоставление доступа к основным социальным услугам; обеспечение «зеленых» и достойных рабочих мест и более высокого качества жизни для всех. Реализация этой программы помогает выполнить общие планы в области развития, уменьшить будущие экономические, экологические и социальные издержки, повысить экономическую конкурентоспособность и сократить уровень нищеты.<br>
                                            <br>
                                            Устойчивое потребление и производство направлено на то, чтобы «делать больше и лучше меньшими средствами», наращивая чистую выгоду от экономической деятельности для поддержания уровня благополучия за счет сокращения объема использования ресурсов, уменьшения деградации и загрязнения в течение всего жизненного цикла при одновременном повышении качества жизни. Для этого необходимо участие различных заинтересованных сторон, в том числе предпринимателей, потребителей, политиков, исследователей, ученых, предприятий розничной торговли, средств массовой информации, учреждений по вопросам сотрудничества в целях развития и других.<br>
                                            <br>
                                            Для этого также требуется системный подход и сотрудничество между участниками цепочки поставок — от производителя до конечного потребителя. Программа предполагает вовлечение потребителей путем просветительских и обучающих инициатив по вопросам устойчивого потребления и образа жизни; предоставление потребителям информации в достаточном объеме за счет стандартизации и маркировки; организацию государственных закупок исходя из принципов устойчивости и так далее.                    </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="content">
                            <div class="row">
                                <div class="col col-md-9 col-12">
                                    <p>
                                        Начиная с 1990 года, доля людей, живущих в крайней нищете, сократилась вдвое. Несмотря на это знаменательное достижение, каждый пятый
                                        человек в развивающихся странах вынужден жить на сумму менее 1,25 доллара в день, при этом миллионы людей получают лишь немногим более этой
                                        суммы, и многие рискуют вновь скатиться к нищете.
                                    </p>

                                    <p>
                                        Нищета — это не просто нехватка доходов и ресурсов для обеспечения средств к существованию на устойчивой основе. Она проявляется в голоде и
                                        недоедании, в ограниченном доступе к образованию и к другим основным социальным услугам, в социальной дискриминации и изоляции, а также в
                                        невозможности участия в принятии решений. Экономический рост должен быть всеохватным, чтобы обеспечивать стабильную занятость и
                                        способствовать равенству.
                                    </p>

                                    <p>
                                        <a href="#!" target="_blank" rel="noopener">Узнать подробнее на сайте ООН</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>