Приглашаем зарегистрироваться на сайте лидеров команд, которые затем должны добавить к работе над проектом остальных его участников.<br>

Хочешь присоединиться к проекту, но тебя не пригласили в команду? Просто напиши ее лидеру.<br>

<a class="popup-open" href="javascript:void(0)" rel="popup-video" aria-label="Открыть">Видеоролик Инфографика</a>

<div class="layout"></div>

<div class="popup" id="popup-video">
    <button class="popup__close icon-popup-close" type="button" aria-label="Закрыть"></button>

    <div class="popup__container">
        <iframe width="100%" height="479" src="https://www.youtube.com/embed/IgApFyjcwaI?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media"
                allowfullscreen></iframe>
    </div>
</div>
