<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <h1>Эксперты</h1>

                        <div class="expert-list">
                            <div class="expert-list__row row">
                                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                                    <div class="expert-card" href="#!">
                                        <a href="#!">
                                            <div class="expert-card__image" style="background-image: url('/templates/pics/familyguy.png')"></div>

                                            <div class="expert-card__title">
                                                Петрова Ирина Ивановна
                                            </div>
                                        </a>

                                        <div class="expert-card__position">Председатель совета чего-нибудь<br>Компания чего-нибудь</div>

                                        <div class="expert-card__tags line-align line-align_center">
                                            <ul class="line-align__wrap">
                                                <li class="line-align__item">
                                                    <div class="tag">Экономика</div>
                                                </li>

                                                <li class="line-align__item">
                                                    <div class="tag">Россия</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                                    <div class="expert-card" href="#!">
                                        <a href="#!">
                                            <div class="expert-card__image" style="background-image: url('/templates/pics/familyguy.png')"></div>

                                            <div class="expert-card__title">
                                                Петрова Ирина Ивановна
                                            </div>
                                        </a>

                                        <div class="expert-card__position">Председатель совета чего-нибудь<br>Компания чего-нибудь</div>

                                        <div class="expert-card__tags line-align line-align_center">
                                            <ul class="line-align__wrap">
                                                <li class="line-align__item">
                                                    <div class="tag">Экономика</div>
                                                </li>

                                                <li class="line-align__item">
                                                    <div class="tag">Россия</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                                    <div class="expert-card" href="#!">
                                        <a href="#!">
                                            <div class="expert-card__image" style="background-image: url('/templates/pics/familyguy.png')"></div>

                                            <div class="expert-card__title">
                                                Петрова Ирина Ивановна
                                            </div>
                                        </a>

                                        <div class="expert-card__position">Председатель совета чего-нибудь<br>Компания чего-нибудь</div>

                                        <div class="expert-card__tags line-align line-align_center">
                                            <ul class="line-align__wrap">
                                                <li class="line-align__item">
                                                    <div class="tag">Экономика</div>
                                                </li>

                                                <li class="line-align__item">
                                                    <div class="tag">Россия</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                                    <div class="expert-card" href="#!">
                                        <a href="#!">
                                            <div class="expert-card__image" style="background-image: url('/templates/pics/familyguy.png')"></div>

                                            <div class="expert-card__title">
                                                Петрова Ирина Ивановна
                                            </div>
                                        </a>

                                        <div class="expert-card__position">Председатель совета чего-нибудь<br>Компания чего-нибудь</div>

                                        <div class="expert-card__tags line-align line-align_center">
                                            <ul class="line-align__wrap">
                                                <li class="line-align__item">
                                                    <div class="tag">Экономика</div>
                                                </li>

                                                <li class="line-align__item">
                                                    <div class="tag">Россия</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="expert-list__footer">
                                Хотите поучаствовать в проекте в качестве эксперта?<br>Пишите <a href="mailto:info@tetradka.org.ru">info@tetradka.org.ru</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>