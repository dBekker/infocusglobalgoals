<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <h1>Личный кабинет</h1>

                        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/nav-sub.php"; ?>

                        <form id="form-project-file" class="form form-project-file" action="/templates/lk/project/file" method="post" data-ajax="false">
                            <div class="form__response">
                                Логин или пароль указаны неверно. Попробуйте ещё раз.
                            </div>

                            <div class="form__content">
                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-project-file-1">Название файла</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-project-file-1" class="input" name="name" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label>Файл</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-project-file-2" class="input input_file" name="photo" type="file" placeholder="Загрузить" required>
                                        <label for="form-project-file-2">Загрузить</label>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                                        <button class="btn" type="submit">Сохранить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>