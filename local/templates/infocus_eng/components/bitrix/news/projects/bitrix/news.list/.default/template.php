<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h1>Проекты</h1>

<?if(count($arResult["ITEMS"])>0):?>
    <div class="expert-list">
        <div class="expert-list__row row">
			<?foreach($arResult["ITEMS"] as $arItem):?>
                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                    <div class="expert-card">
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <div class="expert-card__image" style="background-image: url('<?=$arItem["DISPLAY_PROPERTIES"]["LOGO"]["FILE_VALUE"]["SRC"]?>')">
                                <?if($arItem['PROPERTIES']["SUM_POINT"]):?>
                                    <div class="expert-card__result">
                                        <div class="expert-card__count"><?=$arItem['PROPERTIES']["SUM_POINT"]?></div>
                                        <div class="expert-card__text"><?=App\Helpers::formatSay($arItem['PROPERTIES']["SUM_POINT"], [" балл", " балла", " баллов"])?></div>
                                    </div>
                                <?endif;?>
                            </div>
                            <div class="expert-card__title">
                                <?=$arItem["NAME"]?>
                            </div>
                        </a>

                        <div class="expert-card__position"><?=$arItem["PROPERTIES"]["GOAL"]["VALUE"]?></div>

                        <div class="expert-card__team icon icon_to-text icon_not-hover icon-team"><?=$arItem["TEAM_PROP"]["NAME"];?></div>

                        <div class="expert-card__lead">
                            <b>Куратор:</b> <?=$arItem["PROPERTIES"]["EXPERT"]?>
                        </div>
                    </div>
                </div>
			<?endforeach;?>
        </div>
    </div>
<?else:?>
    <p>На данный момент нет активных проектов.</p>
<?endif;?>