<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <h1>Восстановление пароля</h1>

                        <form id="form-forgot" class="form form-forgot" action="/templates/auth/forgot" method="post" data-ajax="false">
                            <div class="form__response">
                                Логин или пароль указаны неверно. Попробуйте ещё раз.
                            </div>

                            <div class="form__content">
                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-forgot-1">Электронная почта</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-forgot-1" class="input" name="email" type="email" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                                        <button class="btn" type="submit">Отправить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>