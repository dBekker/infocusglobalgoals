<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult["LEADER_USER"] = $USER->CanDoOperation('edit_own_profile', $USER->getId());

if(CModule::IncludeModule("iblock")){
	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
	$arFilter = Array("IBLOCK_ID"=>IBLOCK_PROJECT, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "CREATED_USER_ID" => $USER->getId(), "PROPERTY_STATUS" => "49");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	if($ob = $res->GetNextElement())
	{
		$arResult["IS_READONLY"] = true;
	}
}

if(!$arResult["LEADER_USER"]){
	$arResult["IS_READONLY"] = true;
}
?>