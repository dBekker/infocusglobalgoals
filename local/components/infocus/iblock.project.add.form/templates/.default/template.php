<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false); ?>
<? if ($arResult["ERRORS"]["NO_TEAM"]): ?>
    <p>
        Для того, чтобы добавить проект, необходимо сначала <a href="/profile/team/">сформировать и добавить команду</a>.
    </p>
<? else: ?>

	<? if (strlen($arResult["MESSAGE"]) > 0): ?>
        <div class="status status_success">
			<?= $arResult["MESSAGE"] ?>
        </div>
	<? endif ?>

    <? if (!empty($arResult["ADMIN_COMMENTS"]['TEXT']) && $arResult['PROJECT_STATUS'] == "ERRORS"): ?>
        <p><?= $arResult["ADMIN_COMMENTS"]['TEXT'] ?></p>
    <? endif; ?>

    <div class="content">
        <div class="row">
            <div class="col col-md-9 col-12">
                <p>
					<? $APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "page",
							"AREA_FILE_SUFFIX" => "inc",
							"EDIT_TEMPLATE" => ""
						)
					); ?>
                </p>
            </div>
        </div>
    </div>

    <form id="form-project" class="form-project" name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post"
          enctype="multipart/form-data" data-ajax="false">
		<?= bitrix_sessid_post() ?>
		<? if (!empty($arResult["ERRORS"])): ?>
            <div class="form__response form__response_error">
				<? ShowError(implode("<br />", $arResult["ERRORS"])) ?>
            </div>
		<? endif; ?>

		<? if ($arParams["MAX_FILE_SIZE"] > 0): ?><input type="hidden" name="MAX_FILE_SIZE"
                                                         value="<?= $arParams["MAX_FILE_SIZE"] ?>" /><? endif ?>
        <div class="form__content">
            <div class="row">
				<? for ($column = 1; $column <= 2; $column++): ?>
                    <div class="form-project__col col col-md-6 col-12">
						<? if ($column == 2) { ?> <h3 class="d-block d-sm-none">English version</h3><? }; ?>
                        <div class="form-project__cell">
							<? foreach ($arParams["PROPERTY_CODES_GROUP_" . $column] as $propertyKey => $propertyID): ?>
                                <div class="input-field row">
                                    <div class="col-md-5 col-sm-3 col-12 align-self-center">
                                        <label
											<? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] !== "F"): ?>for="form-project-<?= $column * ($propertyKey + 1) ?>"<? endif; ?>>
											<? if (intval($propertyID) > 0): ?><?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?><? else: ?><?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : GetMessage("IBLOCK_FIELD_" . $propertyID) ?><? endif ?></td>
                                        </label>
                                    </div>
                                    <div class="col-md-6 col-sm-5 col-12 align-self-center">
										<?
										if (intval($propertyID) > 0) {
											if (
												$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
												&&
												$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
											)
												$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                                            elseif (
												(
													$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
													||
													$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
												)
												&&
												$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
											)
												$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
										} elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
											$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

										if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
											$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
											$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
										} else {
											$inputNum = 1;
										}

										if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
											$INPUT_TYPE = "USER_TYPE";
										else
											$INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

										switch ($INPUT_TYPE):
											case "USER_TYPE":
												for ($i = 0; $i < $inputNum; $i++) {

													if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
														$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
													} elseif ($i == 0) {
														$value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
													} else {
														$value = "";
													}
													?>
                                                    <textarea id="form-project-<?= $column * ($propertyKey + 1) ?>"
                                                              name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
                                                              class="input input_area"
														<?= $arResult["LEADER_USER"] ? "" : "readonly" ?>
															  <?
															  if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<?
													endif;
													?>><?= $value ?></textarea>
													<?
												}
												break;

											case "S":
											case "N":
												for ($i = 0; $i < $inputNum; $i++) {
													if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
														$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
													} elseif ($i == 0) {
														$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

													} else {
														$value = "";
													}
													?>
                                                    <input id="form-project-<?= $column * ($propertyKey + 1) ?>"
                                                           class="input"
                                                           name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"
														<?= $arResult["LEADER_USER"] ? "" : "readonly" ?>
                                                           value="<?= $value ?>" type="text"
														   <?
														   if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>required<?
													endif;
													?>>

													<?
													if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
														$APPLICATION->IncludeComponent(
															'bitrix:main.calendar',
															'',
															array(
																'FORM_NAME' => 'iblock_add',
																'INPUT_NAME' => "PROPERTY[" . $propertyID . "][" . $i . "]",
																'INPUT_VALUE' => $value,
															),
															null,
															array('HIDE_ICONS' => 'Y')
														);
														?>

													<? endif ?>

													<?
												}
												break;

											case "F":
												for ($i = 0; $i < $inputNum; $i++) {
													$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
													?>
													<?
													if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])) {
														?>
														<?
														if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]) {
															?>
                                                            <div class="form-project__image preview-image"
                                                                 style="background-image: url('<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>')"></div>
															<?
														}
													} ?>


													<?php if ($arResult["LEADER_USER"]): ?>
                                                        <input type="hidden"
                                                               name="PROPERTY[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                                                               value="<?= $value ?>"/>
                                                        <input type="file"
                                                               id="form-project-<?= $column * ($propertyKey + 1) ?>"
                                                               class="input input_file"
                                                               size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                                                               name="PROPERTY_FILE_<?= $propertyID ?>_<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>"
                                                               placeholder="Загрузить"/>
                                                        <label for="form-project-<?= $column * ($propertyKey + 1) ?>">Загрузить
                                                            фотографию</label>
													<?php endif ?>
													<?
												}

												break;
											case "L":

												if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
													$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "radio";
												else
													$type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

												switch ($type):
													case "checkbox":
													case "radio":
														?>
                                                        <div class="line-align">
                                                            <ul class="line-align__wrap">
                                                                <?$count_radio = 0;?>
																<?
																foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
																	$checked = false;
																	if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
																		if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID])) {
																			foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum) {
																				if ($arElEnum["VALUE"] == $key) {
																					$checked = true;
																					break;
																				}
																			}
																		}
																	} else {
																		if ($arEnum["DEF"] == "Y") $checked = true;
																	}
																	?>
                                                                    <li class="line-align__item">
                                                                        <input type="<?= $type ?>"
                                                                               class="input input_radio input_radio-<?=++$count_radio?>"
																			<?= $arResult["LEADER_USER"] ? "" : "disabled" ?>
                                                                               name="PROPERTY[<?= $propertyID ?>]<?= $type == "checkbox" ? "[" . $key . "]" : "" ?>"
                                                                               value="<?= $key ?>"
                                                                               id="form-project-<?= $column * ($propertyKey + 1) ?>-<?= $key ?>" <?= $checked ? " checked=\"checked\"" : "" ?> />

                                                                        <label for="form-project-<?= $column * ($propertyKey + 1) ?>-<?= $key ?>"><?= $arEnum["VALUE"] ?></label>
                                                                    </li>
																	<?
																} ?>
                                                            </ul>
                                                        </div>
														<?
														break;

													case "dropdown":
														?>
                                                        <select name="PROPERTY[<?= $propertyID ?>]"
                                                                class="input input_select"
                                                                id="form-project-<?= $column * ($propertyKey + 1) ?>"
                                                        >
                                                            <option value=""><?
																echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA") ?></option>
															<?
															if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
															else $sKey = "ELEMENT";

															foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
																$checked = false;
																if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
																	foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
																		if ($key == $arElEnum["VALUE"]) {
																			$checked = true;
																			break;
																		}
																	}
																} else {
																	if ($arEnum["DEF"] == "Y") $checked = true;
																}
																?>
                                                                <option value="<?= $key ?>" <?= $arResult["LEADER_USER"] ? "" : "disabled" ?> <?= $checked ? " selected=\"selected\"" : "" ?>><?= $arEnum["VALUE"] ?></option>
																<?
															}
															?>
                                                        </select>
														<?
														break;

												endswitch;
												break;
										endswitch; ?>
                                    </div>
                                </div>
							<? endforeach; ?>
                        </div>
                    </div>
				<? endfor; ?>
            </div>
			<?php if ($arResult["LEADER_USER"]): ?>
                <div class="input-field row">
                    <div class="col col-12" style="text-align: center">
                        <input type="hidden" name="iblock_submit"
                               value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
                        <button type="submit" class="btn" ><?= GetMessage("IBLOCK_FORM_SUBMIT") ?></button>
						<? if (strlen($arParams["LIST_URL"]) > 0): ?>
                            <input type="hidden" name="iblock_apply" value="<?= GetMessage("IBLOCK_FORM_APPLY") ?>"/>
                            <input
                                    type="hidden"
                                    name="iblock_cancel"
                                    value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
                                    onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"]) ?>';"
                            >
                            <button type="submit"><?= GetMessage("IBLOCK_FORM_APPLY") ?></button>
                            <button
                                    class="btn"
                                    name="iblock_cancel"
                                    onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"]) ?>';"
                            ><? echo GetMessage('IBLOCK_FORM_CANCEL'); ?></button>
						<? endif ?>
                    </div>
                </div>
			<?php endif ?>
        </div>
    </form>

<? endif; ?>