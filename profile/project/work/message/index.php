<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Общение");
?>
<div class="row">
    <div class="col col-12">
        <h1><? $APPLICATION->ShowTitle(false) ?></h1>
        <? $APPLICATION->IncludeFile("/inc/submenu.php", Array(), Array()); ?>
        <? $APPLICATION->IncludeComponent(
            "infocus:iblock.message.add.form",
            ".default",
            array(
                "CUSTOM_TITLE_PREVIEW_TEXT" => "Результаты общения (вопросы и решения)",
                "DEFAULT_INPUT_SIZE" => "30",
                "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                "ELEMENT_ASSOC" => "CREATED_BY",
                "GROUPS" => array(
                    0 => "1",
                    1 => "7",
                    2 => "9",
                ),
                "IBLOCK_ID" => "4",
                "IBLOCK_TYPE" => "Competition",
                "LEVEL_LAST" => "Y",
                "LIST_URL" => "/profile/project/work/",
                "MAX_FILE_SIZE" => "0",
                "MAX_LEVELS" => "100000",
                "MAX_USER_ENTRIES" => "100000",
                "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                "PROPERTY_CODES" => array(
                    0 => "34",
                    1 => "35",
                    2 => "PREVIEW_TEXT",
                ),
                "PROPERTY_CODES_REQUIRED" => array(
                    0 => "34",
                    1 => "35",
                    2 => "PREVIEW_TEXT",
                ),
                "RESIZE_IMAGES" => "N",
                "SEF_MODE" => "N",
                "STATUS" => "ANY",
                "STATUS_NEW" => "N",
                "USER_MESSAGE_ADD" => "",
                "USER_MESSAGE_EDIT" => "",
                "USE_CAPTCHA" => "N",
                "COMPONENT_TEMPLATE" => ".default"
            ),
            false
        ); ?>
    </div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>