<?php if(CSite::InDir(SITE_DIR.'profile/') || CSite::InDir(SITE_DIR.'profile-expert/')){$isLK = true;}?>
<ul class="line-align__wrap">
	<li class="line-align__item">
		<?if(!$USER->IsAuthorized()):?>
			<a class="icon icon_to-text icon-auth" href="/auth/">Login</a>
		<?elseif ($USER->IsAuthorized() && $isLK):?>
			<a class="icon icon_to-text icon-auth" href="/auth/?logout=yes">Logout</a>
		<?else:?>
			<a class="icon icon_to-text icon-auth" href="/profile/">Profile</a>
		<?endif;?>
	</li>
    <li class="line-align__item line-align__item_accent d-none d-md-inline-block">
        <a href="/">RU</a>
    </li>
</ul>