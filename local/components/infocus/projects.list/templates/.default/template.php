<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetPageProperty("title", "Проекты — Личный кабинет — InFocus");
?>
<h1>Проекты</h1>
<? $APPLICATION->IncludeFile("/inc/submenu.php", Array(), Array()); ?>

<?if(count($arResult["ITEMS"])>0):?>
    <div class="expert-list">
        <div class="expert-list__row row">
			<?foreach($arResult["ITEMS"] as $arItem):?>
                <div class="expert-list__col col col-md-4 col-sm-6 col-12">
                    <a class="expert-card" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                        <div class="expert-card__image" style="background-image: url('<?=$arItem["DISPLAY_PROPERTIES"]["LOGO"]["FILE_VALUE"]["SRC"]?>')">
							<?php if ($arItem["EXPERTS"]["IS_ME"]): ?>
                                <div class="expert-card__result">
                                    <div class="expert-card__text">Мой проект</div>
                                </div>
							<?php endif ?>
                        </div>
                        <div class="expert-card__title">
							<?=$arItem["NAME"]?>
                        </div>

                        <div class="expert-card__position"><?=$arItem["PROPERTIES"]["GOAL"]["VALUE"]?></div>


						<?php if ($arItem['USER_PROPS']['SPEAK_EN']): ?>
                            <div class="expert-card__lang">Английский язык</div>
						<?php endif ?>

                        <div class="expert-card__team icon icon_to-text icon_not-hover icon-team"><?=$arItem["TEAM_PROP"]["NAME"];?></div>
                    </a>

					<?php if (!$arItem["EXPERTS"]["HAS_EXPERT"]): ?>
                        <div class="expert-action">
                            <form name="iblock_add" method="POST">
								<?=bitrix_sessid_post()?>
                                <input type="hidden" name="ID" value="<?=$arItem["ID"];?>">
                                <input type="hidden" name="choose_expert" class="btn" value="Выбрать проект">
                                <button type="submit" class="btn">Выбрать проект</button>
                            </form>
                        </div>
					<?php endif ?>
                </div>
			<?endforeach;?>
        </div>
    </div>
<?endif?>