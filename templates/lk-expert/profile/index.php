<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <h1>Личный кабинет</h1>

                        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/nav-sub.php"; ?>

                        <form id="form-profile" class="form form-profile" action="/templates/lk-expert/profile" method="post" data-ajax="false">
                            <div class="form__response">
                                Логин или пароль указаны неверно. Попробуйте ещё раз.
                            </div>

                            <div class="form__content">
                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-1">Имя и фамилия</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-1" class="input" name="name" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-2">Страна</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-2" class="input" name="country" type="text">
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-3">Язык</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-3" class="input" name="lang" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-16">Дата рождения</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-16" class="input input_date" name="date" type="text">
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-4">Сфера специализации</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <select id="form-profile-4" class="input input_select" name="areas">
                                            <option>Экология</option>
                                            <option>Экономика</option>
                                            <option>Общество</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-5">Электронная почта</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-5" class="input" name="email" type="email" required>
                                    </div>

                                    <div class="col-md-6 align-self-center d-none d-md-block">
                                        <div class="input-field__hint">
                                            Адрес электронной почты будет использоваться для входа в Личный кабинет
                                        </div>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-6">Телефон</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-6" class="input input_phone" name="phone" type="text" required>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-7">Должность</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-7" class="input" name="position" type="text">
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-8">Место работы</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-8" class="input" name="job" type="text">
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12">
                                        <label for="form-profile-9">Краткая биография</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <textarea id="form-profile-9" class="input input_area" name="bio"></textarea>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-10">Интересы и хобби</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <textarea id="form-profile-10" class="input input_area" name="hobby"></textarea>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-11">Цитата</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-11" class="input" name="quote" type="text">
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-3 col-sm-5 col-12 offset-md-2 offset-sm-3 align-self-center">
                                        <div class="form-profile__image" style="background-image: url('/templates/pics/familyguy.png')"></div>

                                        <input id="form-profile-12" class="input input_file" name="photo" type="file" placeholder="Загрузить фотографию">
                                        <label for="form-profile-12">Загрузить фотографию</label>
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-13">Старый пароль</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-13" class="input" name="password-old" type="password">
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-14">Новый пароль</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-14" class="input" name="password-new" type="password">
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-2 col-sm-3 col-12 align-self-center">
                                        <label for="form-profile-15">Повтор пароля</label>
                                    </div>

                                    <div class="col-md-3 col-sm-5 col-12 align-self-center">
                                        <input id="form-profile-15" class="input" name="password-new-replay" type="password">
                                    </div>
                                </div>

                                <div class="input-field row">
                                    <div class="col-md-10 col-sm-9 offset-md-2 offset-sm-3 col-12">
                                        <button class="btn" type="submit">Сохранить</button>

                                        <p>
                                            Предоставляя личные данные, вы&nbsp;подтверждаете,
                                            что ознакомлены с&nbsp;<a href="#!">политикой конфиденциальности</a> и принимаете
                                            <a href="#!">пользовательское соглашение</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>