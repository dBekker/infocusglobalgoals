<nav class="nav-sub line-align">
	<?$APPLICATION->IncludeComponent(
		"bitrix:menu",
		"submenu",
		Array(
			"ROOT_MENU_TYPE" => "sub",
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "left",
			"USE_EXT" => "Y",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => "",
		),
		false
	);?>
</nav>