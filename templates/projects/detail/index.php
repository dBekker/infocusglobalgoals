<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/meta.php"; ?>

    <div class="page-wrapper">
        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/header.php"; ?>

        <section class="page-content">
            <div class="container">
                <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/shrot.php"; ?>

                <section class="project-header">
                    <div class="row">
                        <div class="col col-md-7 col-12">
                            <h1 class="project-header__title">Мир, правосудие и эффективные институты</h1>

                            <div class="project-points">
                                <div class="project-points__result">
                                    <div class="project-points__count">180</div>
                                    <div class="project-points__title">баллов</div>
                                </div>

                                <div class="project-points__info">
                                    160 баллов от эксперта<br>
                                    и 20 по итогам голосования
                                </div>
                            </div>

                            <div class="project-tags">
                                <div class="project-tags__team icon icon_to-text icon_not-hover icon-team">Команда №1</div>

                                <div class="project-tags__position">2. Ликвидация голода</div>

                                <div class="project-tags__lang">Английский язык</div>
                            </div>
                        </div>
                    </div>

                    <div class="project-header__image" style="background-image: url('/templates/pics/project.jpg')"></div>
                </section>

                <section class="project-detail">
                    <div class="row">
                        <div class="col col-md-8 col-12">
                            <h1>
                                Международная молодежная сеть социальных инноваторов
                            </h1>

                            <div class="content">
                                <p>
                                    Самое время заявить о себе и показать, как ты и твоя команда можете повлиять на глобальные вызовы! Мир ищет молодых и амбициозных
                                    лидеров-инноваторов, готовых трансформировать сообщества во имя устойчивого развития нашей планеты.
                                </p>

                                <p>
                                    Стань современным героем и предложи вместе с командой идеи по преобразованию локального, регионального или мирового сообщества, приняв участие в
                                    Международной молодежной сети социальных инноваторов #INFOCUS.
                                </p>

                                <p>
                                    Международная молодежная сеть #INFOCUS – это платформа для выстраивания взаимодействия, обмена идеями и практиками в области добровольчества и
                                    содействия достижению Целей устойчивого развития, провозглашенных ООН на период до 2030 года, молодежными командами из разных уголков Земли под
                                    наставничеством ведущих международных экспертов.
                                </p>

                                <p>
                                    На протяжении 24 недель молодые лидеры-инноваторы вместе с экспертами-наставниками международного уровня будут разрабатывать проекты, лучшие из
                                    которых предстанут перед мировым сообществом в Организации Объединенных Наций уже в июне 2018 года!
                                </p>

                                <p>
                                    К участию приглашаются молодые инноваторы от 14 до 25 лет из разных уголков земного шара, желающие внести свой вклад в достижение общемировой
                                    Повестки дня в области устойчивого развития до 2030 года, чтобы оставить свой след в истории.
                                </p>

                                <p>
                                    Самое время заявить о себе и показать, как ты и твоя команда можете повлиять на глобальные вызовы! Мир ищет молодых и амбициозных
                                    лидеров-инноваторов, готовых трансформировать сообщества во имя устойчивого развития нашей планеты.
                                </p>

                                <p>
                                    Стань современным героем и предложи вместе с командой идеи по преобразованию локального, регионального или мирового сообщества, приняв участие в
                                    Международной молодежной сети социальных инноваторов #INFOCUS.
                                </p>

                                <p>
                                    Международная молодежная сеть #INFOCUS – это платформа для выстраивания взаимодействия, обмена идеями и практиками в области добровольчества и
                                    содействия достижению Целей устойчивого развития, провозглашенных ООН на период до 2030 года, молодежными командами из разных уголков Земли под
                                    наставничеством ведущих международных экспертов.
                                </p>

                                <p>
                                    На протяжении 24 недель молодые лидеры-инноваторы вместе с экспертами-наставниками международного уровня будут разрабатывать проекты, лучшие из
                                    которых предстанут перед мировым сообществом в Организации Объединенных Наций уже в июне 2018 года!
                                </p>

                                <p>
                                    К участию приглашаются молодые инноваторы от 14 до 25 лет из разных уголков земного шара, желающие внести свой вклад в достижение общемировой
                                    Повестки дня в области устойчивого развития до 2030 года, чтобы оставить свой след в истории.
                                </p>
                            </div>

                            <div class="to-back">
                                <a href="#!">Другие проекты</a>
                            </div>
                        </div>

                        <div class="col col-md-3 offset-md-1 col-12">
                            <div class="project-lead">
                                <div class="row">
                                    <div class="project-lead__element col col-md-12 col-sm-6 col-12 align-self-center">
                                        <div class="project-lead__image" style="background-image: url('/templates/pics/familyguy.png')"></div>

                                        <div class="project-lead__info">
                                            <div class="project-lead__title">Куратор:</div>
                                            <div class="project-lead__name">Петрова Ирина Ивановна</div>
                                        </div>
                                    </div>

                                    <div class="project-lead__element col col-md-12 col-sm-6 col-12 align-self-center">
                                        <div class="project-lead__info">
                                            <div class="project-lead__title">Тим-лидер:</div>
                                            <div class="project-lead__name">Дмитрий Юрьевич</div>
                                            <div class="project-lead__phone"><a href="tel:+79197022112">+7 (919) 702-21-12</a></div>
                                            <div class="project-lead__mail"><a href="mailto:hello@mail.ru">hello@mail.ru</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>

        <?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/footer.php"; ?>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/templates/inc/end.php"; ?>