<?
$aMenuLinks = Array(
	Array(
		"Participation Procedure",
		"/en/rules/steps/",
		Array(),
		Array(),
		""
	),
	Array(
		"Competition Period",
		"/en/rules/dates/",
		Array(),
		Array(),
		""
	),
	Array(
		"Areas of Activity",
		"/en/rules/activities/",
		Array(),
		Array(),
		""
	),
	Array(
		"Interacting with Experts",
		"/en/rules/expert_interaction/",
		Array(),
		Array(),
		""
	),
	Array(
		"Evaluation Criteria",
		"/en/rules/criteria/",
		Array(),
		Array(),
		""
	),
	Array(
		"Progress Review",
		"/en/rules/results/",
		Array(),
		Array(),
		""
	)
);
?>
